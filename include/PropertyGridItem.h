# ifndef PGITEM_H
# define PGITEM_H

# include <PropertyGridCtrl.h>
# include <CSTB\IntrusiveTree.h>


/// A simple class that holds data for a property grid item.
/// The behavior of the item is in the APGItemType.
class PGItem
{
protected:
	PGItemType&		m_type;
	LPTSTR			m_sTitle;
	UINT			m_uID;
	HMENU			m_hMenu;
	BOOL			m_bMenuShared;
	SHORT			m_pos;
	SHORT			m_height;
	DWORD			m_dwState;
	LPVOID			m_pUserData;

public:
	CSTB::TreeHook hook;
	CSTB::ListHook selHook;

	PGItem( PGItemType& type );
	virtual ~PGItem();

	HPGITEM GetHandle() const { return (HPGITEM)this; }

	/// TODO: Remove 'virtual' from methods which are not really meant to be overridden.

	/// @name Control stuff
	/// @{

	PGItemType&			GetType() const { return m_type; }

	virtual VOID		SetTitle( HWND hPropGrid, LPCTSTR sTitle );
	virtual LPCTSTR		GetTitle( HWND hPropGrid ) const;

	virtual VOID		SetID( UINT id ) { m_uID = id; }
	virtual UINT		GetID() const { return m_uID; }

	virtual VOID		SetMenu( HWND hPropGrid, HMENU hMenu, BOOL bShared );
	virtual HMENU		GetMenu( BOOL* pbShared ) const
						{ if( pbShared ) *pbShared = m_bMenuShared; return m_hMenu; }

	virtual VOID		SetPos( SHORT pos ) { m_pos = pos; }
	virtual SHORT		GetPos() { return m_pos; }

	virtual VOID		SetHeight( SHORT height ) { m_height = height; }
	virtual SHORT		GetHeight() const { return m_height; }

	virtual VOID		SetState( DWORD dwState ) { m_dwState = dwState; }
	virtual DWORD		GetState() const { return m_dwState; }

	virtual VOID		SetUserData( LPVOID pUserData ) { m_pUserData = pUserData; }
	virtual LPVOID		GetUserData() const { return m_pUserData; }

	/// @}



	enum DataType
	{
		DT_NONE,
		DT_TEXT,
		DT_INT,
		DT_FLOAT,
		DT_BOOL,
		DT_OTHER
	};

	/// This is only used to enable the default implementation of the value Get*() and Set*() methods
	/// to convert to/from the item's "native" data type. So if this method returns DT_INT, the default
	/// impelentation of GetText() would call GetInt() and return the string representation of the integer.
	/// TODO: Implement this functionality.
	virtual DataType	GetDataType( HWND hPropGrid ) { return DT_NONE; }


	/// @name Data Accessors
	/// @{
	virtual BOOL		SetText( HWND hPropGrid, LPCTSTR sText, INT length ) { return FALSE; }
	virtual INT			GetText( HWND hPropGrid, LPTSTR sBuffer, INT bufferLen, BOOL& bSuccess ) const
						{ bSuccess = FALSE; return 0; }


	virtual BOOL		SetInt( HWND hPropGrid, INT value ) { return FALSE; }
	virtual INT			GetInt( HWND hPropGrid, BOOL& bSuccess ) const
						{ bSuccess = FALSE; return 0; }


	virtual BOOL		SetFloat( HWND hPropGrid, FLOAT value ) { return FALSE; }
	virtual FLOAT		GetFloat( HWND hPropGrid, BOOL& bSuccess ) const
						{ bSuccess = FALSE; return 0.f; }


	virtual BOOL		SetBool( HWND hPropGrid, BOOL value ) { return FALSE; }
	virtual BOOL		GetBool( HWND hPropGrid, BOOL& bSuccess ) const
						{ bSuccess = FALSE; return FALSE; }

	/// @}


	/// This gets called when the item is inserted into the control. The item is shown (OnShow()) after it's inserted.
	virtual VOID		OnInsert( HWND hPropGrid ) { }

	/// This is called when the item is removed from the control. After this method returns,
	/// the DeleteItem() method of the item's type is called to delete the object.
	virtual VOID		OnRemove( HWND hPropGrid ) { }

	/// This is called when the item is about to be shown or hidden. This can be a result of the parent being
	/// expanded/collapsed, or the item being shown/hidden by a call to PropertyGrid_ShowItem();
	virtual VOID		OnShow( HWND hPropGrid, BOOL bShow ) { }

	/// Called when the item is enabled/disabled by a call to PropertyGrid_EnableItem();
	virtual VOID		OnEnable( HWND hPropGrid, BOOL bEnable ) { }

	/// Called when the position or size of the item has changed.
	virtual VOID		OnSizeMove( HWND hPropGrid ) { }

	/// Called when the item is about to be edited. Return FALSE if the item is not editable.
	virtual BOOL		OnEdit( HWND hPropGrid ) { return FALSE; }

	/// Called when the item finishes editing.
	virtual VOID		OnEndEdit( HWND hPropGrid, BOOL bCancel ) {}

	/// Determines whether the item has a content area. If not, the title area will take up the entire
	/// size of the item's rectangle. This is used with category items.
	virtual BOOL		HasContent( HWND hPropGrid ) { return TRUE; }

	virtual VOID		PaintContent( HWND hPropGrid, HDC hDC, RECT* prcPaint, RECT* prcContent )
						{ FillRect( hDC, prcContent, (HBRUSH)(COLOR_3DFACE+1) ); }//(COLOR_WINDOW+1) ); }

	virtual VOID		PaintTitleBG( HWND hPropGrid, HDC hDC, RECT* prcPaint, RECT* prcTitle );
	virtual VOID		PaintText( HWND hPropGrid, HDC hDC, RECT* prcPaint, RECT* prcText );
	virtual VOID		PaintMenuArrow( HWND hPropGrid, HDC hDC, RECT* prcMenuArrow );
	virtual VOID		PaintExpandGlyph( HWND hPropGrid, HDC hDC, RECT* prcGlyph );
	virtual COLORREF	GetBackgroundColor( HWND hPropGrid ) { return GetSysColor( COLOR_3DFACE ); }


	/// The control passes all of its messages to the focus item through this method if the control is
	/// in edit mode. Set bHandled to TRUE to prevent further processing of the message.
	virtual LRESULT MsgProc( HWND hPropGrid, UINT Msg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
	{ return 0; }
};



// For quick conversion between the internal item pointer and the handle type.
static PGItem* CONV(HPGITEM h) { return (PGItem*)h; }
static HPGITEM CONV(PGItem* p) { return (HPGITEM)p; }
static HPGITEM CONV(const PGItem* p) { return (HPGITEM)p; }



/// This is the function that inserts an item into the property grid control.
/// An item can not exist in more than one property grid control at a time.
HPGITEM PropertyGrid_InsertItem( HWND hPropGrid, PGItem* pItem, UINT id, LPCTSTR sTitle,
						HPGITEM hParent, HPGITEM hInsertBefore,
						LPVOID pUserData, LPVOID pCreationData );







# endif // inclusion guard.