// Property Grid Control.
// Adel Amro - http://code-section.com

/// @desc A pure win32 property grid control. It's technically a property LIST control, but that's how it's most commonly known.


# ifndef PROPERTYGRID_H
# define PROPERTYGRID_H



# include <windows.h>
# include <commctrl.h>
# include <commdlg.h>


/// @defgroup PropertyGrid
/// @{

# define WC_PROPERTYGRID		TEXT("WCPropertyGrid") ///< Property grid window class name.


BOOL PropertyGrid_Init( HINSTANCE ); ///< Call this before creating any property grid controls.
VOID PropertyGrid_Uninit(); ///< Unregister class and cleanup.

/// This should be called to enable keyboard navigation. If it returns TRUE, the message should not be passed to
/// TranslateMessage() and DispatchMessage(). This is similar to the IsDialogMessage() API.
BOOL IsPropertyGridMessage( HWND hPropGrid, LPMSG lpMsg );
BOOL IsPropertyGrid( HWND hWnd );

/// Keyboard hooking - allows applications to avoid having to call IsPropertyGridMessage()
/// before messages are translated and dispatched. Also allows property grid controls to be
/// placed inside modal dialog boxes.
INT PropertyGrid_HookKeyboard();
INT PropertyGrid_UnhookKeyboard();



DECLARE_HANDLE (HPGITEM);		// Property grid item handle.
class PGItem;




/// This class is the interface that an item type is required to implement so that
/// the property grid control can display it properly.
class PGItemType
{
public:
	virtual LPCTSTR GetName() = 0;

	/// This is called by the control when inserting items.
	virtual PGItem* AllocateItem( HWND hPropGrid, LPVOID pCreationData ) = 0;

	/// Called after the control has removed an item of this type from the control.
	/// This typically just calls delete on the item.
	virtual VOID DeleteItem( HWND hPropGrid, PGItem* pItem ) = 0;
};



/// This makes it easier to generate PGItemType classes.
template<typename itemClass>
class PGItemTypeT : public PGItemType
{
public:
	PGItemTypeT( LPCTSTR _sName ) : sName(_sName ) { }
	LPCTSTR sName;
	LPCTSTR GetName() { return sName; }
	PGItem* AllocateItem( HWND hPropGrid, LPVOID pCreationData ) { return new itemClass(*this); }
	void DeleteItem( HWND hPropGrid, PGItem* pItem ) { delete pItem; }
	itemClass* GetItem( HPGITEM hItem )
	{
		if (PropertyGrid_GetItemType(hItem) == this)
			return (itemClass*)hItem;
		return 0;
	}
};



/// Item state and style flags.
# define PGIS_COLLAPSED			(1 << 0)
# define PGIS_DISABLED			(1 << 1) // TODO: Implement.
# define PGIS_HIDDEN			(1 << 2) // TODO: Implement, and modify PropertyGrid_IsItemVisible() and others.
# define PGIS_SELECTED			(1 << 3)


/// Some constants used for painting etc. PG_DIVIDER_THICKNESS is the vertical divider
/// thickness (behavioral) on each side. So it's actually twice this thick plus 1 pixel.
# define PG_INDENTATION_PIXELS		14 ///< Number of pixels for hierarchical indentation.
# define PG_PADDING_PIXELS_H		4 ///<
# define PG_PADDING_PIXELS_V		4 ///<
# define PG_DIVIDER_THICKNESS		3 ///< The thickness of the vertical divider (for resizing, not drawing).
# define PG_DIVIDER_HEIGHT			3 ///< The height of the horizontal divider between items and info pane.
# define PG_MIN_DIVIDER_MARGIN		(PG_INDENTATION_PIXELS*2)


/// See PGN_PROPERTYCHANGED
# define PG_FIRST_CHANGE			((LPVOID)1)





/// Notification message structure used with certain property grid notification messages.
typedef struct tagNMPROPGRID
{
	NMHDR		hdr;
	HPGITEM		hItem; ///< Handle to the item associated with the notification message.
	UINT		idItem; ///< The id of hItem. This is equal to PropertyGrid_GetItemID(hItem);
	LPVOID		additionalInfo; ///< The meaning of this depends on the notification message being sent.
} NMPROPGRID, *LPNMPROPGRID;


typedef LRESULT (*PGNotificationCallback)( HWND hPropGrid, NMPROPGRID* pNM, void* userData );

/// Register a function to be called to receive notifications from the property grid control.
/// If a callback is set, the control will not send notifications to its parent window.
void PropertyGrid_SetNotificationCallback( HWND hPropGrid, PGNotificationCallback cb, void* userData );



/// Property grid notifications. These are sent to the parent window using the WM_NOTIFY message.
/// PGN_PROPERTYCHANGING is sent after the user has finished editing a property but before the new property
/// value is used. PGN_EDITING is different in that it is sent while the user is editing the property.
/// If the editing operation is canceled, the original value (the value at the start of editing) is restored.

# define PGN_SELCHANGING		1 ///< Selection is about to change. Return 0 to let it, 1 to prevent it.
# define PGN_SELCHANGED			2 ///< The selection has changed. Return value ignored.
# define PGN_FOCUSCHANGING		3 ///< The focus item is about to change. Return 0 to let it, 1 to prevent it.
# define PGN_FOCUSCHANGED		4 ///< The focus item has changed. Return value ignored.

# define PGN_PROPERTYCHANGING	5 ///< A property is changing its value. Return 0 to let it, 1 to prevent it (not fully implemented).
# define PGN_PROPERTYCHANGED	6 ///< A property has changed its value. Return value ignored. additionalInfo can be PG_FIRST_CHANGE.

# define PGN_EDITSTART			7 ///< A property is selected for editing.
# define PGN_EDITING			8 ///< A property is being edited (it's value is changing).
# define PGN_EDITDONE			9 ///< The user has finished editing a property.
# define PGN_EDITCANCEL			10 ///< The user has canceled editing a property.

# define PGN_INSERTITEM			11 ///< An item is being inserted into the control. You must not delete the item here.
# define PGN_DELETEITEM			12 ///< An item is being deleted from the control. The item will have been hidden by the time this is sent.
# define PGN_EXPANDITEM			13 ///< A property which is a parent of other properties is expanded.
# define PGN_COLLAPSEITEM		14 ///< A property which is a parent of other properties is collapsed.
# define PGN_DIVIDERPOSCHANGED	15 ///< The divider position has changed.
# define PGN_GETITEMCHILDREN	16 ///< Parent should return if an item has children or not.
# define PGN_GETITEMTITLE		17 ///< Parent should return the item's title. Used when the item's title is set to LPSTR_TEXTCALLBACK.
# define PGN_MENUPOPUP			19
# define PGN_MENUCOMMAND		20

/// \def PGN_MENUPOPUP
/// The notification message is sent when the menu associated with an item is about to be displayed.
/// The additionalInfo member of the NMPROPGRID structure points to the modifiable UINT flags variable
/// that will be passed to TrackPopupMenu(). The default contains the TPM_RETURNCMD flag.
/// You can access and modify the menu by calling PropertyGrid_GetItemMenu().
/// \return Nonzero to prevent the menu from being shown, 0 to let it.

/// \def PGN_MENUCOMMAND
/// The popup menu of the specified item has been dismissed. The "additionalInfo" member contains the ID of
/// the selected item. If the menu was dismissed and no selection was made, additionalInfo will be 0.



/// Flags used with the PGHITTESTINFO structure.
# define PGHT_NOWHERE			0x0001 ///< Below the last visible item but above the information pane.
# define PGHT_ONITEMTITLE		0x0002 ///< The item title area.
# define PGHT_ONITEMCONTENT		0x0004 ///< On the right area of the title.
# define PGHT_ONITEMGLYPH		0x0008 ///< On the expand/collapse button.
# define PGHT_ONITEMMENU		0x0010 ///< One the arrow that drops the menu associated with the item.
# define PGHT_ONITEM	( PGHT_ONITEMGLYPH | PGHT_ONITEMTITLE | PGHT_ONITEMCONTENT | PGHT_ONITEMMENU ) ///< On an item.
//# define PGHT_ONINFOPANE	0x0010 ///< On the information pane at the bottom of the control.
# define PGHT_ONDIVIDER		0x0020 ///< The vertical divider.
//# define PGHT_ONHDIVIDER	0x0040 ///< On the horizontal divider between the items and the info pane.


/// Hit test structure, used with the PropertyGrid_HitTest() function.
/// TODO: Get rid of this and pass arguments to the PropertyGrid_DoHitTest() function directly.
typedef struct tagPGHITTESTINFO
{
	UINT flags;		///< Receives information about the results of a hit test. See the PGHT_* constants.
	HPGITEM hItem;	///< Handle to the item that occupies the point (or NULL).
} PGHITTESTINFO, *LPPGHITTESTINFO;




/// Info structure for items in a property grid control. This is intended for use with the
/// PropertyGrid_GetItemInfo() and PropertyGrid_SetItemInfo() functions.
/// TODO: Consider adding functions to retrieve individual members of this struct, or adding a mask member.
typedef struct tagPGITEMINFO
{
	PGItemType*	pType;
	DWORD		dwState;
	HPGITEM		hParent;
	HPGITEM		hChild;
	HPGITEM		hNextSibling;
	HPGITEM		hPrevSibling;
	HMENU		hMenu;
	BOOL		menuShared; ///< Whether the menu is shared or not.
	UINT		id;
	UINT		level;
	LPCTSTR		sTitle;
	LPVOID		pUserData;
} PGITEMINFO, *LPPGITEMINFO;



///////////////////////


/// Inserts an item into the control.
/// @param pType:		The item type which must have been previously registerd. See PropertyGrid_RegisterItemType().
/// @param id			The control doesn't do anything to guarantee that the item ID is unique.
/// @param sTitle		The item title, displayed at the left. Can be LPSTR_TEXTCALLBACK (see PGN_GETITEMTITLE).
/// @param hParent,hInsertBefore	These can be handles to items previously added to the control, or NULL.
/// @param pUserData	User data stored per item.
/// @param pCreateionData	Passed to the property type's PreCreateItem() and PostCreateItem() methods. This is
/// for use by the property item types.
HPGITEM PropertyGrid_InsertItem( HWND hPropGrid, PGItemType* pType, UINT id, LPCTSTR sTitle,
								HPGITEM hParent, HPGITEM hInsertBefore, LPVOID pUserData,
								LPVOID pCreationData );

/// Removes an item and all descendents (children, grandchildren... etc).
VOID	PropertyGrid_RemoveItem( HWND hPropGrid, HPGITEM hItem, BOOL bRedraw );

/// Removes all items in the control.
VOID	PropertyGrid_Clear( HWND hPropGrid, BOOL bUpdate = TRUE );

VOID	PropertyGrid_GetItemInfo( HWND hPropGrid, HPGITEM hItem, LPPGITEMINFO pItemInfo );

inline PGITEMINFO PropertyGrid_GetItemInfo( HWND hPropGrid, HPGITEM hItem )
{ PGITEMINFO ii; PropertyGrid_GetItemInfo( hPropGrid, hItem, &ii ); return ii; }

PGItemType* PropertyGrid_GetItemType( HPGITEM hItem );

/// See the PGIS_* constants for the flags used by the control.
DWORD	PropertyGrid_GetItemState( HPGITEM hItem );

/// Lower word is used by the control. Upper word can be used for type-specific states.
VOID	PropertyGrid_SetItemState( HPGITEM hItem, DWORD dwNewState );

LPCTSTR PropertyGrid_GetItemTitle( HWND hPropGrid, HPGITEM hItem );
VOID	PropertyGrid_SetItemTitle( HWND hPropGrid, HPGITEM hItem, LPCTSTR sNewTitle, BOOL bUpdate = TRUE );

LPVOID	PropertyGrid_GetItemUserData( HPGITEM hItem );
VOID	PropertyGrid_SetItemUserData( HPGITEM hItem, LPVOID pNewUserData );

UINT	PropertyGrid_GetItemID( HPGITEM hItem ); ///< Control does nothing to ensure items have unique IDs.
VOID	PropertyGrid_SetItemID( HPGITEM hItem, UINT newID );

/// Sets the drop-down menu associated with the item. If the bShared parameter is TRUE, the menu is not
/// destroyed when the item is destroyed. To destroy the current unshared menu of an item,
/// set the hMenu parameter to NULL.
/// Note the menu should be a popup menu created using the CreatePopupMenu() API.
VOID	PropertyGrid_SetItemMenu( HWND hPropGrid, HPGITEM hItem, HMENU hMenu, BOOL bShared );

/// Gets the menu associated with the item. if pbShared is not NULL, its value will indicate whether the
/// menu is shared or not. A shared menu is not destroyed when the item is destroyed.
HMENU	PropertyGrid_GetItemMenu( HPGITEM hItem, BOOL* pbShared );


/// Gets the hierarchical level of the specified item. Level 0 means item is at the root (has no parents),
/// 1 means the item's parent is at the root, and so on.
UINT	PropertyGrid_GetItemLevel( HWND hPropGrid, HPGITEM hItem );

/// Gets the rectangles of the various parts of an item. You can set the ones you don't need to NULL.
/// The item rectangle spans the entire control in width. It's the item's title rectangle plus the content rectangle.
VOID PropertyGrid_GetItemRects( HWND hPropGrid, HPGITEM hItem, RECT* prcItem,
							   RECT* prcContent, RECT* prcTitle, RECT* prcGlyph,
							   RECT* prcText, RECT* prMenuArrow );

UINT	PropertyGrid_GetItemHeight( HWND hPropGrid, HPGITEM hItem );
VOID	PropertyGrid_SetItemHeight( HWND hPropGrid, HPGITEM hItem, UINT height );
SHORT	PropertyGrid_GetDefaultItemHeight( HWND hPropGrid );

/// Returns a pointer to the memroy used to store type-specific item data. The size of this memory block
/// is determined by the PGItemType::GetNumExtraBytes() method of the corresponding item type class.
LPVOID	PropertyGrid_GetItemExtraBytes( HWND hPropGrid, HPGITEM hItem );


// TODO: Add functions to allow the user to move and/or reparent an item and such.
HPGITEM PropertyGrid_GetFirstItem( HWND hPropGrid );
HPGITEM PropertyGrid_GetNextItem( HPGITEM hItem, bool bSkipChildren = false );
HPGITEM PropertyGrid_GetPrevItem( HPGITEM hItem );
HPGITEM	PropertyGrid_GetParent( HPGITEM hItem );
HPGITEM	PropertyGrid_GetChild( HPGITEM hItem );
HPGITEM	PropertyGrid_GetNextSibling( HPGITEM hItem );
HPGITEM	PropertyGrid_GetPrevSibling( HPGITEM hItem );

HPGITEM PropertyGrid_FindItemByID( HWND hPropGrid, UINT ID, HPGITEM hParent = NULL );

//INT		PropertyGrid_GetNumItems( HWND hPropGrid );
INT		PropertyGrid_GetNumVisibleItems( HWND hPropGrid );
HPGITEM PropertyGrid_GetNextVisible( HWND hPropGrid, HPGITEM hItem, BOOL bSkipChildren = FALSE );
HPGITEM PropertyGrid_GetPrevVisible( HWND hPropGrid, HPGITEM hItem );
inline HPGITEM PropertyGrid_GetFirstVisible( HWND hPropGrid )
{ return PropertyGrid_GetNextVisible( hPropGrid, NULL ); }

BOOL	PropertyGrid_ItemHasChildren( HWND hPropGrid, HPGITEM hItem ); ///< For dynamic child addition/removal.

/// Selection mode used with SelectItem.
enum PGSelMode
{
	PGSM_Replace = 0,
	PGSM_Add,
	PGSM_Remove,
	PGSM_Toggle
};

/// Modify the selection. The items whose selected state is modified will be invalidated, but the control
/// will be updated to reflect the change only if bUpdate is TRUE. To remove the selection (select no items),
/// set hItem to NULL and selMode to PGSM_Replace.
VOID	PropertyGrid_SelectItem( HWND hPropGrid, HPGITEM hItem, PGSelMode selMode = PGSM_Replace, BOOL bUpdate = TRUE );
BOOL	PropertyGrid_IsItemSelected( HPGITEM hItem );
HPGITEM PropertyGrid_GetFirstSelectedItem( HWND hPropGrid );
HPGITEM PropertyGrid_GetLastSelectedItem( HWND hPropGrid );
HPGITEM PropertyGrid_GetNextSelectedItem( HWND hPropGrid, HPGITEM hItem );
HPGITEM PropertyGrid_GetPrevSelectedItem( HWND hPropGrid, HPGITEM hItem );

/// @{
/// The selection notification is not sent per-selected item, but instead per selection change.
/// This is implemented using the following two functions. Make sure every call to BeginSelChange()
/// is matched with a call to EndSelChange(). These calls can be nested.

VOID PropertyGrid_BeginSelChange( HWND hPropGrid );
VOID PropertyGrid_EndSelChange( HWND hPropGrid );

/// @}


/// Simply goes through all the control's items to see if the specified item is a member of the control.
/// Useful for debugging purposes.
inline BOOL PropertyGrid_IsItem( HWND hPropGrid, HPGITEM hItem )
{
	if( hItem == NULL ) return FALSE;
	for( HPGITEM h = PropertyGrid_GetFirstItem( hPropGrid );
		h != NULL;
		h = PropertyGrid_GetNextItem( h ) )
	{
		if( h == hItem )
			return TRUE;
	}
	return FALSE;
}

HPGITEM PropertyGrid_GetFocusItem( HWND hPropGrid ); ///< Returns NULL if no item has the focus.
BOOL	PropertyGrid_SetFocusItem( HWND hPropGrid, HPGITEM hItem, BOOL bForce, BOOL bUpdate = TRUE );

BOOL	PropertyGrid_IsEditing( HWND hPropGrid );
BOOL	PropertyGrid_EditItem( HWND hPropGrid, HPGITEM hItem, BOOL bForce );
BOOL	PropertyGrid_CancelEdit( HWND hPropGrid, BOOL bForce );
BOOL	PropertyGrid_EndEdit( HWND hPropGrid, BOOL bForce );

VOID	PropertyGrid_EnableItem( HWND hPropGrid, HPGITEM hItem, BOOL bEnable );
BOOL	PropertyGrid_IsItemEnabled( HPGITEM hItem );

/// This modifies the PGIS_HIDDEN flag in the item's state flags.
/// Note that setting bShow to TRUE does not guarantee the item will become visible, as one of
/// its parents could be collapsed or hidden (see PropertyGrid_EnsureItemVisible()).
VOID	PropertyGrid_ShowItem( HWND hPropGrid, HPGITEM hItem, BOOL bShow, BOOL bUpdate = TRUE );

VOID	PropertyGrid_CollapseItem( HWND hPropGrid, HPGITEM hItem, BOOL bUpdate = TRUE, BOOL bNotify = FALSE );
VOID	PropertyGrid_ExpandItem( HWND hPropGrid, HPGITEM hItem, BOOL bUpdate = TRUE, BOOL bNotify = FALSE );

/// The item is not visible if any parent is collapsed or hidden or if the item itself is hidden.
/// You can use PropertyGrid_IsItemInView() to check for visibility with regard to scrolling.
BOOL	PropertyGrid_IsItemVisible( HWND hPropGrid, HPGITEM hItem );

BOOL	PropertyGrid_IsItemInView( HWND hPropGrid, HPGITEM hItem ); ///< Determines visibility given current scrolling info.
BOOL	PropertyGrid_EnsureItemVisible( HWND hPropGrid, HPGITEM hItem ); ///< Expands parents and scrolls item into view.

inline VOID	PropertyGrid_InvalidateItem( HWND hPropGrid, HPGITEM hItem, BOOL andAllBelowIt = FALSE )
{
	if( !hItem || !hPropGrid || !PropertyGrid_IsItemVisible( hPropGrid, hItem ) )
		return;
	RECT rItem;
	PropertyGrid_GetItemRects( hPropGrid, hItem, &rItem, NULL, NULL, NULL, NULL, NULL );
	rItem.bottom += 1; // For the 1-pixel divider between items.
	if( andAllBelowIt )
	{
		RECT rClient;
		GetClientRect( hPropGrid, &rClient );
		rItem.bottom = rClient.bottom;
	}
	InvalidateRect( hPropGrid, &rItem, TRUE );
}

/// Sets the vertical divider position (offset in pixels from the left edge of the control).
VOID	PropertyGrid_SetDividerPosition( HWND hPropGrid, INT newPos );
INT		PropertyGrid_GetDividerPosition( HWND hPropGrid );


HPGITEM	PropertyGrid_HitTest( HWND hPropGrid, POINT ptClientSpace, PGHITTESTINFO* pHitTestInfo );

// TODO: Move implementation to cpp.
/// Returns TRUE if the given key code is used by the property grid control for navigation. This allows derived
/// item types to pass relevant key strokes to the control.
inline BOOL PropertyGrid_IsNavigationKey( WPARAM keyCode )
{ return	keyCode == VK_DOWN || keyCode == VK_UP || keyCode == VK_RETURN ||
			keyCode == VK_ESCAPE || keyCode == VK_TAB;
}


/// This function is mostly used internally to send a notification message from the property grid control
/// to the parent window or to the registered notification callback.
LRESULT PropertyGrid_Notify( HWND hPropGrid, UINT code, HPGITEM hItem, LPVOID additionalInfo );


/// @name Data Accessors
/// @{

BOOL PropertyGrid_SetText( HWND hPropGrid, HPGITEM hItem, LPCTSTR sNewText, INT length = -1);

INT PropertyGrid_GetText( HWND hPropGrid, HPGITEM hItem, LPTSTR sBuffer, INT bufferSize, BOOL* pSuccess = NULL );

BOOL PropertyGrid_SetInt( HWND hPropGrid, HPGITEM hItem, INT n );

INT PropertyGrid_GetInt( HWND hPropGrid, HPGITEM hItem, BOOL* pSuccess = NULL );

BOOL PropertyGrid_SetFloat( HWND hPropGrid, HPGITEM hItem, FLOAT f );

FLOAT PropertyGrid_GetFloat( HWND hPropGrid, HPGITEM hItem, BOOL* pSuccess = NULL );

BOOL PropertyGrid_SetBool( HWND hPropGrid, HPGITEM hItem, BOOL b );

BOOL PropertyGrid_GetBool( HWND hPropGrid, HPGITEM hItem, BOOL* pSuccess = NULL );

/// @}



PGItemType* PropertyGrid_GetBlankType();
PGItemType* PropertyGrid_GetCategoryType();
PGItemType* PropertyGrid_GetEditType();
PGItemType* PropertyGrid_GetCheckboxType();
PGItemType* PropertyGrid_GetComboBoxType();
PGItemType* PropertyGrid_GetSliderType();
PGItemType* PropertyGrid_GetSpinBoxNumberType();
PGItemType* PropertyGrid_GetColorBoxType();
PGItemType* PropertyGrid_GetFileNameType();
PGItemType* PropertyGrid_GetFontType();


# define PGIT_BLANK			(PropertyGrid_GetBlankType())
# define PGIT_CATEGORY		(PropertyGrid_GetCategoryType())
# define PGIT_EDITBOX		(PropertyGrid_GetEditType())
# define PGIT_CHECKBOX		(PropertyGrid_GetCheckboxType())
# define PGIT_COMBOBOX		(PropertyGrid_GetComboBoxType())
# define PGIT_SLIDER		(PropertyGrid_GetSliderType())
# define PGIT_SPINBOXNUMBER	(PropertyGrid_GetSpinBoxNumberType())
# define PGIT_COLORBOX		(PropertyGrid_GetColorBoxType())
# define PGIT_FILENAME		(PropertyGrid_GetFileNameType())
# define PGIT_FONT			(PropertyGrid_GetFontType())




/// Insert a blank item. Useful for custom drawing.
inline HPGITEM PropertyGrid_InsertBlank( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent,
										 HPGITEM hInsertBefore, LPVOID pUserData )
{
	return PropertyGrid_InsertItem( hPropGrid, PropertyGrid_GetBlankType(), id, sTitle, hParent,
								 hInsertBefore, pUserData, NULL );
}


///////////////
// Category functions.

/// Inserts a category item. This is just a simple item with a darker background color and no content.
HPGITEM PropertyGrid_InsertCategory( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent,
									HPGITEM hInsertBefore, LPVOID pUserData );


HPGITEM PropertyGrid_InsertEdit( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent,
								HPGITEM hInsertBefore, LPVOID pUserData, LPCTSTR sInitialText );
HWND PGEdit_GetEdit( HWND hPropGrid, HPGITEM hItem );
BOOL PGEdit_SetMultiLine( HWND hPropGrid, HPGITEM hItem,
					BOOL bMultiLine, UINT maxVisibleLines, BOOL bAutoSize );





HPGITEM PropertyGrid_InsertFileName( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent,
									HPGITEM hInsertBefore, LPVOID pUserData, LPCTSTR sInitialText );
OPENFILENAME*	PGFileName_GetOFN( HWND hPropGrid, HPGITEM hItem );
HWND			PGFileName_GetEdit( HWND hPropGrid, HPGITEM hItem );
HWND			PGFileName_GetBrowseButton( HWND hPropGrid, HPGITEM hItem );





///////////////
// Color stuff.

/// Inserts a color property. Alpha values are not supported - insert an additional subitem to allow
/// the user to set alpha values separately.
HPGITEM PropertyGrid_InsertColor( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent,
								 HPGITEM hInsertBefore, LPVOID pUserData, COLORREF initialColor );

BOOL		PropertyGrid_SetColor( HWND hPropGrid, HPGITEM hColorItem, COLORREF color );
COLORREF	PropertyGrid_GetColor( HWND hPropGrid, HPGITEM hColorItem, BOOL* pSuccess = NULL );
HWND		PGColor_GetColorBox( HWND hPropGrid, HPGITEM hItem );





///////////////
// Combo box stuffs.

/// Inserts a combo box property item. Calling PropertyGrid_SetInt( INT n ) on a combo box item will set
/// the current selection, where n is the 0-based index of the selected item. To set the selection to the
/// item of a specified value, call PGCombo_SelectByValue( LPARAM value ); This will search for the first item
/// in the combo box which has the specified value. If no such item exists, the call will clear the selection (select -1).
/// Similarly, calling PropertyGrid_GetInt() will return the index of the selected item. To get the value associated
/// with the currently selected item, call PGCombo_GetSelectedValue().
HPGITEM PropertyGrid_InsertComboBox( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent,
								HPGITEM hInsertAfter, LPVOID pUserData );

INT		PGCombo_AddItem( HWND hPropGrid, HPGITEM hItem, LPCTSTR sText, LPARAM value );
VOID	PGCombo_RemoveItem( HWND hPropGrid, HPGITEM hItem, INT index );
INT		PGCombo_GetItemCount( HWND hPropGrid, HPGITEM hItem );
LPARAM	PGCombo_GetItemValue( HWND hPropGrid, HPGITEM hItem, INT itemIndex, BOOL* pSuccess = FALSE );
LPARAM	PGCombo_GetSelectedValue( HWND hPropGrid, HPGITEM hItem, BOOL* pbSuccess = FALSE );
HWND	PGCombo_GetComboBox( HWND hPropGrid, HPGITEM hItem ); ///< Returns the combo box control handle.
BOOL	PGCombo_SelectByValue( HWND hPropGrid, HPGITEM hItem, LPARAM value ); ///< See PropertyGrid_InsertComboBox().




/// Inserts a checkbox. 0 = unchecked, 1 = checked, 2 = indeterminate.
/// Use PropertyGrid_Get/SetBool() or PropertyGrid_Get/SetInt() to access the value.
/// Use PropertyGrid_Get/SetText() to access the caption.
/// TODO: Enable user to modify style flags somehow to support tri-state checkboxes.
HPGITEM PropertyGrid_InsertCheckbox( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent,
									HPGITEM hInsertBefore, LPVOID pUserData, LPCTSTR sCaption,
									BOOL bInitialValue );
HWND	PGCheckbox_GetCheckbox( HWND hPropGrid, HPGITEM hItem ); ///< Returns the checkbox control handle.






/// Slider stuff. You can set and get the position using PropertyGrid_SetInt() and PropertyGrid_GetInt().
/// NOTE: The slider will be created with the tooltips style flag set. If you don't want a tooltip, after
/// creating the slider item call SendMessage( PGSlider_GetSlider( hPropGrid, hItem ), TBM_SETTOOLTIPS, 0, 0 ).
HPGITEM PropertyGrid_InsertSlider( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent,
								HPGITEM hInsertBefore, LPVOID pUserData );

HPGITEM PropertyGrid_InsertSlider( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent,
								HPGITEM hInsertBefore, LPVOID pUserData,
								INT nMin, INT nMax, INT nPos,
								INT nSelMin = 0, INT nSelMax = 100, BOOL bEnableSelRange = FALSE );
VOID PGSlider_SetRange( HWND hPropGrid, HPGITEM hItem, INT nMin, INT nMax );
VOID PGSlider_GetRange( HWND hPropGrid, HPGITEM hItem, INT* pMin, INT* pMax );
VOID PGSlider_SetSel( HWND hPropGrid, HPGITEM hItem, INT nStart, INT nEnd );
VOID PGSlider_GetSel( HWND hPropGrid, HPGITEM hItem, INT* pStart, INT* pEnd );

VOID PGSlider_EnableSelRange( HWND hPropGrid, HPGITEM hSliderItem, BOOL bEnable );
BOOL PGSlider_IsSelRangeEnabled( HWND hPropGrid, HPGITEM hSliderItem );
HWND PGSlider_GetSlider( HWND hPropGrid, HPGITEM hItem ); ///< Returns the slider control handle.

inline HWND PGSlider_GetSliderToolTips( HWND hPropGrid, HPGITEM hItem )
{ return (HWND)SendMessage( PGSlider_GetSlider( hPropGrid, hItem ), TBM_GETTOOLTIPS, 0, 0 ); }

/// Can be used to disable tooltips by passing NULL. Useful for sliders used to edit floats.
inline VOID PGSlider_SetToolTips( HWND hPropGrid, HPGITEM hItem, HWND hToolTips )
{ SendMessage( PGSlider_GetSlider( hPropGrid, hItem ), TBM_SETTOOLTIPS, (WPARAM)hToolTips, 0 ); }

/// Sets the slider position in percentage, not absolute value. This effectively interpolates between min and max range values.
inline BOOL PGSlider_SetFloat01( HWND hPropGrid, HPGITEM hItem, FLOAT f )
{
	INT min = 0, max = 1;
	PGSlider_GetRange( hPropGrid, hItem, &min, &max );
	return PropertyGrid_SetInt( hPropGrid, hItem, (INT)(min + f * (max-min) ) );
}
/// Gets the slider position in percentage. Returns 0 if the slider position is at min, 1 if it's at max.
inline FLOAT PGSlider_GetFloat01( HWND hPropGrid, HPGITEM hItem, BOOL* pSuccess = NULL )
{
	INT min = 0, max = 1, n = 0;
	PGSlider_GetRange( hPropGrid, hItem, &min, &max );
	n = PropertyGrid_GetInt( hPropGrid, hItem, pSuccess );
	return (FLOAT)n / (min+max == 0 ? 1 : min+max);
}





/// SpinBox number. This is an edit control with a spin box next to it. Used to edit numbers (floats and ints).
HPGITEM PropertyGrid_InsertSpinBoxNumber( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent,
										 HPGITEM hInsertBefore, LPVOID pUserData );

/// For now, the parent can specify the effect of the spin box on the number by responding to PGN_PROPERTYCHANGING.
/// The "additionalInfo" member of the NMPROPGRID structure passed in lParam points to this structure to provide the
/// additional information required to allow the parent to implement the desired spinbox effect.
struct SBN_SPINDRAG
{
	FLOAT oldValue; ///< The old value.
	FLOAT spinFactor; ///< The spin factor.
	INT spinDelta; ///< Specifies the delta as supplied by the spin box control.
	FLOAT newValue; ///< The suggested new value, which is by default the old value + spinDelta * spinFactor.
};

VOID PGSpinBoxNumber_SetDragPixels( HWND hPropGrid, HPGITEM hSpinBoxNumberItem, UINT dragPixels );
VOID PGSpinBoxNumber_SetSpinFactor( HWND hPropGrid, HPGITEM hSpinBoxNumberItem, FLOAT factor );
FLOAT PGSpinBoxNumber_GetSpinFactor( HWND hPropGrid, HPGITEM hSpinBoxNumberItem ); ///< By default, this is 1.
VOID PGSpinBoxNumber_GetRange( HWND hPropGrid, HPGITEM hSpinBoxNumberItem, FLOAT* pMin, FLOAT* pMax );

/// Sets the valid range of values. If min is greater than or equal to max, range checking is disabled.
/// You can use INFINITY or -INFINITY with fMin or fMax to effectively set a limit in one direction and not the other.
VOID PGSpinBoxNumber_SetRange( HWND hPropGrid, HPGITEM hSpinBoxNumberItem, FLOAT fMin, FLOAT fMax );
HWND PGSpinBoxNumber_GetEdit( HWND hPropGrid, HPGITEM hSpinBoxNumberItem );
HWND PGSpinBoxNumber_GetSpinBox( HWND hPropGrid, HPGITEM hSpinBoxNumberItem );

/// Sets the format strings used to display numerical values to the user in the edit control.
/// The default values for sIntFormat and sFloatFormat are "%d" and "%g".
/// This function can be used to display units to the user.
/// For example: PGSpinBoxNumber_SetFormatStrings( hPG, hItem, "%d centimeters", "%.2f centimeters" );
/// The format string pointers are assumed to remain valid during the lifetime of the property grid item.
VOID PGSpinBoxNumber_SetFormatStrings( HWND hPropGrid, HPGITEM hItem, const TCHAR* sIntFormat, const TCHAR* sFloatFormat );



/// Inserts a font item. Font items allow the user to select a font. It uses the system's ChooseFont dialog.
inline HPGITEM PropertyGrid_InsertFont( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent,
	HPGITEM hPos, LPVOID userData )
{
	return PropertyGrid_InsertItem( hPropGrid, PGIT_FONT, id, sTitle, hParent, hPos, userData, 0 );
}


HFONT			PGFont_GetFont( HPGITEM hItem );
const LOGFONT*	PGFont_GetLogFont( HPGITEM hItem );
HFONT			PGFont_SetFont( HWND hPropGrid, HPGITEM hItem, LOGFONT* lf );





/// @} end group PropertyGrid

# endif // inclusion guard
