// This file implements the text property type for code-section's Property Grid control.

# include <PropertyGridCtrl.h>
# include <PropertyGridItem.h>
# include <CSTB\Win32\Win32Util.h>





class PGComboBoxItem : public PGItem
{
//protected:
public:
	HWND	m_hComboBox;
	INT		m_nSel;

public:
	PGComboBoxItem( PGItemType& type ) : PGItem( type )
	{
		m_hComboBox = NULL;
		m_nSel = -1;
	}



	virtual ~PGComboBoxItem() { GuiAssert( m_hComboBox == NULL ); }



	VOID OnInsert( HWND hPropGrid )
	{
		DWORD dwStyle = WS_CHILD | CBS_DROPDOWNLIST;

		m_hComboBox = CreateWindowEx( 0, WC_COMBOBOX, TEXT(""),
			dwStyle,
			0, 0, 10, 10, hPropGrid, NULL,
			(HINSTANCE)GetWindowLongPtr( hPropGrid, GWLP_HINSTANCE ),
			0 );
		if( !m_hComboBox )
			return; // FALSE
		SendMessage( m_hComboBox, WM_SETFONT, (LPARAM)SendMessage( hPropGrid, WM_GETFONT, 0, 0 ), 0 );
		SHORT defHeight = PropertyGrid_GetDefaultItemHeight( hPropGrid );
		PropertyGrid_SetItemHeight( hPropGrid, GetHandle(), MAX( defHeight, 22 ) );
		RECT rWindow = GetWindowRect( m_hComboBox );

# ifndef WIN32UTIL_VISUAL_STYLES
		SetWindowPos( m_hComboBox, 0, 0, 0, RectWidth(rWindow), 400, SWP_NOMOVE );
# endif
	}




	VOID OnRemove( HWND hPropGrid ) { SAFE_DESTROY_WINDOW( m_hComboBox ); }




	BOOL OnEdit( HWND hPropGrid )
	{
		// If the item is being edited not by being clicked with the mouse, drop down the list
		// and set focus to the control.
		if( !(GetAsyncKeyState(VK_LBUTTON)&0xF000) )
		{
			SetFocus( m_hComboBox );
			if( !SendMessage( m_hComboBox, CB_GETDROPPEDSTATE, 0, 0 ) )
				SendMessage( m_hComboBox, CB_SHOWDROPDOWN, TRUE, 0 );
		}
		return TRUE;
	}




	VOID OnSizeMove( HWND hPropGrid )
	{
		GuiAssert( m_hComboBox );
		RECT rContent;
		PropertyGrid_GetItemRects( hPropGrid, GetHandle(), NULL, &rContent, NULL, NULL, NULL, NULL );
		rContent.left += PG_PADDING_PIXELS_H;
		//InflateRect( &rContent, 0, -PG_PADDING_PIXELS_V );
		rContent.bottom -= 3;
		MoveWindow( m_hComboBox, rContent.left, rContent.top, RectWidth(rContent), RectHeight(rContent), TRUE );
	}




	VOID OnShow( HWND hPropGrid, BOOL bShow )
	{
		if( bShow )
			OnSizeMove( hPropGrid );

		ShowWindow( m_hComboBox, bShow ? SW_SHOW : SW_HIDE );
	}




	BOOL SetInt( HWND hPropGrid, INT n )
	{
		if( n >= PGCombo_GetItemCount( hPropGrid, GetHandle() ) || n < -1 )
			return FALSE; // TODO: Check if this test is really necessary.
		SendMessage( m_hComboBox, CB_SETCURSEL, (WPARAM)n, 0 );
		//m_nSel = n; // maybe the control will modify the value, like clamp to item count, so we use CB_GETCURSEL.
		m_nSel = SendMessage( m_hComboBox, CB_GETCURSEL, 0, 0 );
		return TRUE;
	}




	INT GetInt( HWND hPropGrid, BOOL& bSuccess ) const
	{
		if( !m_hComboBox )
			return -1;
		bSuccess = TRUE;
		//return SendMessage( m_hComboBox, CB_GETCURSEL, 0, 0 );
		return m_nSel;
	}




	BOOL GetBool( HWND hPropGrid, BOOL& bSuccess ) const { return (BOOL)GetInt( hPropGrid, bSuccess ); }
	BOOL SetBool( HWND hPropGrid, BOOL b ) { return SetInt( hPropGrid, (INT)b ); }


	INT GetText( HWND hPropGrid, LPTSTR sBuffer, INT bufferSize, BOOL& bSuccess ) const
	{
		if( !m_hComboBox || m_nSel < 0 )
			return PGItem::GetText( hPropGrid, sBuffer, bufferSize, bSuccess );

		int ret = SendMessage( m_hComboBox, CB_GETLBTEXT, m_nSel, (LPARAM)sBuffer );
		bSuccess = ret != CB_ERR;
		return ret;
	}




	LRESULT MsgProc( HWND hPropGrid, UINT Msg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
	{
		switch( Msg )
		{
		case WM_COMMAND:
			if( lParam != (LPARAM)m_hComboBox )
				break;
			bHandled = TRUE;

			// Only change the value on CBN_SELENDOK, not on CBN_SELCHANGE.
			if( HIWORD(wParam) == CBN_SELENDOK )
			{
				INT nCurSel = SendMessage( m_hComboBox, CB_GETCURSEL, 0, 0 );
				if( m_nSel == nCurSel )
					return 0;
				m_nSel = nCurSel;
				PropertyGrid_Notify( hPropGrid, PGN_PROPERTYCHANGED, GetHandle(), PG_FIRST_CHANGE );
				return 0;
			}

			// CBN_SELENDCANCEL is pretty much useless.
			if( HIWORD(wParam) == CBN_CLOSEUP )
			{
				INT nCurSel = SendMessage( m_hComboBox, CB_GETCURSEL, 0, 0 );
				if( m_nSel == nCurSel )
					return 0;
				//DebugPrintfLine( TEXT("Restoring original selection...") );
				SendMessage( m_hComboBox, CB_SETCURSEL, (WPARAM)m_nSel, 0 );
			}

			return 0;
		}
		return 0;
	}
};




PGItemTypeT< PGComboBoxItem > g_PGComboBoxItemType(TEXT("ComboBox") );




PGItemType* PropertyGrid_GetComboBoxType() { return &g_PGComboBoxItemType; }





HPGITEM PropertyGrid_InsertComboBox( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent,
								HPGITEM hInsertBefore, LPVOID pUserData )
{
	HPGITEM hItem = PropertyGrid_InsertItem( hPropGrid, g_PGComboBoxItemType.AllocateItem( hPropGrid, NULL ),
		id, sTitle, hParent, hInsertBefore, pUserData, NULL );
	return hItem;
}




INT PGCombo_AddItem( HWND hPropGrid, HPGITEM hItem, LPCTSTR sText, LPARAM value )
{
	PGComboBoxItem* pItem = (PGComboBoxItem*)hItem;
	if( !pItem->m_hComboBox )
		return -1;
	INT index = SendMessage( pItem->m_hComboBox, CB_ADDSTRING, 0, (LPARAM)sText );
	SendMessage( pItem->m_hComboBox, CB_SETITEMDATA, (WPARAM)index, value );
	return index;
}




VOID PGCombo_RemoveItem( HWND hPropGrid, HPGITEM hItem, INT index )
{
	PGComboBoxItem* pItem = (PGComboBoxItem*)hItem;
	if( !pItem->m_hComboBox ) return;
	SendMessage( pItem->m_hComboBox, CB_DELETESTRING, (WPARAM)index, 0 );
}




LPARAM PGCombo_GetItemValue( HWND hPropGrid, HPGITEM hItem, INT itemIndex, BOOL* pSuccess )
{
	PGComboBoxItem* pItem = (PGComboBoxItem*)hItem;
	if( itemIndex < 0 || itemIndex >= PGCombo_GetItemCount( hPropGrid, hItem ) )
	{
		if( pSuccess )
			*pSuccess = FALSE;
		return 0;
	}
	if( pSuccess )
		*pSuccess = TRUE;
	return SendMessage( pItem->m_hComboBox, CB_GETITEMDATA, (WPARAM)itemIndex, 0 );
}




LPARAM PGCombo_GetSelectedValue( HWND hPropGrid, HPGITEM hItem, BOOL* pSuccess )
{
	PGComboBoxItem* pItem = (PGComboBoxItem*)hItem;
	BOOL bSuccess = FALSE;
	INT index = PropertyGrid_GetInt( hPropGrid, hItem, &bSuccess );
	if( bSuccess )
		return PGCombo_GetItemValue( hPropGrid, hItem, index, pSuccess );
	if( pSuccess ) *pSuccess = FALSE;
	return 0;
}



HWND PGCombo_GetComboBox( HWND hPropGrid, HPGITEM hItem )
{
	PGComboBoxItem* pItem = (PGComboBoxItem*)hItem;
	return pItem->m_hComboBox;
}




BOOL PGCombo_SelectByValue( HWND hPropGrid, HPGITEM hItem, LPARAM value )
{
	INT itemCount = PGCombo_GetItemCount( hPropGrid, hItem );
	for( INT i=0; i<itemCount; i++ )
		if( PGCombo_GetItemValue( hPropGrid, hItem, i ) == value  )
			return PropertyGrid_SetInt( hPropGrid, hItem, i );
	return PropertyGrid_SetInt( hPropGrid, hItem, -1 );
}




INT PGCombo_GetItemCount( HWND hPropGrid, HPGITEM hItem )
{
	PGComboBoxItem* pItem = (PGComboBoxItem*)hItem;
	return SendMessage( pItem->m_hComboBox, CB_GETCOUNT, 0, 0 );
}