// Property grid control implementation file. See PropertyGridCtrl.h.
// Implementation uses pure win32.

# include "PropertyGridCtrlImpl.h"
# include <strsafe.h>



PGGlobals pgGlobals;




LRESULT PropertyGrid_Notify( HWND hPropGrid, UINT code, HPGITEM hItem, LPVOID additionalInfo )
{
	NMPROPGRID nm = {0};
	nm.hdr.code = code;
	nm.hdr.hwndFrom = hPropGrid;
	nm.hdr.idFrom = GetWindowLong( hPropGrid, GWL_ID );
	nm.hItem = hItem;
	nm.idItem = PropertyGrid_GetItemID( hItem );
	nm.additionalInfo = additionalInfo;

	PGInfo* pg = GetControlData( hPropGrid );
	if( pg->pfNotify )
		return pg->pfNotify( hPropGrid, &nm, pg->pfNotifyData );

	HWND hNotifyWindow = GetParent( hPropGrid );
	return SendMessage( hNotifyWindow, WM_NOTIFY, (WPARAM)nm.hdr.idFrom, (LPARAM)&nm );
}



void PropertyGrid_SetNotificationCallback( HWND hPropGrid, PGNotificationCallback cb, void* userData )
{
	PGInfo* pg = GetControlData( hPropGrid );
	pg->pfNotify = cb;
	pg->pfNotifyData = userData;
}




///////////////////////////////////////////////////////////////////
// Gets the actual rectangles of the item.
VOID PropertyGrid_GetItemRects( HWND hPropGrid, HPGITEM hItem, RECT* prcItem,
							   RECT* prcContent, RECT* prcTitle, RECT* prcGlyph,
							   RECT* prcText, RECT* prcMenuArrow )
{
	if( !PropertyGrid_IsItemVisible( hPropGrid, hItem ) )
	{
		if( prcItem )		ResetRect( prcItem );
		if( prcContent )	ResetRect( prcContent );
		if( prcTitle )		ResetRect( prcTitle );
		if( prcGlyph )		ResetRect( prcGlyph );
		if( prcText )		ResetRect( prcText );
		if( prcMenuArrow )	ResetRect( prcMenuArrow );
		return;
	}

	if( PropertyGrid_IsItemVisible( hPropGrid, hItem ) && !PropertyGrid_GetFirstVisible( hPropGrid ) )
	{
		DebugPrintf( "WPG: Invalid control state" );
		return;
	}


	PGInfo* pPGI = GetControlData( hPropGrid );

	RECT rClient;
	GetClientRect( hPropGrid, &rClient );

	// Determine the y offset to compensate for scrolling.
	SCROLLINFO si = GetScrollInfo( hPropGrid );
	INT yOffset = si.nPos;
	BOOL bHasContent = CONV(hItem)->HasContent( hPropGrid );

	RECT rItem, rContent, rTitle, rGlyph, rText, rMenuArrow;
	SetRect( &rItem, rClient.left, CONV(hItem)->GetPos(), rClient.right, CONV(hItem)->GetPos() + CONV(hItem)->GetHeight() );
	OffsetRect( &rItem, 0, -yOffset );


	UINT levelPixels = PropertyGrid_GetItemLevel( hPropGrid, hItem ) * PG_INDENTATION_PIXELS;

	SetRect( &rGlyph, levelPixels, rItem.top, levelPixels, rItem.top );
	if( PropertyGrid_ItemHasChildren( hPropGrid, hItem ) )
	{
		rGlyph.right += PG_INDENTATION_PIXELS;
		rGlyph.bottom += RECTHEIGHT(rItem);
	}


	INT nDivider = 0;
	if( bHasContent )
		nDivider = pPGI->nDividerPos; // Item has content, use control's divider position.
	else
		nDivider = rItem.right; // Item only has a title, set the divider to the rightmost point.

	SetRect( &rContent, nDivider+1, rItem.top, rClient.right, rItem.bottom );
	SetRect( &rTitle, rClient.left, rItem.top, nDivider, rItem.bottom );
	//if( !bHasContent )
		//rTitle.right += 1; // Item doesn't use a divider - don't leave a pixel for the divider and include it in the title.

	// Calculate the menu arrow rectangle.
	if( PropertyGrid_GetItemMenu( hItem, NULL ) )
	{
		unsigned arrowButtonSize = 12;
		rMenuArrow = rTitle;
		rMenuArrow.left = rMenuArrow.right - arrowButtonSize;
		OffsetRect( &rMenuArrow, -PG_PADDING_PIXELS_H, 0 );
	}
	else
		SetRect( &rMenuArrow, rTitle.right, rTitle.top, rTitle.right, rTitle.bottom );


	SetRect( &rText, rGlyph.right + PG_PADDING_PIXELS_H, rItem.top + PG_PADDING_PIXELS_V, rMenuArrow.left, rItem.bottom );

	if( prcItem ) *prcItem = rItem;
	if( prcContent ) *prcContent = rContent;
	if( prcTitle ) *prcTitle = rTitle;
	if( prcGlyph ) *prcGlyph = rGlyph;
	if( prcText ) *prcText = rText;
	if( prcMenuArrow ) *prcMenuArrow = rMenuArrow;
}






///////////////////////////////////////////////////////////////////
// Do a hit test. The point is assumed to be in client coordinates.
// Returns ....
//
HPGITEM PropertyGrid_HitTest( HWND hPropGrid, POINT pt, PGHITTESTINFO* pHTI )
{
	PGInfo* pPGI = GetControlData( hPropGrid );

	PGHITTESTINFO htiLocal;
	PGHITTESTINFO& hti = pHTI ? *pHTI : htiLocal;

	// Initialize to "nothing".
	hti.flags = PGHT_NOWHERE;
	hti.hItem = NULL;

	RECT rClient;
	GetClientRect( hPropGrid, &rClient );

	if( !PtInRect( &rClient, pt ) )
		return NULL;

	
	for( HPGITEM hItem = PropertyGrid_GetFirstVisible( hPropGrid );
		hItem != NULL;
		hItem = PropertyGrid_GetNextVisible( hPropGrid, hItem ) )
	{
		RECT rItem, rTitle, rContent, rGlyph, rMenuArrow;
		PropertyGrid_GetItemRects( hPropGrid, hItem, &rItem, &rContent, &rTitle, &rGlyph, NULL, &rMenuArrow );

		if( rItem.top > pt.y )
			break;

		if( rItem.bottom < pt.y )
			continue;

		hti.hItem = hItem;

		// Extend the item rectangles one pixel down to include the horizontal dividing line.
		rItem.bottom += 1;
		rContent.bottom += 1;
		rTitle.bottom += 1;

		if( CONV(hItem)->HasContent( pPGI->hWnd ) )
		{
			// First, check if the point is on the vertical divider if the item has one.
			INT dx = pt.x - pPGI->nDividerPos;
			dx = dx >= 0 ? dx : -dx;
			if( dx <= PG_DIVIDER_THICKNESS )
			{
				hti.flags = PGHT_ONDIVIDER;
				return hItem;
			}
		}

		if( PtInRect( &rTitle, pt ) )
		{
			hti.flags = PGHT_ONITEMTITLE;
			if( PtInRect( &rGlyph, pt ) )
				hti.flags |= PGHT_ONITEMGLYPH;
			else if( PtInRect( &rMenuArrow, pt ) )
				hti.flags |= PGHT_ONITEMMENU;
		}
		else if( PtInRect( &rContent, pt ) )
			hti.flags = PGHT_ONITEMCONTENT;
		else
			OutputDebugString( TEXT(__FUNCTION__)TEXT( ": Unexpected hit test condition. Debug." ) );

		return hItem;
	}

	return NULL;
}





/////////////////////////////////////////////
// This is called when an item is inserted, removed, collapsed, or resized. This will also be called when
// per-item visibility flags are implemented. This recalculates the positions of the visible items after
// the specified item. The specified item is assumed to be visible. hStart can be NULL to indicate all
// visible item positions should be calculated.
VOID CalculatePositionsBetween( HWND hPropGrid, HPGITEM hStart, HPGITEM hEnd = NULL,
	BOOL bIncludeStart = FALSE, BOOL bOnShow = FALSE )
{
	if( !hStart || (hStart && !PropertyGrid_IsItemVisible( hPropGrid, hStart )) )
		return; // Starting item is assumed to be visible. If it's not, there is no need to do anything.

	HPGITEM hPrev, hItem;
	if( bIncludeStart )
	{
		hPrev = PropertyGrid_GetPrevVisible( hPropGrid, hStart );
		hItem = hStart;
	}
	else
	{
		hPrev = hStart;
		hItem = PropertyGrid_GetNextVisible( hPropGrid, hStart );
	}

	while( hItem != hEnd )
	{
		SHORT y = hPrev ? (CONV(hPrev)->GetPos() + CONV(hPrev)->GetHeight() + 1) : 0;
		if( bOnShow )
		{
			CONV(hItem)->SetPos( y );
			PropertyGrid_InvalidateItem( hPropGrid, hItem );
			GetControlData( hPropGrid )->numVisibleItems++;
			CONV(hItem)->OnShow( hPropGrid, TRUE );
		}
		else
		{
			if( CONV(hItem)->GetPos() != y )
			{
				// Invalidate both the old rectangle and the new rectangle.
				// TODO: See if invalidating the old rectangle is really necessary.
				PropertyGrid_InvalidateItem( hPropGrid, hItem );
				CONV(hItem)->SetPos( y );
				PropertyGrid_InvalidateItem( hPropGrid, hItem );
				CONV(hItem)->OnSizeMove( hPropGrid );
			}
		}
		hPrev = hItem;
		hItem = PropertyGrid_GetNextVisible( hPropGrid, hPrev );
	}
}





VOID RecalculateScrollInfo( HWND hPropGrid )
{
	HPGITEM hLast = PropertyGrid_GetFirstVisible( hPropGrid );
	HPGITEM hNV;
	while( (hNV = PropertyGrid_GetNextVisible( hPropGrid, hLast ) ) )
	{
		hLast = hNV;
	}
	int maxY = 0;
	if( hLast )
		maxY = CONV(hLast)->GetPos() + CONV(hLast)->GetHeight();

	PGInfo* pPGI = GetControlData( hPropGrid );
	SCROLLINFO si = {0};
	si.cbSize = sizeof(SCROLLINFO);
	si.fMask = SIF_PAGE | SIF_POS | SIF_RANGE;
	GetScrollInfo( hPropGrid, SB_VERT, &si );
	si.nMin = 0;
	si.nMax = maxY;
	si.nPage = RectHeight( GetClientRect( hPropGrid ) );
	SetScrollInfo( hPropGrid, SB_VERT, &si, TRUE );
}




BOOL PropertyGrid_ItemHasChildren( HWND hPropGrid, HPGITEM hItem )
{
	// TODO: Ask the parent if the item has a flag specifying I_CHILDRENCALLBACK or something.
	return PropertyGrid_GetChild( hItem ) != NULL;
}




UINT PropertyGrid_GetItemHeight( HWND hPropGrid, HPGITEM hItem ) { return CONV(hItem)->GetHeight(); }

VOID PropertyGrid_SetItemHeight( HWND hPropGrid, HPGITEM hItem, UINT height )
{
	PropertyGrid_InvalidateItem( hPropGrid, hItem );
	CONV(hItem)->SetHeight( height );
	if( PropertyGrid_IsItemVisible( hPropGrid, hItem ) )
	{
		CalculatePositionsBetween( hPropGrid, hItem, NULL, FALSE, FALSE );
		PropertyGrid_InvalidateItem( hPropGrid, hItem );
		CONV(hItem)->OnSizeMove( hPropGrid );
	}
}



SHORT PropertyGrid_GetDefaultItemHeight( HWND hPropGrid )
{ return GetControlData( hPropGrid )->lineHeight; }






HPGITEM PropertyGrid_InsertItem( HWND hPropGrid, PGItemType* pType, UINT id, LPCTSTR sTitle,
	HPGITEM hParent, HPGITEM hInsertBefore, LPVOID pUserData,
	LPVOID pCreationData )
{
	return PropertyGrid_InsertItem( hPropGrid,
		pType->AllocateItem( hPropGrid, pCreationData ),
		id, sTitle, hParent, hInsertBefore, pUserData, pCreationData );
}




HPGITEM PropertyGrid_InsertItem( HWND hPropGrid, PGItem* pItem,
								UINT id, LPCTSTR sTitle,
								HPGITEM hParent, HPGITEM hInsertBefore,
								LPVOID pUserData, LPVOID pCreationData )
{
	PGInfo* pPGI = GetControlData( hPropGrid );
	HPGITEM hItem = CONV(pItem);

	// Ensure the InsertBefore item is indeed a child of the specified parent.
	if( hInsertBefore && PropertyGrid_GetParent( hInsertBefore ) != hParent )
		hInsertBefore = NULL;

	// If the item has a parent and the parent has no children, update it to add [+] to it.
	if( hParent )
	{
		if( !PropertyGrid_ItemHasChildren( hPropGrid, hParent ) )
			PropertyGrid_InvalidateItem( hPropGrid, hParent );
	}

	pPGI->items.Add( pItem, CONV(hParent), CONV(hInsertBefore ) );
	
	pItem->SetState( pItem->GetState() | PGIS_HIDDEN );

	// TODO: This is inconsistent! Setting the item ID, user data, and title should be done
	// the same way that the item height and position are set.
	PropertyGrid_SetItemID( hItem, id );
	PropertyGrid_SetItemUserData( hItem, pUserData );
	PropertyGrid_SetItemTitle( hPropGrid, hItem, sTitle, FALSE );
	pItem->SetHeight( PropertyGrid_GetDefaultItemHeight( hPropGrid ) );

	pItem->OnInsert( hPropGrid );
	PropertyGrid_Notify( hPropGrid, PGN_INSERTITEM, hItem, NULL );

	PropertyGrid_ShowItem( hPropGrid, hItem, TRUE );

	return hItem;
}




////////////////////////////////
// Removes an item and all descendents (children, grandchildren... etc).
//
VOID PropertyGrid_RemoveItem( HWND hPropGrid, HPGITEM hItem, BOOL bUpdate )
{
	if( hItem == NULL ) return;

	PGInfo* pPGI = GetControlData( hPropGrid );
	PGItem* pItem = CONV(hItem);

	HPGITEM hParent = PropertyGrid_GetParent( hItem );

	PropertyGrid_ShowItem( hPropGrid, hItem, FALSE, FALSE );

	PropertyGrid_Notify( hPropGrid, PGN_DELETEITEM, hItem, NULL );

	// Delete all children first.
	while( PropertyGrid_GetChild( hItem ) )
		PropertyGrid_RemoveItem( hPropGrid, PropertyGrid_GetChild( hItem ), FALSE );

	pItem->OnRemove( hPropGrid );
	pPGI->items.Remove( pItem );
	pItem->GetType().DeleteItem( hPropGrid, pItem );

	if( bUpdate )
		UpdateWindow( hPropGrid );
}





////////////////////////////////
// Removes all items in the control.
//
VOID PropertyGrid_Clear( HWND hPropGrid, BOOL bUpdate )
{
	if( !hPropGrid || PropertyGrid_GetFirstItem( hPropGrid ) == NULL )
		return;
	while( PropertyGrid_GetFirstItem( hPropGrid ) )
		PropertyGrid_RemoveItem( hPropGrid, PropertyGrid_GetFirstItem( hPropGrid ), FALSE );
	InvalidateRect( hPropGrid, NULL, TRUE );
	if( bUpdate )
		UpdateWindow( hPropGrid );
}




//////////////////////////////////////////////////////
//
BOOL PropertyGrid_SetFocusItem( HWND hPropGrid, HPGITEM hItem, BOOL bForce, BOOL bUpdate )
{
	PGInfo* pPGI = GetControlData( hPropGrid );
	HPGITEM hOldFocus = PropertyGrid_GetFocusItem( hPropGrid );
	PGItem* pOldFocus = CONV( hOldFocus );

	if( hItem == hOldFocus )
		return TRUE;

	//if( hItem && !PropertyGrid_IsItemVisible( hPropGrid, hItem ) )
	//	return FALSE; // The item has to be visible.
	PropertyGrid_EnsureItemVisible( hPropGrid, hItem );


	// Notify the parent that the focus is going to change, and don't change it if it returns nonzero.
	if( PropertyGrid_Notify( hPropGrid, PGN_FOCUSCHANGING, hItem, NULL ) )
	{
		// Parent doesn't want us to change focus.
		if( !bForce )
			return FALSE;
	}


	// End editing if the previous focus item is editing.
	BOOL bRet = PropertyGrid_EndEdit( hPropGrid, bForce );
	if( !bRet && !bForce )
		return FALSE;

	// Now set the focus to the new item.
	pPGI->hFocusItem = hItem;

	// TODO: Add a bNotify parameter so that the parent is not notified if the focus changed
	// was instigated by the API and not by the user.
	PropertyGrid_Notify( hPropGrid, PGN_FOCUSCHANGED, hItem, NULL );

	if( hOldFocus ) PropertyGrid_InvalidateItem( hPropGrid, hOldFocus );
	if( hItem )		PropertyGrid_InvalidateItem( hPropGrid, hItem );

	if( bUpdate )
		UpdateWindow( hPropGrid );

	return TRUE;
}




BOOL PropertyGrid_IsEditing( HWND hPropGrid ) { return GetControlData( hPropGrid )->bEditing; }



BOOL InternalEndEdit( HWND hPropGrid, BOOL bForce, BOOL bCancel )
{
	if( !PropertyGrid_IsEditing( hPropGrid ) )
		return TRUE;
	HPGITEM hItem = PropertyGrid_GetFocusItem( hPropGrid );
	if( !hItem )
	{
		OutputDebugString( TEXT( "PropertyGrid control logic error! - control is editing with no focus item." ) );
		return FALSE;
	}

	CONV(hItem)->OnEndEdit( hPropGrid, bCancel );

	GetControlData( hPropGrid )->bEditing = FALSE;

	PropertyGrid_Notify( hPropGrid, bCancel ? PGN_EDITCANCEL : PGN_EDITDONE, hItem, NULL );
	
	return TRUE;
}






BOOL PropertyGrid_EndEdit( HWND hPropGrid, BOOL bForce ) { return InternalEndEdit( hPropGrid, bForce, FALSE ); }
BOOL PropertyGrid_CancelEdit( HWND hPropGrid, BOOL bForce ) { return InternalEndEdit( hPropGrid, bForce, TRUE ); }





BOOL PropertyGrid_EditItem( HWND hPropGrid, HPGITEM hItem, BOOL bForce )
{
	if( !hItem )
		return FALSE;

	if( PropertyGrid_GetFocusItem( hPropGrid ) == hItem && PropertyGrid_IsEditing( hPropGrid ) )
		return TRUE; // Item is already editing. TODO: Consider restarting editing.

	BOOL bRet = PropertyGrid_SetFocusItem( hPropGrid, hItem, bForce );
	if( !bRet && !bForce )
		return bRet;

	if( !PropertyGrid_IsItemEnabled( hItem ) )
		return FALSE;

	if( !CONV(hItem)->OnEdit( hPropGrid ) )
		return FALSE; // Even if bForce is TRUE, some items are not editable.

	GetControlData( hPropGrid )->bEditing = TRUE;

	PropertyGrid_Notify( hPropGrid, PGN_EDITSTART, hItem, NULL );

	return bRet;
}






VOID DrawHierarchyLines( PGInfo* pPGI, HDC hdc )
{
	HPEN hOldPen = (HPEN)SelectObject( hdc, pgGlobals.hHierarchyPen );
	INT oldBkMode = SetBkMode( hdc, TRANSPARENT );

	HWND hPropGrid = pPGI->hWnd;
	HPGITEM hItem = PropertyGrid_GetFirstVisible( pPGI->hWnd );
	while( hItem )
	{
		HPGITEM hChild = PropertyGrid_GetChild( hItem );
		if( hChild && 0 == (PropertyGrid_GetItemState( hItem ) & PGIS_COLLAPSED ) )
		{
			UINT level = PropertyGrid_GetItemLevel( hPropGrid, hItem );
			INT x = level * PG_INDENTATION_PIXELS + PG_INDENTATION_PIXELS/2;
			RECT rItem;
			PropertyGrid_GetItemRects( hPropGrid, hItem, &rItem, NULL, NULL, NULL, NULL, NULL );
			INT y = rItem.bottom;
			INT lastY = y;
			while( hChild )
			{
				RECT rChild;
				PropertyGrid_GetItemRects( hPropGrid, hChild, &rChild, NULL, NULL, NULL, NULL, NULL );
				lastY = rChild.top + RECTHEIGHT(rChild)/2;
				if( (lastY - y) % 2 )
					lastY -= 1;
				MoveToEx( hdc, x, lastY, NULL );
				LineTo( hdc, x + PG_INDENTATION_PIXELS/2, lastY );
				hChild = ::PropertyGrid_GetNextSibling( hChild );
			}
			MoveToEx( hdc, x, y, NULL );
			LineTo( hdc, x, lastY );
		}
		hItem = PropertyGrid_GetNextVisible( hPropGrid, hItem );
	}

	SelectObject( hdc, hOldPen );
	SetBkMode( hdc, oldBkMode );
}





// This is called by the WM_PAINT (and WM_PRINT??) handler to paint the control.
VOID PaintPropertyGrid( PGInfo* pPGI, HDC hDC, RECT* prcPaint )
{
	RECT rClient;//, rIntersection;
	HWND hPropGrid = pPGI->hWnd;

	GetClientRect( pPGI->hWnd, &rClient );

	if( IsRectEmpty( &rClient ) )
		return;

	if( PropertyGrid_GetFirstVisible( hPropGrid ) == NULL )
	{
		// Then it's empty. Nothing to draw.
		FillRect( hDC, prcPaint, (HBRUSH)(COLOR_3DFACE+1) );
		return;
	}

	// Select the window's font into the hdc so that text is drawn using that font by default.
	HGDIOBJ hOldFont = NULL;
	if( pPGI->hFont )
		hOldFont = SelectObject( hDC, pPGI->hFont );
	HPEN hOldPen = (HPEN)SelectObject( hDC, pgGlobals.hDividerPen );

	HPGITEM hLast = NULL;
	for( HPGITEM hItem = PropertyGrid_GetFirstVisible( hPropGrid );
		hItem != NULL;
		hItem = PropertyGrid_GetNextVisible( hPropGrid, hItem ) )
	{
		RECT rItem, rContent, rTitle, rGlyph, rText, rMenuArrow;
		PropertyGrid_GetItemRects( hPropGrid, hItem, &rItem, &rContent, &rTitle, &rGlyph, &rText, &rMenuArrow );

		if( rItem.bottom < 0 )
			continue;
		if( rItem.top > rClient.bottom )
			break;

		BOOL bHasContent = CONV(hItem)->HasContent( hPropGrid );

		if( bHasContent )
			CONV(hItem)->PaintContent( hPropGrid, hDC, prcPaint, &rContent );

		if( !IsRectEmpty( &rTitle ) )
		{
			CONV(hItem)->PaintTitleBG( hPropGrid, hDC, prcPaint, &rTitle );
			CONV(hItem)->PaintText( hPropGrid, hDC, prcPaint, &rText );

			// Now draw expand/collapse glyph.
			if( PropertyGrid_ItemHasChildren( hPropGrid, hItem ) )
				CONV(hItem)->PaintExpandGlyph( hPropGrid, hDC, &rGlyph );

			// Now draw the menu's drop arrow.
			if( PropertyGrid_GetItemMenu( hItem, NULL ) )
				CONV(hItem)->PaintMenuArrow( hPropGrid, hDC, &rMenuArrow );
		}

		INT y = rItem.bottom;
		MoveToEx( hDC, 0, y, NULL );
		LineTo( hDC, rClient.right, y );

		if( bHasContent )
		{
			MoveToEx( hDC, pPGI->nDividerPos, rItem.top, NULL );
			LineTo( hDC, pPGI->nDividerPos, rItem.bottom );
		}

		hLast = hItem;
	}

	if( hLast )
	{
		RECT rLast;
		PropertyGrid_GetItemRects( hPropGrid, hLast, &rLast, NULL, NULL, NULL, NULL, NULL );
		SetRect( &rLast, rClient.left, rLast.bottom+1, rClient.right, rClient.bottom );
		FillRect( hDC, &rLast, (HBRUSH)(COLOR_3DFACE+1) );
	}

	
	if( pgGlobals.hHierarchyPen )
		DrawHierarchyLines( pPGI, hDC );

	// Restore device context.
	if( hOldFont )
		SelectObject( hDC, hOldFont );
	SelectObject( hDC, hOldPen );
}





/////////////////////////////////////////////////////////////
// Functions to operate the control.



//
// Fills a structure with information about an item.
VOID PropertyGrid_GetItemInfo( HWND hPropGrid, HPGITEM hItem, LPPGITEMINFO pItemInfo )
{
	PGItem* pItem = CONV(hItem);

	pItemInfo->pType = &pItem->GetType();
	pItemInfo->id = pItem->GetID();
	pItemInfo->hParent = PropertyGrid_GetParent( hItem );;
	pItemInfo->hChild = PropertyGrid_GetChild( hItem );
	pItemInfo->hNextSibling = PropertyGrid_GetNextItem( hItem);
	pItemInfo->hPrevSibling = PropertyGrid_GetPrevItem( hItem);
	pItemInfo->hMenu = pItem->GetMenu( &pItemInfo->menuShared );
	pItemInfo->pUserData = pItem->GetUserData();
	pItemInfo->level = PropertyGrid_GetItemLevel( hPropGrid, hItem );
	pItemInfo->sTitle = PropertyGrid_GetItemTitle( hPropGrid, hItem );
}




PGItemType* PropertyGrid_GetItemType( HPGITEM hItem ) { return &CONV(hItem)->GetType(); }
DWORD	PropertyGrid_GetItemState( HPGITEM hItem ) { return CONV(hItem)->GetState(); }
LPVOID	PropertyGrid_GetItemUserData( HPGITEM hItem ) { return CONV(hItem)->GetUserData(); }
UINT	PropertyGrid_GetItemID( HPGITEM hItem ) { return hItem ? CONV(hItem)->GetID() : 0; }
HMENU	PropertyGrid_GetItemMenu( HPGITEM hItem, BOOL* pbShared ) { return CONV(hItem)->GetMenu( pbShared ); }
UINT	PropertyGrid_GetItemLevel( HWND hWnd, HPGITEM hItem )
{
	UINT level = 1;
	for( HPGITEM hParent = PropertyGrid_GetParent( hItem );
		hParent != NULL;
		hParent = PropertyGrid_GetParent( hParent ) )
		level++;
	return level;
}


VOID PropertyGrid_SetItemState( HPGITEM hItem, DWORD dwNewState )
{ CONV(hItem)->SetState( dwNewState ); }


VOID PropertyGrid_SetItemTitle( HWND hPropGrid, HPGITEM hItem, LPCTSTR sNewTitle, BOOL bUpdate )
{
	CONV(hItem)->SetTitle( hPropGrid, sNewTitle );
	if( bUpdate )
		UpdateWindow( hPropGrid );
}



LPCTSTR PropertyGrid_GetItemTitle( HWND hPropGrid, HPGITEM hItem )
{ return CONV(hItem)->GetTitle( hPropGrid ); }

VOID PropertyGrid_SetItemUserData( HPGITEM hItem, LPVOID pNewUserData )
{ CONV(hItem)->SetUserData( pNewUserData ); }

VOID PropertyGrid_SetItemID( HPGITEM hItem, UINT newID )
{ CONV(hItem)->SetID( newID ); }

VOID PropertyGrid_SetItemMenu( HWND hPropGrid, HPGITEM hItem, HMENU hMenu, BOOL bShared )
{ CONV(hItem)->SetMenu( hPropGrid, hMenu, bShared ); }


INT PropertyGrid_GetNumVisibleItems( HWND hPropGrid )
{ return GetControlData( hPropGrid )->numVisibleItems; }


//...
HPGITEM PropertyGrid_GetFirstItem( HWND hPropGrid )
{ return CONV(GetControlData( hPropGrid )->items.First()); }


//...
HPGITEM PropertyGrid_GetParent( HPGITEM hItem )
{ return CONV( PGItemTree::Parent( CONV(hItem) )); }

HPGITEM	PropertyGrid_GetChild( HPGITEM hItem )
{ return CONV( PGItemTree::FirstChild( CONV(hItem) ) ); }

HPGITEM	PropertyGrid_GetNextSibling( HPGITEM hItem )
{ return CONV( PGItemTree::Next( CONV(hItem) ) ); }

HPGITEM	PropertyGrid_GetPrevSibling( HPGITEM hItem )
{ return CONV( PGItemTree::Prev( CONV(hItem ) ) ); }

HPGITEM PropertyGrid_GetNextItem( HPGITEM hItem, bool bSkipChildren )
{ return CONV( PGItemTree::LogicalNext( CONV(hItem), bSkipChildren ) ); }

HPGITEM PropertyGrid_GetPrevItem( HPGITEM hItem )
{ return CONV( PGItemTree::LogicalPrev( CONV(hItem) ) ); }


// Assumes hItem is visible or NULL.
HPGITEM PropertyGrid_GetNextItem_SkipCollapsed( HWND hPropGrid, HPGITEM hItem, BOOL bSkipChildren = FALSE )
{
	if( hItem == NULL )
		return PropertyGrid_GetFirstItem( hPropGrid );

	if( !bSkipChildren )
		if( PropertyGrid_GetChild( hItem ) && 0 == (PropertyGrid_GetItemState(hItem) & PGIS_COLLAPSED ) )
			return PropertyGrid_GetChild( hItem );

	while( hItem )
	{
		if( PropertyGrid_GetNextSibling( hItem ) )
			return PropertyGrid_GetNextSibling( hItem );
		hItem = PropertyGrid_GetParent( hItem );
	}
	return NULL;
}



HPGITEM PropertyGrid_GetNextVisible( HWND hPropGrid, HPGITEM hItem, BOOL bSkipChildren )
{
	HPGITEM hRet = PropertyGrid_GetNextItem_SkipCollapsed( hPropGrid, hItem, bSkipChildren );
	while( hRet && !PropertyGrid_IsItemVisible( hPropGrid, hRet ) )
		hRet = PropertyGrid_GetNextItem_SkipCollapsed( hPropGrid, hRet, bSkipChildren );

	return hRet;
}



HPGITEM PropertyGrid_GetPrevItem_SkipCollapsed( HWND hPropGrid, HPGITEM hItem )
{
	if( hItem == NULL )
		return PropertyGrid_GetFirstItem( hPropGrid );

	if( PropertyGrid_GetPrevItem( hItem ) )
	{
		HPGITEM hPrev = PropertyGrid_GetPrevItem( hItem );
		while( HPGITEM hNext = PropertyGrid_GetNextVisible( hPropGrid, hPrev ) )
		{
			if( hNext == hItem )
				return hPrev;
			hPrev = hNext;
		}
	}
	return PropertyGrid_GetParent( hItem );
}




HPGITEM PropertyGrid_GetPrevVisible( HWND hPropGrid, HPGITEM hItem )
{
	HPGITEM hRet = PropertyGrid_GetPrevItem_SkipCollapsed( hPropGrid, hItem );
	while( hRet && !PropertyGrid_IsItemVisible( hPropGrid, hRet ) )
		hRet = PropertyGrid_GetPrevItem_SkipCollapsed( hPropGrid, hRet );

	return hRet;
}




HPGITEM PropertyGrid_FindItemByID( HWND hPropGrid, UINT ID, HPGITEM hParent )
{
	HPGITEM hFirst, hLast;
	if( hParent )
	{
		hFirst = PropertyGrid_GetNextItem( hParent );
		hLast = PropertyGrid_GetNextItem( hParent, TRUE );
	}
	else
	{
		hFirst = PropertyGrid_GetFirstItem( hPropGrid );
		hLast = NULL;
	}
	for( HPGITEM hItem = hFirst;
		hItem != NULL && hItem != hLast;
		hItem = PropertyGrid_GetNextItem( hItem ) )
	{
		if( PropertyGrid_GetItemID( hItem ) == ID )
			return hItem;
	}
	return NULL;
}




BOOL PropertyGrid_IsItemSelected( HPGITEM hItem )
{ return 0 != (PropertyGrid_GetItemState( hItem ) & PGIS_SELECTED); }




VOID PropertyGrid_SelectItem( HWND hPropGrid, HPGITEM hItem, PGSelMode selMode, BOOL bUpdate )
{
	PGInfo* pPGI = GetControlData( hPropGrid );
	switch( selMode )
	{
	case PGSM_Replace:
		{
			HPGITEM hSel = PropertyGrid_GetFirstSelectedItem( hPropGrid );
			// Check for redundancy.
			if( hItem == hSel )
				if( !hItem || !PropertyGrid_GetNextSelectedItem( hPropGrid, hSel ) )
					return;

			while( hSel )
			{
				HPGITEM hNext = PropertyGrid_GetNextSelectedItem( hPropGrid, hSel );
				PropertyGrid_SelectItem( hPropGrid, hSel, PGSM_Remove, FALSE );
				hSel = hNext;
			}
			PropertyGrid_SelectItem( hPropGrid, hItem, PGSM_Add, bUpdate );
		}
		break;
		


	case PGSM_Add:
		if( hItem && !PropertyGrid_IsItemSelected( hItem ) )
		{
			pPGI->sel.AddHead( CONV(hItem) );
			PropertyGrid_SetItemState( hItem, PropertyGrid_GetItemState( hItem ) | PGIS_SELECTED );
			RECT rTitle;
			PropertyGrid_GetItemRects( hPropGrid, hItem, NULL, NULL, &rTitle, NULL, NULL, NULL );
			InvalidateRect( hPropGrid, &rTitle, TRUE );
			pPGI->bSelChanged = TRUE;
		}
		break;


	case PGSM_Remove:
		if( hItem && PropertyGrid_IsItemSelected( hItem ) )
		{
			pPGI->sel.Remove( CONV(hItem) );
			PropertyGrid_SetItemState( hItem, PropertyGrid_GetItemState( hItem ) & ~PGIS_SELECTED );
			RECT rTitle;
			PropertyGrid_GetItemRects( hPropGrid, hItem, NULL, NULL, &rTitle, NULL, NULL, NULL );
			InvalidateRect( hPropGrid, &rTitle, TRUE );
			pPGI->bSelChanged = TRUE;
		}
		break;


	case PGSM_Toggle:
		if( hItem )
			PropertyGrid_SelectItem( hPropGrid, hItem, PropertyGrid_IsItemSelected( hItem ) ? PGSM_Remove : PGSM_Add, FALSE );
		break;
	}

	if( bUpdate )
		UpdateWindow( hPropGrid );
}

HPGITEM PropertyGrid_GetFirstSelectedItem( HWND hPropGrid )
{ return CONV( GetControlData( hPropGrid )->sel.First() ); }

//...
HPGITEM PropertyGrid_GetNextSelectedItem( HWND hPropGrid, HPGITEM hItem )
{ return CONV( GetControlData( hPropGrid )->sel.Next( CONV(hItem) ) ); }


//..
HPGITEM PropertyGrid_GetPrevSelectedItem( HWND hPropGrid, HPGITEM hItem )
{ return CONV( GetControlData( hPropGrid )->sel.Prev( CONV(hItem ) ) ); }


//...
HPGITEM PropertyGrid_GetFocusItem( HWND hPropGrid )
{ return GetControlData( hPropGrid )->hFocusItem; }



//...
VOID PropertyGrid_EnableItem( HWND hPropGrid, HPGITEM hItem, BOOL bEnable )
{
	if( bEnable == PropertyGrid_IsItemEnabled( hItem ) )
		return;

	if( PropertyGrid_GetFocusItem( hPropGrid ) == hItem && PropertyGrid_IsEditing( hPropGrid ) )
		PropertyGrid_EndEdit( hPropGrid, TRUE );

	DWORD dwState = CONV(hItem)->GetState();
	if( bEnable )	dwState &= (~PGIS_DISABLED);
	else			dwState |= PGIS_DISABLED;
	CONV(hItem)->SetState( dwState );
	CONV(hItem)->OnEnable( hPropGrid, bEnable );
}



BOOL PropertyGrid_IsItemEnabled( HPGITEM hItem ) { return 0 == (PropertyGrid_GetItemState( hItem ) & PGIS_DISABLED); }



VOID PropertyGrid_ShowItem( HWND hPropGrid, HPGITEM hItem, BOOL bShow, BOOL bUpdate )
{
	if( !bShow && (PGIS_HIDDEN & PropertyGrid_GetItemState( hItem ) ) )
		return; // The item is already hidden

	BOOL bWasVisible = PropertyGrid_IsItemVisible( hPropGrid, hItem );

	// This item is the next visible item after skipping the children.
	// The item that is being shown/hidden is defined then by the range
	// between hItem and this item.
	HPGITEM hNext;

	if( bShow )
	{
		// The item is hidden and we want to show it.
		CONV(hItem)->SetState( CONV(hItem)->GetState() & (~PGIS_HIDDEN) );
		if( PropertyGrid_IsItemVisible( hPropGrid, hItem ) )
		{
			// The item was hidden and is now visible.
			hNext = PropertyGrid_GetNextVisible( hPropGrid, hItem, TRUE );
			CalculatePositionsBetween( hPropGrid, hItem, hNext, TRUE, TRUE );
			CalculatePositionsBetween( hPropGrid, hNext, NULL, TRUE, FALSE );
			RecalculateScrollInfo( hPropGrid );

			// Update the previous sibling so that hierarchy lines remain continuous.
			if( PropertyGrid_GetParent( hItem ) )
			{
				HPGITEM hPrevVisible = PropertyGrid_GetPrevVisible( hPropGrid, hItem );
				if( hPrevVisible && PropertyGrid_GetParent( hPrevVisible ) == PropertyGrid_GetParent( hItem ) )
					PropertyGrid_InvalidateItem( hPropGrid, hPrevVisible );
			}
		}
	}
	else
	{
		// The item is shown and we want to hide it.
		if( bWasVisible )
		{
			PGInfo* pPGI = GetControlData( hPropGrid );

			PropertyGrid_BeginSelChange( hPropGrid );

			hNext = PropertyGrid_GetNextVisible( hPropGrid, hItem, TRUE );
			HPGITEM hPrev = PropertyGrid_GetPrevVisible( hPropGrid, hItem );

			for( HPGITEM hHide = hItem;
				hHide != hNext;
				hHide = PropertyGrid_GetNextVisible( hPropGrid, hHide ) )
			{
				if( PropertyGrid_GetFocusItem( hPropGrid ) == hHide )
					PropertyGrid_SetFocusItem( hPropGrid, hNext ? hNext : hPrev, TRUE, FALSE );
				if( PropertyGrid_IsItemSelected( hHide ) )
					PropertyGrid_SelectItem( hPropGrid, hHide, PGSM_Remove, FALSE );

				PropertyGrid_InvalidateItem( hPropGrid, hHide );
				CONV(hHide)->OnShow( hPropGrid, FALSE );
				pPGI->numVisibleItems--;
			}
			CONV(hItem)->SetState( CONV(hItem)->GetState() | PGIS_HIDDEN );
			CalculatePositionsBetween( hPropGrid, hNext, NULL, TRUE, FALSE );
			RecalculateScrollInfo( hPropGrid );
			PropertyGrid_EndSelChange( hPropGrid );
		}
		else
			CONV(hItem)->SetState( CONV(hItem)->GetState() | PGIS_HIDDEN );
	}

	if( bUpdate )
		UpdateWindow( hPropGrid );
}





VOID PropertyGrid_CollapseItem( HWND hPropGrid, HPGITEM hItem, BOOL bUpdate, BOOL bNotify )
{
	if( CONV(hItem)->GetState() & PGIS_COLLAPSED )
		return;

	CONV(hItem)->SetState( CONV(hItem)->GetState() | PGIS_COLLAPSED );

	HPGITEM hEndItem = PropertyGrid_GetNextVisible( hPropGrid, hItem );

	if( PropertyGrid_IsItemVisible( hPropGrid, hItem ) )
	{
		// Hide the children and subchildren which were visible when the item was expanded.
		for( HPGITEM hHideItem = PropertyGrid_GetChild( hItem );
			hHideItem != NULL && hHideItem != hEndItem;
			hHideItem = PropertyGrid_GetNextItem( hHideItem ) )
		{
			if( 0 == (PropertyGrid_GetItemState( hHideItem ) & PGIS_HIDDEN ) )
			{
				GetControlData( hPropGrid )->numVisibleItems--;
				CONV(hHideItem)->OnShow( hPropGrid, FALSE );
			}
		}

		CalculatePositionsBetween( hPropGrid, hItem, NULL );
		RecalculateScrollInfo( hPropGrid );
		PropertyGrid_InvalidateItem( hPropGrid, hItem, TRUE ); // Invalidate the item and all items below it

		if( bUpdate )
			UpdateWindow( hPropGrid );
	}

	if( bNotify )
		PropertyGrid_Notify( hPropGrid, PGN_COLLAPSEITEM, hItem, NULL );
}





VOID PropertyGrid_ExpandItem( HWND hPropGrid, HPGITEM hItem, BOOL bUpdate, BOOL bNotify )
{
	if( !(CONV(hItem)->GetState() & PGIS_COLLAPSED) )
		return;

	HPGITEM hEndItem = PropertyGrid_GetNextVisible( hPropGrid, hItem );

	CONV(hItem)->SetState( CONV(hItem)->GetState() & (~PGIS_COLLAPSED) );

	if( PropertyGrid_IsItemVisible( hPropGrid, hItem ) )
	{
		// Calculate the positions of the newly shown children and show them.
		CalculatePositionsBetween( hPropGrid, hItem, hEndItem, FALSE, TRUE );

		// Calculate the positions of the existing items after the expanded item.
		if( hEndItem )
			CalculatePositionsBetween( hPropGrid, hEndItem, NULL, TRUE, FALSE );

		RecalculateScrollInfo( hPropGrid );
		PropertyGrid_InvalidateItem( hPropGrid, hItem ); // Invalidate the item itself to update [+]
		if( bUpdate )
			UpdateWindow( hPropGrid );
	}

	if( bNotify )
		PropertyGrid_Notify( hPropGrid, PGN_EXPANDITEM, hItem, NULL );
}





BOOL PropertyGrid_IsItemVisible( HWND hPropGrid, HPGITEM hItem )
{
	if( PropertyGrid_GetItemState( hItem ) & PGIS_HIDDEN )
		return FALSE;

	HPGITEM hParent = PropertyGrid_GetParent( hItem );
	while( hParent )
	{
		if( PropertyGrid_GetItemState( hParent ) & (PGIS_HIDDEN | PGIS_COLLAPSED) )
			return FALSE;
		hParent = PropertyGrid_GetParent( hParent );
	}
	return TRUE;
}



BOOL PropertyGrid_IsItemInView( HWND hPropGrid, HPGITEM hItem )
{
	if( !PropertyGrid_IsItemVisible( hPropGrid, hItem ) )
		return FALSE;

	RECT rItem;
	PropertyGrid_GetItemRects( hPropGrid, hItem, &rItem, 0, 0, 0, 0, 0 );
	RECT rClient = GetClientRect( hPropGrid ), rInt;
	return IntersectRect( &rInt, &rClient, &rItem );
}



BOOL PropertyGrid_EnsureItemVisible( HWND hPropGrid, HPGITEM hItem )
{
	if( !hItem ) return FALSE;

	if( !PropertyGrid_IsItemVisible( hPropGrid, hItem ) )
	{
		PropertyGrid_ShowItem( hPropGrid, hItem, TRUE, FALSE );
		HPGITEM hParent = hItem;
		while( hParent = PropertyGrid_GetParent( hParent ) )
			PropertyGrid_ExpandItem( hPropGrid, hParent, FALSE, TRUE );
	}
	RECT rItem;
	PropertyGrid_GetItemRects( hPropGrid, hItem, &rItem, 0, 0, 0, 0, 0 );
	RECT rClient = GetClientRect( hPropGrid );
	if( rItem.top > 0 && rItem.bottom < rClient.bottom )
		return TRUE;
	int dy;
	if( rItem.top < 0 )
		dy = rItem.top;
	else
		dy = rItem.bottom - rClient.bottom;

	//SendMessage( hPropGrid, WM_VSCROLL, MAKEWPARAM(dy < 0 ? SB_LINEDOWN : SB_LINEUP,0), 0 );

	SCROLLINFO si = {0}; si.cbSize = sizeof(SCROLLINFO);
	si.fMask = SIF_POS;
	GetScrollInfo( hPropGrid, SB_VERT, &si );
	si.nPos += dy;
	SetScrollInfo( hPropGrid, SB_VERT, &si, TRUE );

	InvalidateRect( hPropGrid, NULL, TRUE );
	ScrollWindow( hPropGrid, 0, dy, NULL, NULL );

	for( hItem = PropertyGrid_GetFirstVisible(hPropGrid);
		hItem != NULL;
		hItem = PropertyGrid_GetNextVisible(hPropGrid, hItem) )
	{
		CONV(hItem)->OnSizeMove( hPropGrid );
	}
}




INT PropertyGrid_GetDividerPosition( HWND hPropGrid )
{ return GetControlData( hPropGrid )->nDividerPos; }



VOID PropertyGrid_SetDividerPosition( HWND hPropGrid, INT newPos )
{
	// TODO: Implement this correctly - send resize notifications to the visible items.
	PGInfo* pPGI = GetControlData( hPropGrid );
	if( pPGI->nDividerPos == newPos )
		return;

	pPGI->nDividerPos = newPos;
	for( HPGITEM hItem = PropertyGrid_GetFirstVisible( hPropGrid );
		hItem != NULL;
		hItem = PropertyGrid_GetNextVisible( hPropGrid, hItem ) )
	{
		CONV(hItem)->OnSizeMove( hPropGrid );
	}

	// TODO: Consider invalidating only the rectangle between the old and new divider positions.
	InvalidateRect( hPropGrid, NULL, TRUE );
	UpdateWindow( hPropGrid );

	PropertyGrid_Notify( hPropGrid, PGN_DIVIDERPOSCHANGED, NULL, (LPVOID)newPos );
}






VOID PropertyGrid_BeginSelChange( HWND hPropGrid )
{ PGInfo* pPGI = GetControlData( hPropGrid ); pPGI->nSelChangeOperations++; }



VOID PropertyGrid_EndSelChange( HWND hPropGrid )
{
	PGInfo* pPGI = GetControlData( hPropGrid );
	pPGI->nSelChangeOperations--;
	if( pPGI->nSelChangeOperations <= 0 )
	{
		pPGI->nSelChangeOperations = 0;
		if( pPGI->bSelChanged )
		{
			pPGI->bSelChanged = FALSE;
			//OnSelChange();
			PropertyGrid_Notify( hPropGrid, PGN_SELCHANGED, PropertyGrid_GetFirstSelectedItem( hPropGrid ), NULL );
		}
	}
}





BOOL IsPropertyGridMessage( HWND hPropGrid, LPMSG pMsg )
{
	//if( pMsg->message < WM_KEYFIRST || pMsg->message > WM_KEYLAST )
	//if( pMsg->message != WM_KEYDOWN ) // We only care about WM_KEYDOWN messages.
	if( pMsg->message != WM_KEYDOWN && pMsg->message != WM_KEYUP )
		return FALSE;

	if( pMsg->hwnd != hPropGrid && GetParent( pMsg->hwnd ) != hPropGrid )
		return FALSE; // Ignore messages not sent to the property grid or its children.

	if( !IsPropertyGrid( hPropGrid ) )
		return FALSE; // This function should only be called on a property grid control.

	LRESULT childKeys = 0;
	if( pMsg->hwnd != hPropGrid )
		childKeys = SendMessage( pMsg->hwnd, WM_GETDLGCODE, pMsg->wParam, (LPARAM)pMsg );

	// Navigation keys: if aimed at the property grid, dispatch it and return TRUE. If it's
	// aimed at a child, then only send to property grid if child doesn't want, or CTRL is down.
	// Non-navigation keys: If target is prop grid, return FALSE. If target is a child, then see
	// if it wants it. If it does, dispatch it and return TRUE.
	if( PropertyGrid_IsNavigationKey( pMsg->wParam ) )
	{
		if( pMsg->hwnd == hPropGrid )
		{
			TranslateMessage( pMsg );
			DispatchMessage( pMsg );
			return TRUE;
		}
		// Message is aimed at a child. Post to the property grid window if the child doesn't want it
		// or if CTRL is down.
		if( !ISKEYDOWN( VK_CONTROL ) )
		{
			if( (childKeys & DLGC_WANTALLKEYS) ||

				((pMsg->wParam == VK_UP || pMsg->wParam == VK_DOWN ||
				pMsg->wParam == VK_LEFT || pMsg->wParam == VK_RIGHT)
				&& (childKeys & DLGC_WANTARROWS ) ) ||

				(pMsg->wParam == VK_TAB && (childKeys & DLGC_WANTTAB)) )
			{
				TranslateMessage( pMsg );
				DispatchMessage( pMsg );
				return TRUE;
			}
		}
		// CONTROL is down or child doesn't want key. Send to property grid.
		SendMessage( hPropGrid, pMsg->message, pMsg->wParam, pMsg->lParam );
		return TRUE;
	}
	else
	{
		// It's not a navigation key.
		if( pMsg->hwnd == hPropGrid )
			return FALSE;
		
		// The target window is a child control.
		if( (childKeys & DLGC_WANTALLKEYS) || (childKeys & DLGC_WANTCHARS) ||
			((childKeys & (DLGC_BUTTON | DLGC_RADIOBUTTON)) && pMsg->wParam == VK_SPACE ) )
		{
			TranslateMessage( pMsg );
			DispatchMessage( pMsg );
			return TRUE;
		}
	}
	return FALSE;
}






LRESULT CALLBACK PGMessageHookProc( INT code, WPARAM wParam, LPARAM lParam )
{
	LRESULT ret = CallNextHookEx( pgGlobals.msgHook, code, wParam, lParam );
	LPMSG pMsg = (LPMSG)lParam;
	if( pMsg->hwnd && IsPropertyGridMessage( GetParent(pMsg->hwnd), pMsg ) )
		pMsg->message = WM_NULL; // Prevent further processing of the WM_KEYDOWN message.
	return ret;
}


INT PropertyGrid_HookKeyboard()
{
	pgGlobals.hookCalls++;
	if( pgGlobals.msgHook )
		return pgGlobals.hookCalls;
	pgGlobals.msgHook = SetWindowsHookEx( WH_GETMESSAGE, PGMessageHookProc,
						pgGlobals.hInstance, GetCurrentThreadId() );
	return pgGlobals.hookCalls;
}


INT PropertyGrid_UnhookKeyboard()
{
	pgGlobals.hookCalls--;
	if( !pgGlobals.msgHook || pgGlobals.hookCalls != 0 )
		return pgGlobals.hookCalls;
	UnhookWindowsHookEx( pgGlobals.msgHook );
	pgGlobals.msgHook = NULL;
	return pgGlobals.hookCalls;
}








// The property grid control's message procedure.
LRESULT CALLBACK A3DPGWndProc( HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam )
{

	PGInfo* pPGI = GetControlData( hWnd );

	if( pPGI )
	{
		HPGITEM hFocus = PropertyGrid_GetFocusItem( hWnd );
		if( hFocus && PropertyGrid_IsEditing( hWnd ) )
		{
			BOOL bHandled = FALSE;
			LRESULT ret = CONV(hFocus)->MsgProc( hWnd, Msg, wParam, lParam, bHandled );
			if( bHandled )
				return ret;
		}
	}


	switch( Msg )
	{
	case WM_NCCREATE:
		if( NULL == (pPGI = new PGInfo( hWnd ) ) )
			return FALSE;
		SetControlData( hWnd, pPGI );
		return TRUE;


	case WM_DESTROY:
		PropertyGrid_Clear( hWnd, FALSE );
		break;


	case WM_NCDESTROY:
		delete pPGI;
		break;


	case WM_CREATE:
		// Create the tooltip.
		{
			HINSTANCE hInstance = (HINSTANCE)GetWindowLongPtr( hWnd, GWLP_HINSTANCE );
			pPGI->hToolTip = CreateWindowEx( WS_EX_TOPMOST, TOOLTIPS_CLASS, NULL,
				WS_POPUP | TTS_NOPREFIX | TTS_ALWAYSTIP,
				CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
				hWnd, NULL, hInstance, NULL );
			SetWindowPos( pPGI->hToolTip, HWND_TOPMOST, 0, 0, 0, 0,
				SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE );
			// Set the "tool" information, which is the entire client area.
			TOOLINFO ti = {0};
			ti.cbSize = sizeof(TOOLINFO);
			//ti.uFlags = TTF_SUBCLASS; // TODO: Use relayevent
			ti.uFlags = TTF_SUBCLASS | TTF_TRANSPARENT;
			//ti.uId = (UINT_PTR)hWnd;
			ti.hwnd = hWnd;
			ti.hinst = hInstance;
			ti.lpszText = LPSTR_TEXTCALLBACK;//TEXT("le sample text of tooptip");
			GetClientRect( hWnd, &ti.rect );
			SendMessage( pPGI->hToolTip, TTM_ADDTOOL, 0, (LPARAM)&ti );
		}
		{
			SCROLLINFO si;
			si.cbSize = sizeof(SCROLLINFO);
			si.fMask = SIF_PAGE | SIF_POS | SIF_RANGE;
			si.nMax = 0;
			si.nMin = 0;
			si.nPage = 0;
			si.nPos = 0;
			si.nTrackPos = 0;
			SetScrollInfo( hWnd, SB_VERT, &si, FALSE );
			SetWindowText( hWnd, ((CREATESTRUCT*)lParam)->lpszName );
			return 0;
		}
		break;

	case WM_GETFONT: return (LRESULT)pPGI->hFont;

	case WM_SETFONT:
		{
			pPGI->SetFont( (HFONT)wParam );
			InvalidateRect( hWnd, NULL, FALSE );
		}
		break;



	case WM_ACTIVATE:
		//g_Log( TEXT( "PropGrid WM_ACTIVATE: %x %x" ), wParam, lParam );
		//if( LOWORD( wParam ) == WA_INACTIVE )
		//	PropertyGrid_EndEdit( hWnd, TRUE );
		break;


	case WM_NOTIFY:
		if( ((NMHDR*)lParam)->hwndFrom == pPGI->hToolTip && pPGI->hToolTip != NULL )
		{
			LPNMHDR pNMH = (LPNMHDR)lParam;
			if( pNMH->code == TTN_SHOW )
			{
				HPGITEM hItem = PropertyGrid_HitTest( hWnd, GetCursorPos(hWnd), NULL );
				if( !hItem || !PropertyGrid_GetItemTitle( hWnd, hItem ) )
					break;
				RECT rText;
				PropertyGrid_GetItemRects( hWnd, hItem, NULL, NULL, NULL, NULL, &rText, NULL );
				ClientToScreen( hWnd, &rText );
				// TODO: Use only if available.
				SendMessage( pPGI->hToolTip, TTM_ADJUSTRECT, TRUE, (LPARAM)&rText );
				SetWindowPos( pPGI->hToolTip, NULL, rText.left, rText.top, 0, 0,
					SWP_NOSIZE | SWP_NOZORDER | SWP_NOACTIVATE );
				return 1;
			}
			else if( pNMH->code == TTN_GETDISPINFO )
			{
				LPNMTTDISPINFO pDispInfo = (LPNMTTDISPINFO)lParam;
				HPGITEM hItem = PropertyGrid_HitTest( hWnd, GetCursorPos(hWnd), NULL );
				if( !hItem || !PropertyGrid_GetItemTitle( hWnd, hItem ) )
				{
					// Determine if the text is truncated.
					pDispInfo->lpszText = NULL;
				}
				else
				{
					pDispInfo->lpszText = const_cast<LPTSTR>(PropertyGrid_GetItemTitle( hWnd, hItem ));
				}
			}
		}
		break;


	case WM_PARENTNOTIFY:
		if( wParam == WM_LBUTTONDOWN )
		{
			PGHITTESTINFO hti;
			HPGITEM hItem = PropertyGrid_HitTest( hWnd, GetCursorPos(hWnd), &hti );
			if( !hItem )
				break;


			PropertyGrid_BeginSelChange( hWnd );
			if( ISKEYDOWN( VK_SHIFT ) )
			{
				HPGITEM hFirst = PropertyGrid_GetFirstSelectedItem( hWnd );
				if( !hFirst )
					hFirst = PropertyGrid_GetFirstVisible( hWnd );
				HPGITEM hLast = hItem;
				RECT rFirst, rLast;
				PropertyGrid_GetItemRects( hWnd, hFirst, &rFirst, NULL, NULL, NULL, NULL, NULL );
				PropertyGrid_GetItemRects( hWnd, hLast, &rLast, NULL, NULL, NULL, NULL, NULL );
				if( rFirst.top > rLast.top )
					SWAP( hFirst, hLast );
				hLast = PropertyGrid_GetNextVisible( hWnd, hLast );
				do
				{
					PropertyGrid_SelectItem( hWnd, hFirst, PGSM_Add, FALSE );
					hFirst = PropertyGrid_GetNextVisible( hWnd, hFirst );
				} while( hFirst && hFirst != hLast );
			}
			else
				PropertyGrid_SelectItem( hWnd, hItem, ISKEYDOWN( VK_CONTROL ) ? PGSM_Toggle : PGSM_Replace, FALSE );
			PropertyGrid_EndSelChange( hWnd );


			PropertyGrid_SetFocusItem( hWnd, hItem, FALSE );
			if( hti.flags & PGHT_ONITEMCONTENT )
				PropertyGrid_EditItem( hWnd, hItem, FALSE );
			else
				PropertyGrid_EndEdit( hWnd, FALSE );
			UpdateWindow( hWnd );
		}
		break;


	case WM_LBUTTONDOWN:
		if( IsWindowEnabled( hWnd ) )
		{
			//g_Log( TEXT( "PG WM_LBUTTONDOWN" ) );
			SetFocus( hWnd );
			PGHITTESTINFO hti;
			HPGITEM hItem = PropertyGrid_HitTest( hWnd, GetCursorPos(hWnd), &hti );

			PropertyGrid_BeginSelChange( hWnd );
			if( hti.flags & PGHT_ONDIVIDER )
			{
				// Start dragging the vertical divider.
				pgGlobals.draggingDivider = TRUE;
				SetCapture( hWnd );
			}
			else if( hItem )
			{
				PropertyGrid_SetFocusItem( hWnd, hItem, FALSE );
				if( hti.flags & PGHT_ONITEMGLYPH )
				{
					if( PropertyGrid_GetItemState( hItem ) & PGIS_COLLAPSED )
						PropertyGrid_ExpandItem( hWnd, hItem, TRUE, TRUE );
					else
						PropertyGrid_CollapseItem( hWnd, hItem, TRUE, TRUE );
				}
				if( hti.flags & PGHT_ONITEMCONTENT )
					PropertyGrid_EditItem( hWnd, hItem, FALSE );
				else
					PropertyGrid_EndEdit( hWnd, FALSE );

				if( wParam & MK_SHIFT )
				{
					HPGITEM hFirst = PropertyGrid_GetFirstSelectedItem( hWnd );
					if( !hFirst )
						hFirst = PropertyGrid_GetFirstVisible( hWnd );
					HPGITEM hLast = hItem;
					RECT rFirst, rLast;
					PropertyGrid_GetItemRects( hWnd, hFirst, &rFirst, NULL, NULL, NULL, NULL, NULL );
					PropertyGrid_GetItemRects( hWnd, hLast, &rLast, NULL, NULL, NULL, NULL, NULL );
					if( rFirst.top > rLast.top )
						SWAP( hFirst, hLast );
					hLast = PropertyGrid_GetNextVisible( hWnd, hLast );
					do
					{
						PropertyGrid_SelectItem( hWnd, hFirst, PGSM_Add, FALSE );
						hFirst = PropertyGrid_GetNextVisible( hWnd, hFirst );
					} while( hFirst && hFirst != hLast );
				}
				else
					PropertyGrid_SelectItem( hWnd, hItem, (wParam & MK_CONTROL) ? PGSM_Toggle : PGSM_Replace, FALSE );



				if( (hti.flags & PGHT_ONITEMMENU) && PropertyGrid_GetItemMenu( hItem, NULL ))
				{
					UINT flags = 0;
					//flags |= TPM_RECURSE; // Submenus will automatically open.
					flags |= TPM_RETURNCMD;
					if( 0 == PropertyGrid_Notify( hWnd, PGN_MENUPOPUP, hItem, (LPVOID)&flags ) )
					{
						HMENU hMenu = PropertyGrid_GetItemMenu( hItem, NULL );
						if( hItem )
						{
							POINT ptCursor = GetCursorPos();
							BOOL cmd = TrackPopupMenu( hMenu, flags, ptCursor.x, ptCursor.y, 0, hWnd, NULL );
							PropertyGrid_Notify( hWnd, PGN_MENUCOMMAND, hItem, (LPVOID)cmd );
						}
					}
				}

				UpdateWindow( hWnd );
			}
			else
			{
				// Mouse clicked on empty space. Deselect all items.
				if( 0 == (wParam & MK_CONTROL) )
					PropertyGrid_SelectItem( hWnd, NULL, PGSM_Replace );
			}

			PropertyGrid_EndSelChange( hWnd );
		}
		break;


	case WM_LBUTTONUP:
		if( pgGlobals.draggingDivider )
		{
			pgGlobals.draggingDivider = FALSE;
			ReleaseCapture();
		}
		break;

	case WM_CAPTURECHANGED:
		if( hWnd != (HWND)lParam )
			pgGlobals.draggingDivider = FALSE;
		break;


	case WM_MOUSEMOVE:
		{
			// This is to have the window receive WM_MOUSELEAVE and WM_MOUSEHOVER, which are cool!
			//TRACKMOUSEEVENT tme = {0};
			//tme.cbSize = sizeof(TRACKMOUSEEVENT);
			//tme.hwndTrack = hWnd;

			//tme.dwHoverTime = HOVER_DEFAULT;
			//tme.dwFlags = TME_LEAVE | TME_HOVER;
			////_TrackMouseEvent( &tme );
			//TrackMouseEvent( &tme );
		}
		if( pgGlobals.draggingDivider )
		{
			RECT rClient;
			GetClientRect( hWnd, &rClient );
			POINT ptCursor;
			GetCursorPos( &ptCursor );
			ScreenToClient( hWnd, &ptCursor );
			INT newPos = ptCursor.x;

			newPos = CLAMP( newPos, PG_MIN_DIVIDER_MARGIN, rClient.right - PG_MIN_DIVIDER_MARGIN );
			if( newPos == PropertyGrid_GetDividerPosition( hWnd ) )
				break;
			PropertyGrid_SetDividerPosition( hWnd, newPos );
		}
		else
		{
			PGHITTESTINFO hti;
			HPGITEM hHotItem = PropertyGrid_HitTest( hWnd, GetCursorPos( hWnd ), &hti );
			if( hti.flags & PGHT_ONDIVIDER )
				SetCursor( LoadCursor( NULL, IDC_SIZEWE ) );
			else
				SetCursor( LoadCursor( NULL, IDC_ARROW ) );

			if( pPGI->hToolTip )
			{
				TOOLINFO ti = {0};
				ti.cbSize = sizeof(TOOLINFO);
				ti.hwnd = hWnd;
				if( !SendMessage( pPGI->hToolTip, TTM_GETTOOLINFO, 0, (LPARAM)&ti ) )
				{
					//DebugPrintfLine( "Failed to get tool info" );
					break;
				}

				RECT rTT;
				if( hHotItem )
					PropertyGrid_GetItemRects( hWnd, hHotItem, NULL, NULL, &rTT, NULL, NULL, NULL );
				else
					SetRect( &rTT, 0, 0, 0, 0 );

				if( rTT != ti.rect )
				{
					ti.rect = rTT;
					SendMessage( pPGI->hToolTip, TTM_NEWTOOLRECT, 0, (LPARAM)&ti );
				}
			}
		}
		break;




	case WM_LBUTTONDBLCLK:
		{
			PGHITTESTINFO hti;
			HPGITEM hItem = PropertyGrid_HitTest( hWnd, GetCursorPos( hWnd ), &hti );
			if( hti.flags & PGHT_ONITEM )
			{
				if( PropertyGrid_GetItemState( hItem ) & PGIS_COLLAPSED )
					PropertyGrid_ExpandItem( hWnd, hItem, TRUE, TRUE );
				else
					PropertyGrid_CollapseItem( hWnd, hItem, TRUE, TRUE );
			}
		}
		break;



		
	case WM_GETDLGCODE: return DLGC_WANTALLKEYS;

	case WM_KEYDOWN:
		if( PropertyGrid_GetFirstItem( hWnd ) != NULL )
		{
			if( PropertyGrid_IsNavigationKey( wParam ) )
				SetFocus( hWnd );

			HPGITEM hFocus = PropertyGrid_GetFocusItem( hWnd );

			switch( wParam )
			{
				case VK_DOWN:
				case VK_UP:
				case VK_TAB:
					{
						if( hFocus == NULL )
							hFocus = PropertyGrid_GetFirstVisible( hWnd );
						else if( wParam == VK_UP || (wParam == VK_TAB && ISKEYDOWN(VK_SHIFT)) )
							hFocus = PropertyGrid_GetPrevVisible( hWnd, hFocus );
						else
							hFocus = PropertyGrid_GetNextVisible( hWnd, hFocus );

						if( !hFocus )
							hFocus = PropertyGrid_GetFocusItem( hWnd );

						if( hFocus )
						{
							BOOL bWasEditing = PropertyGrid_IsEditing( hWnd );
							BOOL bFocusChanged = PropertyGrid_SetFocusItem( hWnd, hFocus, FALSE, FALSE );
							if( bFocusChanged && !ISKEYDOWN( VK_CONTROL ) )
							{
								PGSelMode selMode = PGSM_Replace;
								if( ISKEYDOWN( VK_SHIFT ) && wParam != VK_TAB )
									selMode = PGSM_Add;
								PropertyGrid_BeginSelChange( hWnd );
								PropertyGrid_SelectItem( hWnd, hFocus, selMode, FALSE );
								PropertyGrid_EndSelChange( hWnd );
								UpdateWindow( hWnd );
							}
							if( bWasEditing )
								PropertyGrid_EditItem( hWnd, hFocus, FALSE );
						}
					} break;

				case VK_SPACE:
					if( hFocus )//ISKEYDOWN( VK_CONTROL ) && hFocus )
						PropertyGrid_SelectItem( hWnd, hFocus, PGSM_Toggle, TRUE );
					break;

				case VK_ADD:
				case VK_SUBTRACT:
					{
						if( !hFocus || !PropertyGrid_ItemHasChildren( hWnd, hFocus ) )
							return 0;
						if( PropertyGrid_GetItemState( hFocus ) & PGIS_COLLAPSED )
							PropertyGrid_ExpandItem( hWnd, hFocus, TRUE, TRUE );
						else
							PropertyGrid_CollapseItem( hWnd, hFocus, TRUE, TRUE );
					}
					return 0;

				case VK_RETURN:
					if( PropertyGrid_IsEditing( hWnd ) )
						PropertyGrid_EndEdit( hWnd, FALSE );
					else
						PropertyGrid_EditItem( hWnd, hFocus, FALSE );
					return 0;

				case VK_ESCAPE:
					PropertyGrid_CancelEdit( hWnd, FALSE );
					return 0;
			}
		}
		break;



	case WM_KILLFOCUS:
	case WM_SETFOCUS:
		{
			HPGITEM hFocus = PropertyGrid_GetFocusItem( hWnd );
			if( hFocus )
			{
				PropertyGrid_InvalidateItem( hWnd, hFocus );
				UpdateWindow( hWnd );
			}
		}
		break;



	case WM_SIZE:
		{
			RECT rClient;
			GetClientRect( hWnd, &rClient );
			RecalculateScrollInfo( hWnd );

			for( HPGITEM hItem = PropertyGrid_GetFirstVisible( hWnd );
				hItem != NULL;
				hItem = PropertyGrid_GetNextVisible( hWnd, hItem ) )
			{
				// For now, if the item has a menu and no content, we'll invalidate it. This is needed
				// because an item's menu arrow is located on the right side, which means it moves when
				// the window is resized.
				if( !CONV(hItem)->HasContent( hWnd ) )
				{
					if( PropertyGrid_IsItemSelected( hItem ) ||
						PropertyGrid_GetItemMenu( hItem, NULL ) )
						PropertyGrid_InvalidateItem( hWnd, hItem );
				}
				CONV(hItem)->OnSizeMove( hWnd );
			}
		}
		break;

	case WM_MOUSEWHEEL:
		{
			INT dy = (SHORT)HIWORD(wParam);// / WHEEL_DELTA;
			SendMessage( hWnd, WM_VSCROLL, MAKEWPARAM(dy<0 ? SB_LINEDOWN : SB_LINEUP,0), 0 );
		}
		break;

	case WM_VSCROLL:
		{
			SCROLLINFO si = {0};
			si.cbSize = sizeof(SCROLLINFO);
			si.fMask = SIF_ALL;
			GetScrollInfo( hWnd, SB_VERT, &si );
			INT oldPos = si.nPos;
			int pagePixels = RectHeight( GetClientRect( hWnd ) );
			switch( LOWORD(wParam) )
			{
			case SB_TOP:		si.nPos = 0; break;
			case SB_BOTTOM:		si.nPos = si.nMax; break;
			case SB_PAGEUP:		si.nPos -= pagePixels; break;
			case SB_LINEUP:		si.nPos -= 24; break;
			case SB_PAGEDOWN:	si.nPos += pagePixels; break;
			case SB_LINEDOWN:	si.nPos += 24; break;
			case SB_THUMBTRACK:	si.nPos = si.nTrackPos; break;
			}
			si.fMask = SIF_POS;
			SetScrollInfo( hWnd, SB_VERT, &si, TRUE );
			GetScrollInfo( hWnd, SB_VERT, &si );
			if( oldPos == si.nPos )
				break;

			int dy = oldPos - si.nPos;
			
			InvalidateRect( hWnd, NULL, TRUE );
			//ScrollWindow( hWnd, 0, dy, &rClient, NULL );
			ScrollWindow( hWnd, 0, dy, NULL, NULL );

			for( HPGITEM hItem = PropertyGrid_GetFirstVisible(hWnd);
				hItem != NULL;
				hItem = PropertyGrid_GetNextVisible(hWnd, hItem) )
			{
				CONV(hItem)->OnSizeMove( hWnd );
			}

			UpdateWindow( hWnd );
		}
		break;


	case WM_ERASEBKGND: return 1;

	case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC paintDC = BeginPaint( hWnd, &ps );
			if( !paintDC )
			{ EndPaint( hWnd, &ps ); break; }
			PaintPropertyGrid( pPGI, paintDC, &ps.rcPaint );
			//ValidateRect( hWnd, &ps.rcPaint ); // TODO: Useful?
			EndPaint( hWnd, &ps );
		}
		return 0;

	case WM_SYSCOLORCHANGE:
	case WM_THEMECHANGED:
		{
			// TODO: Reinitialize global GDI objects.
		}
		break;
	}
	return DefWindowProc( hWnd, Msg, wParam, lParam );
}




/////////////////////////////////////////////////////////////////////////////////
// Init and Uninit() - register and unregister the window class, and do
// class-level allocations and cleanup.

BOOL IsPropertyGrid( HWND hWnd )
{
	if( hWnd && GetClassLong( hWnd, GCW_ATOM ) == pgGlobals.atom )
		return TRUE;
	return FALSE;
}


BOOL PropertyGrid_Init( HINSTANCE hInstance )
{
	pgGlobals.hInstance = hInstance;
	if( pgGlobals.initialized )
		return pgGlobals.atom != 0;
	pgGlobals.initialized = true;

	WNDCLASSEX wcex;
	ZeroMemory( &wcex, sizeof( WNDCLASSEX ) );

	wcex.cbSize = sizeof( WNDCLASSEX );
	wcex.style = CS_GLOBALCLASS | CS_DBLCLKS | CS_HREDRAW;
	wcex.lpfnWndProc = A3DPGWndProc;
	wcex.cbWndExtra = sizeof( PGInfo* );
	wcex.hInstance = hInstance;
	//wcex.hCursor = LoadCursor( 0, IDC_ARROW );
	wcex.hCursor = NULL;
	//wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszClassName = WC_PROPERTYGRID;

	pgGlobals.atom = RegisterClassEx( &wcex );
	if( 0 == pgGlobals.atom )
	{
		OutputDebugString( TEXT("Failed to register the win32 property grid class!") );
		return 0;
	}


	// Create the global GDI objects used to render the property grid controls.
	pgGlobals.hGuiFont = CreateGuiFont();

	{
		// Create the symbol font.
		LOGFONT lf;
		ZeroMemory( &lf, sizeof(LOGFONT) );
		StringCchCopy( lf.lfFaceName, ARRAY_SIZE( lf.lfFaceName ), TEXT( "Webdings" ) );
		lf.lfCharSet = SYMBOL_CHARSET;
		//lf.lfHeight = 22;
		lf.lfQuality = ANTIALIASED_QUALITY;
		pgGlobals.hSymbolFont = CreateFontIndirect( &lf );
		if( !pgGlobals.hSymbolFont )
			OutputDebugString( TEXT("Failed to load Wingdings font!" ) );

		//lf.lfWeight = FW_BLACK;
		//lf.lfHeight += 8;
		//g_hBigGlyphFont = CreateFontIndirect( &lf );
	}

	pgGlobals.hDividerPen = CreatePen( PS_SOLID, 1, GetSysColor( COLOR_BTNSHADOW ) );
	pgGlobals.hGlyphPen = CreatePen( PS_SOLID, 1, GetSysColor( COLOR_BTNTEXT ) );

	LOGBRUSH lb = {0};
	lb.lbColor = GetSysColor( COLOR_BTNTEXT ); //GetSysColor( COLOR_WINDOWTEXT );
	lb.lbStyle = BS_SOLID;
	pgGlobals.hHierarchyPen = ExtCreatePen( PS_COSMETIC | PS_ALTERNATE, 1, &lb, 0, NULL );

	return TRUE;
}





VOID PropertyGrid_Uninit()
{
	if( !pgGlobals.initialized )
		return;
	pgGlobals.initialized = false;

	if( pgGlobals.atom != 0 )
		UnregisterClass( (LPCTSTR)pgGlobals.atom, GetModuleHandle(0) );
	pgGlobals.atom = NULL;

	// Delete the global GDI objects.
	DeleteObject( pgGlobals.hGuiFont );			pgGlobals.hGuiFont = NULL;
	DeleteObject( pgGlobals.hSymbolFont );		pgGlobals.hSymbolFont = NULL;
	DeleteObject( pgGlobals.hDividerPen );		pgGlobals.hDividerPen = NULL;
	DeleteObject( pgGlobals.hGlyphPen );		pgGlobals.hGlyphPen = NULL;
	DeleteObject( pgGlobals.hHierarchyPen );	pgGlobals.hHierarchyPen = NULL;
}