// This file implements the spin box number property type for the Angel3D Property Grid control.

# include <PropertyGridCtrl.h>
# include <PropertyGridItem.h>
# include <CSTB\Win32\SpinBoxCtrl.h>
# include <CSTB\Win32\Win32Util.h>
# include <strsafe.h>
# include <tchar.h>




class PGSpinBoxNumberItem : public PGItem
{
public:
	HWND	m_hEdit;
	HWND	m_hSpinBox;
	FLOAT	m_fMin;
	FLOAT	m_fMax;
	FLOAT	m_fSpinFactor;
	BOOL	m_bDirty;
	const TCHAR*	m_sFloatFormat;
	const TCHAR*	m_sIntFormat;


	PGSpinBoxNumberItem( PGItemType& type ) : PGItem( type )
	{
		m_hSpinBox = NULL;
		m_fMin = 0.f;
		m_fMax = 0.f;
		m_fSpinFactor = 1.f;
		m_bDirty = FALSE;
		m_sFloatFormat = TEXT("%g");
		m_sIntFormat = TEXT("%d");
	}



	virtual ~PGSpinBoxNumberItem()
	{
		GuiAssert( m_hEdit == NULL );
		GuiAssert( m_hSpinBox == NULL );
	}




	VOID OnInsert( HWND hPropGrid )
	{
		HINSTANCE hInstance = (HINSTANCE)GetWindowLongPtr( hPropGrid, GWLP_HINSTANCE );
		if( FALSE == A3DSpinBox_Init( hInstance ) )
		{
			DebugPrintf( TEXT("A3DSpinBox_Init() has failed! GLE: %d\n"), GetLastError() );
			return; // TODO: return FALSE.
		}
		DWORD dwEditStyle = WS_CHILD | ES_AUTOHSCROLL;

		RECT rContent;
		PropertyGrid_GetItemRects( hPropGrid, GetHandle(), NULL, &rContent, NULL, NULL, NULL, NULL );
		rContent.left += PG_PADDING_PIXELS_H;
		rContent.top += PG_PADDING_PIXELS_V;

		m_hEdit = CreateWindowEx( 0, WC_EDIT, 0, dwEditStyle,
			rContent.left, rContent.top, RectWidth(rContent), RectHeight(rContent),
			hPropGrid, 0, hInstance, 0 );

		SendMessage( m_hEdit, WM_SETFONT, (WPARAM)SendMessage( hPropGrid, WM_GETFONT, 0L, 0L ), 0L );

		m_hSpinBox = CreateWindowEx( 0, WC_A3DSPINBOX, NULL, WS_CHILD,
			0, 0, 10, 10, hPropGrid, NULL, hInstance, NULL );
		if( !m_hSpinBox )
		{
			DebugPrintf( TEXT( "Failed to create spin box control! GLE: %d\n" ), GetLastError() );
			DestroyWindow( m_hEdit ); m_hEdit = NULL;
			return;
		}
		//SpinBox_SetDragPixels( m_hSpinBox, 3 );
	}




	VOID OnSizeMove( HWND hPropGrid )
	{
		GuiAssert( PropertyGrid_IsItemVisible( hPropGrid, GetHandle() ) );
		GuiAssert( m_hSpinBox );
		RECT rContent;
		PropertyGrid_GetItemRects( hPropGrid, GetHandle(), NULL, &rContent, NULL, NULL, NULL, NULL );
		rContent.left += PG_PADDING_PIXELS_H;
		rContent.top += PG_PADDING_PIXELS_V;
		//InflateRect( &rContent, -PG_PADDING_PIXELS_H, -PG_PADDING_PIXELS_V )
		INT buttonWidth = 36;
		RECT rEdit = rContent;
		rEdit.right = MAX( rEdit.left, rEdit.right - buttonWidth );
		RECT rButton = rContent;
		rButton.left = rEdit.right;
		MoveWindow( m_hEdit, rEdit.left, rEdit.top, RectWidth(rEdit), RectHeight(rEdit), TRUE );
		MoveWindow( m_hSpinBox, rButton.left, rButton.top, RectWidth(rButton), RectHeight(rButton), TRUE );
	}




	VOID OnShow( HWND hPropGrid, BOOL bShow )
	{
		if( bShow )
			OnSizeMove( hPropGrid );
		ShowWindow( m_hEdit, bShow ? SW_SHOW : SW_HIDE );
		ShowWindow( m_hSpinBox, bShow ? SW_SHOW : SW_HIDE );
	}




	VOID OnEnable( HWND hPropGrid, BOOL bEnable )
	{
		EnableWindow( m_hEdit, bEnable );
		EnableWindow( m_hSpinBox, bEnable );
	}




	VOID OnRemove( HWND hPropGrid )
	{
		SAFE_DESTROY_WINDOW( m_hEdit );
		SAFE_DESTROY_WINDOW( m_hSpinBox );
	}




	BOOL OnEdit( HWND hPropGrid )
	{
		SetFocus( m_hEdit );
		SendMessage( m_hEdit, EM_SETSEL, 0, -1 );
		return TRUE;
	}




	VOID PaintContent( HWND hPropGrid, HDC hDC, RECT* prcPaint, RECT* prcContent )
	{ FillRect( hDC, prcContent, GetSysColor( COLOR_WINDOW ) ); }




	BOOL SetText( HWND hPropGrid, LPCTSTR sText, INT length )
	{
		if( !m_hEdit )
			return FALSE;

		if( length == 0 || sText == NULL )
			SetWindowText( m_hEdit, TEXT("") );
		else if( length < 0 )
			SetWindowText( m_hEdit, sText );
		else
			SetWindowText( m_hEdit, sText ); // TODO: set partial string.

		return TRUE;
	}




	INT GetText( HWND hPropGrid, LPTSTR sBuffer, INT bufferSize, BOOL& bSuccess ) const
	{
		if( !m_hEdit )
			return PGItem::GetText( hPropGrid, sBuffer, bufferSize, bSuccess );
		bSuccess = TRUE;
		return GetWindowText( m_hEdit, sBuffer, bufferSize );
	}




	BOOL SetFloat( HWND hPropGrid, FLOAT f )
	{
		if( m_fMin < m_fMax )
			f = CLAMP( f, m_fMin, m_fMax );
		TCHAR s[16]; s[0] = 0;
		StringCchPrintf( s, ARRAY_SIZE(s), m_sFloatFormat, f );
		return SetText( hPropGrid, s, ARRAY_SIZE(s) );
	}




	FLOAT GetFloat( HWND hPropGrid, BOOL& bSuccess ) const
	{
		TCHAR s[16]; s[0] = 0;
		INT len = GetText( hPropGrid, s, ARRAY_SIZE(s), bSuccess );
		if( len <= 0 || !bSuccess )
			return PGItem::GetFloat( hPropGrid, bSuccess );
		bSuccess = TRUE;
		return (FLOAT)_tstof( s );
	}



	BOOL SetInt( HWND hPropGrid, INT n )
	{
		TCHAR s[ 16 ]; s[0] = 0;
		StringCchPrintf( s, ARRAY_SIZE(s), m_sIntFormat, n );
		return SetText( hPropGrid, s, ARRAY_SIZE( s ) );
	}



	INT GetInt( HWND hPropGrid, BOOL& bSuccess ) const
	{
		TCHAR s[ 16 ]; s[0] = 0;
		INT len = GetText( hPropGrid, s, ARRAY_SIZE( s ), bSuccess );
		if( len <= 0 || !bSuccess )
			return PGItem::GetInt( hPropGrid, bSuccess );
		bSuccess = TRUE;
		return _tstoi( s );
	}




	LRESULT MsgProc( HWND hPropGrid, UINT Msg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
	{
		HPGITEM hItem = GetHandle();
		switch( Msg )
		{
		case WM_COMMAND:
			if( m_hEdit && (HWND)lParam == m_hEdit )
			{
				bHandled = TRUE;
				if( HIWORD( wParam ) == EN_CHANGE ) // Sent after control redraws.
				{
					m_bDirty = TRUE;
					PropertyGrid_Notify( hPropGrid, PGN_EDITING, hItem, NULL );
				}
				else if( HIWORD( wParam ) == EN_KILLFOCUS && m_bDirty )
				{
					BOOL bSuccess;
					SetFloat( hPropGrid, GetFloat( hPropGrid, bSuccess ) );
					m_bDirty = FALSE;
					PropertyGrid_Notify( hPropGrid, PGN_PROPERTYCHANGING, hItem, NULL );
					PropertyGrid_Notify( hPropGrid, PGN_PROPERTYCHANGED, hItem, PG_FIRST_CHANGE );
				}
				return 0;
			}
			break;


		case WM_NOTIFY:
			if( ((NMHDR*)lParam)->hwndFrom == m_hSpinBox )
			{
				bHandled = TRUE;

				NMSPINBOX* pNMSB = (NMSPINBOX*)lParam;
				/*if( pNMSB->hdr.code == SBN_DRAGSTART )
				{
					PropertyGrid_Notify( hPropGrid, PGN_EDITSTART, hItem, NULL );
				}
				else if( pNMSB->hdr.code == SBN_DRAGEND || pNMSB->hdr.code == SBN_DRAGCANCEL )
				{
					UINT code = pNMSB->hdr.code == SBN_DRAGEND ? PGN_EDITDONE : PGN_EDITCANCEL;
					PropertyGrid_Notify( hPropGrid, code, hItem, NULL );
				}
				else */if( pNMSB->hdr.code == SBN_DELTAVALUE )
				{
					SBN_SPINDRAG sd;
					BOOL bSuccess;
					sd.oldValue = GetFloat( hPropGrid, bSuccess );
					sd.spinFactor = m_fSpinFactor;
					sd.spinDelta = pNMSB->deltaValue;
					sd.newValue = sd.oldValue + sd.spinDelta * sd.spinFactor;

					if( 0 != PropertyGrid_Notify( hPropGrid, PGN_PROPERTYCHANGING, hItem, (LPVOID)&sd ) )
						return 0;

					if( sd.newValue == sd.oldValue )
						return 0;

					m_bDirty = FALSE;
					SetFloat( hPropGrid, sd.newValue );
					LPVOID pAdditionalData = (pNMSB->additionalInfo == SB_FIRST_CHANGE) ? PG_FIRST_CHANGE : 0;
					//if( pAdditionalData )
						//DebugPrintfLine( TEXT("spin box first change!" ) );
					PropertyGrid_Notify( hPropGrid, PGN_PROPERTYCHANGED, hItem, pAdditionalData );//(LPVOID)&sd );
				}
			}
			return 0;
		}
		
		return 0;
	}
};




PGItemTypeT< PGSpinBoxNumberItem > g_PGSpinBoxNumberItemType( TEXT("SpinBoxNumber") );



PGItemType* PropertyGrid_GetSpinBoxNumberType() { return &g_PGSpinBoxNumberItemType; }


HPGITEM PropertyGrid_InsertSpinBoxNumber( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent, HPGITEM hInsertBefore,
								LPVOID pUserData )
{
	return PropertyGrid_InsertItem( hPropGrid, g_PGSpinBoxNumberItemType.AllocateItem( hPropGrid, NULL ),
		id, sTitle, hParent, hInsertBefore, pUserData, NULL );
}




HWND PGSpinBoxNumber_GetEdit( HWND hPropGrid, HPGITEM hItem )
{ return ((PGSpinBoxNumberItem*)hItem)->m_hEdit; }


HWND PGSpinBoxNumber_GetSpinBox( HWND hPropGrid, HPGITEM hItem )
{ return ((PGSpinBoxNumberItem*)hItem)->m_hSpinBox; }


VOID PGSpinBoxNumber_SetDragPixels( HWND hPropGrid, HPGITEM hItem, UINT dragPixels )
{ SpinBox_SetDragPixels( PGSpinBoxNumber_GetSpinBox( hPropGrid, hItem ), dragPixels ); }


VOID PGSpinBoxNumber_SetSpinFactor( HWND hPropGrid, HPGITEM hItem, FLOAT factor )
{ ((PGSpinBoxNumberItem*)hItem)->m_fSpinFactor = factor; }


VOID PGSpinBoxNumber_GetRange( HWND hPropGrid, HPGITEM hItem, FLOAT* pfMin, FLOAT* pfMax )
{
	PGSpinBoxNumberItem* pItem = (PGSpinBoxNumberItem*)hItem;
	if( pfMin ) *pfMin = pItem->m_fMin;
	if( pfMax ) *pfMax = pItem->m_fMax;
}


VOID PGSpinBoxNumber_SetRange( HWND hPropGrid, HPGITEM hItem, FLOAT fMin, FLOAT fMax )
{
	PGSpinBoxNumberItem* pItem = (PGSpinBoxNumberItem*)hItem;
	pItem->m_fMin = fMin;
	pItem->m_fMax = fMax;
}


VOID PGSpinBoxNumber_SetFormatStrings( HWND hPropGrid, HPGITEM hItem, const TCHAR* sIntFormat, const TCHAR* sFloatFormat )
{
	PGSpinBoxNumberItem* pItem = (PGSpinBoxNumberItem*)hItem;
	if( sIntFormat ) pItem->m_sIntFormat = sIntFormat;
	if( sFloatFormat ) pItem->m_sFloatFormat = sFloatFormat;
}