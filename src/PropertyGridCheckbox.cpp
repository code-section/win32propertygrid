// This file implements the checkbox property type for code-section's Property Grid control.

# include <PropertyGridCtrl.h>
# include <PropertyGridItem.h>
# include <CSTB\win32\Win32Util.h>
# include <windowsx.h>




/// A checkbox item is an item which displays a checkbox that allows the user to control
/// a boolean value. Only PropertyGrid_GetBool/SetBool() work on this item.
/// The checkbox control is created when the item is shown, and destroyed when it's hidden
/// for memory efficiency.
class PGCheckboxItem : public PGItem
{
public:
	INT			m_nValue;
	HWND		m_hButton;
	LPTSTR		m_sLabel;


	PGCheckboxItem( PGItemType& type ) : PGItem( type )
	{
		m_nValue = 0;
		m_hButton = NULL;
		m_sLabel = NULL;
	}




	virtual ~PGCheckboxItem()
	{
		GuiAssert( m_hButton == NULL );
		SetLabel( NULL );
	}




	LPCTSTR GetLabel() const { return m_sLabel; }




	void SetLabel( LPCTSTR sLabel )
	{
		SAFE_DELETE_ARRAY( m_sLabel );
		INT len = lstrlen( sLabel );
		if( len > 0 && len < MAX_PATH )
		{
			m_sLabel = new TCHAR[ len+1 ];
			lstrcpyn( m_sLabel, sLabel, len+1 );
		}
		if( m_hButton )
			SetWindowText( m_hButton, m_sLabel );
	}




	BOOL OnEdit( HWND hPropGrid )
	{
		GuiAssert( m_hButton );
		//DebugPrintfLine( TEXT("Setting focus to the checkbox") );
		SetFocus( m_hButton );
		return TRUE;
	}




	// This creates the checkbox control associated with the item.
	// This assumes the item is now visible.
	BOOL CreateButton( HWND hPropGrid )
	{
		if( m_hButton )
			return FALSE;

		DWORD style = WS_CHILD | BS_AUTOCHECKBOX;

		// Create the checkbox control window.
		m_hButton = CreateWindowEx( 0, WC_BUTTON, m_sLabel, style,
			0, 0, 20, 20, hPropGrid, NULL,
			(HINSTANCE)GetWindowLongPtr(hPropGrid, GWLP_HINSTANCE), 0 );
		if( !m_hButton )
		{
			DebugPrintf( TEXT("Failed to create checkbox control. GLE: %d - %s\n"),
				GetLastError(), GetLastErrorString() );
			return FALSE;
		}
		SendMessage( m_hButton, WM_SETFONT, (LPARAM)SendMessage( hPropGrid, WM_GETFONT, 0, 0 ), 0 );
		SetControlValue( m_nValue );
		OnSizeMove( hPropGrid );
		ShowWindow( m_hButton, SW_SHOW );
		return TRUE;
	}




	VOID OnShow( HWND hPropGrid, BOOL bShow )
	{
		//DebugPrintfLine( TEXT("%s checkbox control"), bShow ? TEXT("Creating") : TEXT("Destroying") );

		if( bShow )
			CreateButton( hPropGrid );
		else
			SAFE_DESTROY_WINDOW( m_hButton );
	}




	VOID OnSizeMove( HWND hPropGrid )
	{
		GuiAssert( m_hButton );
		//GuiAssert( PropertyGrid_IsItemVisible( hPropGrid, GetHandle() ) );
		RECT rContent;
		PropertyGrid_GetItemRects( hPropGrid, GetHandle(), NULL, &rContent, NULL, NULL, NULL, NULL );
		rContent.left += PG_PADDING_PIXELS_H;
		//rContent.top += PG_PADDING_PIXELS_V;
		InflateRect( &rContent, 0, -PG_PADDING_PIXELS_V );
		MoveWindow( m_hButton, rContent.left, rContent.top, RectWidth(rContent), RectHeight(rContent), TRUE );
	}




	VOID OnEnable( HWND hPropGrid, BOOL bEnable )
	{ if( m_hButton ) EnableWindow( m_hButton, bEnable ); }

	// Prevent any drawing in the content area, as the combo box is occupying it.
	//VOID PaintContent( HWND hPropGrid, HDC hDC, RECT* prcPaint, RECT* prcContent ) { }




	INT GetControlValue()
	{
		if( m_hButton )
		{
			switch( Button_GetCheck( m_hButton ) )
			{
				case BST_UNCHECKED: return 0;
				case BST_CHECKED: return 1;
				case BST_INDETERMINATE: return 2;
			}
		}
		return -1;
	}




	BOOL SetControlValue( INT n )
	{
		if( !m_hButton )
			return FALSE;
		WPARAM checkState = 0;
		switch( n )
		{
			case 0: checkState = BST_UNCHECKED; break;
			case 1: checkState = BST_CHECKED; break;
			default: checkState = BST_INDETERMINATE; break;
		}
		Button_SetCheck( m_hButton, checkState );
		return TRUE;
	}




	INT GetInt( HWND hPropGrid, BOOL& bSuccess ) const
	{
		bSuccess = TRUE;
		return m_nValue;
	}




	BOOL SetInt( HWND hPropGrid, INT n )
	{
		m_nValue = n;
		if( m_hButton )
			SetControlValue( n );
		return TRUE;
	}




	BOOL GetBool( HWND hPropGrid, BOOL& bSuccess ) const { return (BOOL)GetInt( hPropGrid, bSuccess ); }
	BOOL SetBool( HWND hPropGrid, BOOL b ) { return SetInt( hPropGrid, (INT)b ); }




	LRESULT MsgProc( HWND hPropGrid, UINT Msg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
	{
		switch( Msg )
		{
		case WM_COMMAND:
			if( !m_hButton || lParam != (LPARAM)m_hButton )
				break; // Only handle command messages from the checkbox control.
			if( HIWORD(wParam) == BN_CLICKED )
			{
				bHandled = TRUE;
				m_nValue = GetControlValue();
				PropertyGrid_Notify( hPropGrid, PGN_PROPERTYCHANGED, GetHandle(), PG_FIRST_CHANGE );
			}
			return 1;
		}
		return 0;
	}

};



PGItemTypeT< PGCheckboxItem > g_PGCheckboxItemType(TEXT("Checkbox"));



PGItemType* PropertyGrid_GetCheckboxType() { return &g_PGCheckboxItemType; }




HPGITEM PropertyGrid_InsertCheckbox( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent,
									HPGITEM hInsertBefore, LPVOID pUserData, LPCTSTR sCaption,
									BOOL bInitialValue )
{
	HPGITEM hItem = PropertyGrid_InsertItem( hPropGrid,
		g_PGCheckboxItemType.AllocateItem( hPropGrid, NULL ),
		id, sTitle, hParent, hInsertBefore, pUserData, NULL );
	if( !hItem )
		return NULL;
	PropertyGrid_SetBool( hPropGrid, hItem, bInitialValue );
	((PGCheckboxItem*)hItem)->SetLabel( sCaption );
	return hItem;
}
