// Internal header file used by the various implementation files of the Property Grid control.


# ifndef PGIMPL_H
# define PGIMPL_H

# include <PropertyGridCtrl.h>
# include <PropertyGridItem.h>
# include <CSTB\Win32\Win32Util.h>
# include <commctrl.h>
# include <tchar.h>
# include <vector>


struct PGGlobals
{
	bool	initialized = false;
	ATOM	atom = 0;
	bool	draggingDivider = false;
	HHOOK	msgHook = 0;
	HINSTANCE hInstance = 0;
	int		hookCalls = 0;
	HFONT	hGuiFont = 0;
	HFONT	hSymbolFont = 0;
	HPEN	hDividerPen = 0;
	HPEN	hGlyphPen = 0;
	HPEN	hHierarchyPen = 0;
	FillRectAlpha hilighter;
};

extern PGGlobals pgGlobals;




typedef CSTB::IntrusiveWeakTreeT< PGItem, offsetof( PGItem, PGItem::hook ) > PGItemTree;



/////////////////////////////////////////////////////////////////
// Struct holding information about the property grid control.
struct PGInfo
{
	PGItemTree items;
	
	//std::vector< PGItem* > selection;
	CSTB::IntrusiveWeakListT< PGItem, offsetof( PGItem, PGItem::selHook ) > sel;

	HWND		hWnd;
	HWND		hToolTip = 0;
	HPGITEM		hFocusItem = 0;
	INT			numVisibleItems = 0;
	BOOL		bEditing = FALSE;		// If the focus item is being edited. If not, messages are not sent to it.
	INT			nDividerPos = 120;	// Position of the vertical divider.

	INT			nSelChangeOperations = 0;
	BOOL		bSelChanged = FALSE;
	int			lineHeight = 22;
	HFONT		hFont = 0;
	PGNotificationCallback pfNotify = 0;
	void*		pfNotifyData = 0;

	PGInfo( HWND hPG ) : hWnd( hPG )
	{
		SetFont( pgGlobals.hGuiFont );
		hFont = pgGlobals.hGuiFont;
	}

	VOID SetFont( HFONT hNewFont )
	{
		hFont = hNewFont;

		HDC hDC = GetDC( hWnd );
		HFONT hOldFont = (HFONT)SelectObject( hDC, hFont );
		TEXTMETRIC tm;
		ZeroMemory( &tm, sizeof(tm) );
		GetTextMetrics( hDC, &tm );
		SelectObject( hDC, hOldFont );
		ReleaseDC( hWnd, hDC );
		lineHeight = PG_PADDING_PIXELS_V * 2 + (SHORT)tm.tmHeight;
	}
};



// Disable "conversion from LONG to ptr of different size" warnings.
//# pragma warning( push )
//# pragma warning( disable: 4311 )
//# pragma warning( disable: 4312 )

inline PGInfo*	GetControlData( HWND hWnd )					{ return (PGInfo*)GetWindowLongPtr( hWnd, 0 ); }
inline VOID		SetControlData( HWND hWnd, PGInfo* pPGI )	{ SetWindowLongPtr( hWnd, 0, (LONG_PTR)pPGI ); }

//# pragma warning( pop )




# endif // inclusion guard