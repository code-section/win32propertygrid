
# include <PropertyGridCtrl.h>
# include <PropertyGridItem.h>
# include <strsafe.h>
# include <tchar.h>
# include <CSTB\Win32\Win32Util.h>




// Uses TCHARs. Creates an edit control whenever an item of this type is to be edited. This is
// better than creating a separate control for every visible text property.
class PGEditItem : public PGItem
{
public:
	PGEditItem( PGItemType& type ) : PGItem( type )
	{
		m_hEdit = NULL;
		m_uVisibleLines = 0;
		m_uMaxLines = 5;
		m_bReadOnly = FALSE;
		m_bMultiline = FALSE;
		m_bAutoSize = TRUE;
		m_bDirty = FALSE;
	}



	VOID PaintContent( HWND hPropGrid, HDC hDC, RECT* prcPaint, RECT* prcContent )
	{ FillRect( hDC, prcContent, GetSysColor( COLOR_WINDOW ) ); }

	VOID OnInsert( HWND hPropGrid )
	{
		CreateEditControl( hPropGrid );
		//UpdateHeight( hPropGrid );
	}

	VOID OnRemove( HWND hPropGrid ) { SAFE_DESTROY_WINDOW( m_hEdit ); }

	BOOL OnEdit( HWND hPropGrid )
	{
		SetFocus( m_hEdit );
		SendMessage( m_hEdit, EM_SETSEL, 0, -1 );
		return TRUE;
	}


	/// Create or recreate the edit control.
	BOOL CreateEditControl( HWND hPropGrid )
	{
		SAFE_DESTROY_WINDOW( m_hEdit );

		// TODO: Pass this in pCreationData.
		DWORD dwStyle = WS_CHILD | ES_AUTOHSCROLL | ES_AUTOVSCROLL;
		if( PropertyGrid_IsItemVisible( hPropGrid, GetHandle() ) )
			dwStyle |= WS_VISIBLE;

		if( m_bMultiline )
			dwStyle |= (ES_MULTILINE | ES_WANTRETURN);

		RECT rContent;
		PropertyGrid_GetItemRects( hPropGrid, GetHandle(), NULL, &rContent, NULL, NULL, NULL, NULL );
		rContent.left += PG_PADDING_PIXELS_H;
		rContent.top += PG_PADDING_PIXELS_V;

		m_hEdit = CreateWindowEx( 0, WC_EDIT, 0, dwStyle,
			rContent.left, rContent.top, RectWidth(rContent), RectHeight(rContent),
			hPropGrid, 0, (HINSTANCE)GetWindowLongPtr(hPropGrid, GWLP_HINSTANCE), 0 );

		if( !m_hEdit )
		{
			DebugPrintf( "Failed to create edit control! GetLastError: %d", GetLastError() );
			return FALSE;
		}

		SetWindowText( m_hEdit, m_sValue );

		// Set the edit box font to be the same as the property grid control.
		SendMessage( m_hEdit, WM_SETFONT, (WPARAM)SendMessage( hPropGrid, WM_GETFONT, 0, 0 ), NULL );
		return TRUE;
	}



	BOOL SetMultiline( HWND hPropGrid, BOOL bMultiline, UINT maxVisibleLines, BOOL bAutoSize )
	{
		maxVisibleLines = MAX( maxVisibleLines, 1 );

		if( m_bMultiline == bMultiline && m_uMaxLines == maxVisibleLines && bAutoSize == m_bAutoSize )
			return TRUE;

		m_uMaxLines = maxVisibleLines;
		m_bAutoSize = bAutoSize;
		if( m_bMultiline != bMultiline )
		{
			m_bMultiline = bMultiline;
			BOOL bSuccess;
			TCHAR sBuffer[ 2048 ];
			sBuffer[0] = 0;
			INT len = PropertyGrid_GetText( hPropGrid, GetHandle(), sBuffer, ARRAY_SIZE(sBuffer), &bSuccess );
			if( !bSuccess )
				return FALSE;

			CreateEditControl( hPropGrid );
			PropertyGrid_SetText( hPropGrid, GetHandle(), sBuffer, len );
		}
		UpdateHeight( hPropGrid );
		return TRUE;
	}



	/// Calculates the height based on the string value and used font.
	INT CalculateHeight( HWND hPropGrid )
	{
		SHORT defHeight = PropertyGrid_GetDefaultItemHeight( hPropGrid );

		if( !IsWindow( m_hEdit ) )
			return defHeight;

		// Calculate height for multi-line edit control.
		TEXTMETRIC tm;
		GetTextMetrics( m_hEdit, &tm );

		UINT visibleLines = SendMessage( m_hEdit, EM_GETLINECOUNT, 0, 0 );
		visibleLines = CLAMP( visibleLines, 1, m_uMaxLines );
		INT editHeight = visibleLines * tm.tmHeight + 2 * PG_PADDING_PIXELS_V;
		return MAX( defHeight, editHeight );
	}



	/// Updates the height of the item to match the content. This is used with multi-line edit controls.
	VOID UpdateHeight( HWND hPropGrid )
	{
		if( !m_bAutoSize )
			return; // The height will only be changed "manually" using PropertyGrid_SetItemHeight().

		UINT visibleLines = SendMessage( m_hEdit, EM_GETLINECOUNT, 0, 0 );
		visibleLines = MIN( visibleLines, m_uMaxLines );
		if( visibleLines == m_uVisibleLines )
			return;

		m_uVisibleLines = visibleLines;
		PropertyGrid_SetItemHeight( hPropGrid, GetHandle(), CalculateHeight( hPropGrid ) );
	}



	VOID OnSizeMove( HWND hPropGrid )
	{
		if( !PropertyGrid_IsItemVisible( hPropGrid, GetHandle() ) )
			return;

		if( !IsWindow( m_hEdit ) )
			return;

		//DebugPrintfLine( TEXT("!") );

		RECT rContent;
		PropertyGrid_GetItemRects( hPropGrid, GetHandle(), NULL, &rContent, NULL, NULL, NULL, NULL );
		rContent.left += PG_PADDING_PIXELS_H;
		rContent.top += PG_PADDING_PIXELS_V;
		//InflateRect( &rContent, -PG_PADDING_PIXELS_H, -PG_PADDING_PIXELS_V );
		//InflateRect( &rContent, 0, -PG_PADDING_PIXELS_V );
		MoveWindow( m_hEdit, rContent.left, rContent.top, RectWidth(rContent), RectHeight(rContent), TRUE );
	}


	VOID OnShow( HWND hPropGrid, BOOL bShow )
	{
		//DebugPrintfLine( TEXT("bShow: %d"), bShow );
		if( !IsWindow( m_hEdit ) )
			return;
		if( bShow )
		{
			OnSizeMove( hPropGrid ); // TODO: Only move and size window if you must.
			ShowWindow( m_hEdit, SW_SHOW );
		}
		else
			ShowWindow( m_hEdit, SW_HIDE );
	}



	BOOL SetText( HWND hPropGrid, LPCTSTR sText, INT length )
	{
		if( !IsWindow( m_hEdit ) )
			return FALSE;

		if( length == 0 || sText == NULL )
			SetWindowText( m_hEdit, TEXT("") );
		else if( length < 0 )
			SetWindowText( m_hEdit, sText );
		else
		{
			SetWindowText( m_hEdit, sText ); // TODO: set partial string.
			/*TCHAR* sBuffer = (TCHAR*)_alloca( (length+1) * sizeof(TCHAR) );
			sBuffer[0] = 0;
			_tcsncpy( sBuffer, sText, length );
			sBuffer[ length ] = 0;
			SetWindowText( m_hEdit, sBuffer );*/
		}
		if( m_bMultiline )
			UpdateHeight( hPropGrid );

		return TRUE;
	}


	INT GetText( HWND hPropGrid, LPTSTR sBuffer, INT bufferSize, BOOL& bSuccess ) const
	{
		if( !IsWindow( m_hEdit ) )
			return PGItem::GetText( hPropGrid, sBuffer, bufferSize, bSuccess );
		bSuccess = TRUE;
		return GetWindowText( m_hEdit, sBuffer, bufferSize );
	}



	BOOL SetFloat( HWND hPropGrid, FLOAT f )
	{
		TCHAR s[ 16 ]; s[0] = 0;
		StringCchPrintf( s, ARRAY_SIZE(s), TEXT("%g"), f );
		return SetText( hPropGrid, s, ARRAY_SIZE( s ) );
	}


	FLOAT GetFloat( HWND hPropGrid, BOOL& bSuccess ) const
	{
		TCHAR s[ 16 ]; s[0] = 0;

		INT len = GetText( hPropGrid, s, ARRAY_SIZE( s ), bSuccess );
		if( len <= 0 )
			return PGItem::GetFloat( hPropGrid, bSuccess );
		bSuccess = TRUE;
		return (FLOAT)_tstof( s );
	}



	BOOL SetInt( HWND hPropGrid, INT n )
	{
		TCHAR s[ 16 ]; s[0] = 0;
		StringCchPrintf( s, ARRAY_SIZE(s), TEXT("%d"), n );
		return SetText( hPropGrid, s, ARRAY_SIZE( s ) );
	}


	INT GetInt( HWND hPropGrid, BOOL& bSuccess ) const
	{
		TCHAR s[ 16 ]; s[0] = 0;

		INT len = GetText( hPropGrid, s, ARRAY_SIZE( s ), bSuccess );
		if( len <= 0 )
			return PGItem::GetInt( hPropGrid, bSuccess );
		bSuccess = TRUE;
		return _tstoi( s );
	}



	LRESULT MsgProc( HWND hPropGrid, UINT Msg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
	{
		switch( Msg )
		{
		case WM_COMMAND:
			if( (HWND)lParam == m_hEdit && IsWindow( m_hEdit ) )
			{
				bHandled = TRUE;
				if( HIWORD( wParam ) == EN_CHANGE )
				{
					//if( !m_bDirty )
						//DebugPrintfLine( TEXT("Marking as dirty") );
					m_bDirty = TRUE;
					PropertyGrid_Notify( hPropGrid, PGN_EDITING, GetHandle(), NULL );
				}
				else if( HIWORD( wParam ) == EN_UPDATE )
				{
					UpdateHeight( hPropGrid );
				}
				else if( HIWORD( wParam ) == EN_KILLFOCUS && m_bDirty )
				{
					m_bDirty = FALSE;
					// TODO: This won't work as-is with the file-browser property type.
					//DebugPrintfLine( TEXT("EN_KILLFOCUS") );
					PropertyGrid_Notify( hPropGrid, PGN_PROPERTYCHANGING, GetHandle(), NULL );
					PropertyGrid_Notify( hPropGrid, PGN_PROPERTYCHANGED, GetHandle(), PG_FIRST_CHANGE );
				}
				return 0;
			}
			break;
		}
		return 0;
	}


protected:
	LPTSTR m_sValue;
	HWND m_hEdit;
	UINT m_uVisibleLines;
	UINT m_uMaxLines;
	BOOL m_bReadOnly;
	BOOL m_bMultiline;
	BOOL m_bAutoSize;
	BOOL m_bDirty;
};




PGItemTypeT< PGEditItem > g_PGEditItemType(TEXT("Edit"));



PGItemType* PropertyGrid_GetEditType() { return &g_PGEditItemType; }






HPGITEM PropertyGrid_InsertEdit( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent,
								HPGITEM hInsertBefore, LPVOID pUserData, LPCTSTR sInitialText )
{
	HPGITEM hItem = PropertyGrid_InsertItem( hPropGrid,
		g_PGEditItemType.AllocateItem( hPropGrid, NULL ),
		id, sTitle, hParent, hInsertBefore, pUserData, NULL );
	if( !hItem )
		return NULL;
	PropertyGrid_SetText( hPropGrid, hItem, sInitialText, -1 );
	return hItem;
}




BOOL PGEdit_SetMultiLine( HWND hPropGrid, HPGITEM hItem,
	BOOL bMultiline, UINT maxVisibleLines, BOOL bAutoSize )
{
	return ((PGEditItem*)hItem)->SetMultiline( hPropGrid, bMultiline, maxVisibleLines, bAutoSize );
}