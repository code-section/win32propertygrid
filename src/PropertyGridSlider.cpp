// This file implements the slider property type for the Angel3D Property Grid control.

# include <PropertyGridCtrl.h>
# include <PropertyGridItem.h>
# include <CSTB\win32\Trackbar.h>





class PGSliderItem : public PGItem
{
public:
	CSTB::Trackbar m_slider;
	INT m_nMin, m_nMax, m_nSelStart, m_nSelEnd;
	INT m_nPos, m_nPrevPos; // For proper notification behavior.
	BOOL m_bEnableSelRange;
	BOOL m_bShow;

	PGSliderItem( PGItemType& type ) : PGItem( type )
	{
		m_bShow = FALSE;
		m_nMin = m_nSelStart = m_nSelEnd = 0;
		m_nMax = 100;
		m_nPos = m_nPrevPos = 0;
		m_bEnableSelRange = FALSE;
	}


	virtual ~PGSliderItem() { GuiAssert( m_slider.hWnd == NULL ); }



protected:
	VOID CreateSlider( HWND hPropGrid )
	{
		GuiAssert( m_slider.hWnd == NULL );
		DWORD dwStyle = WS_CHILD | TBS_TOOLTIPS | TBS_DOWNISLEFT;

		if( FALSE == m_slider.Create( 0, dwStyle, 0,
			0, 0, 10, 10, m_nMin, m_nMax, m_nSelStart, m_nSelEnd,
			hPropGrid, (HINSTANCE)GetWindowLongPtr( hPropGrid, GWLP_HINSTANCE ) ) )
		{
			DebugPrintf( TEXT( "Failed to create trackbar control! GLE: %d\n" ), GetLastError() );
			return;
		}
		m_slider.SetPos( m_nPos, FALSE );
		if( !PropertyGrid_IsItemEnabled( GetHandle() ) )
			EnableWindow( m_slider, FALSE );
	}



public:
	VOID OnInsert( HWND hPropGrid )
	{
		INT defHeight = PropertyGrid_GetDefaultItemHeight( hPropGrid );
		INT scrollHeight = GetSystemMetrics( SM_CYHSCROLL );
		PropertyGrid_SetItemHeight( hPropGrid, GetHandle(),
			MAX( defHeight, scrollHeight + 2 * PG_PADDING_PIXELS_V ) );
	}




	VOID OnShow( HWND hPropGrid, BOOL bShow )
	{
		m_bShow = bShow;
		if( bShow )
		{
			CreateSlider( hPropGrid );
			OnSizeMove( hPropGrid );
			ShowWindow( m_slider, SW_SHOW );
		}
		else
		{
			DestroyWindow( m_slider );
			m_slider.Detach();
		}
	}




	VOID OnSizeMove( HWND hPropGrid )
	{
		if( m_slider.hWnd == NULL )
			return;
		RECT rContent;
		PropertyGrid_GetItemRects( hPropGrid, GetHandle(), NULL, &rContent, NULL, NULL, NULL, NULL );
		rContent.left += PG_PADDING_PIXELS_H;
		//rContent.top += PG_PADDING_PIXELS_V;
		//InflateRect( &rContent, 0, -PG_PADDING_PIXELS_V );
		MoveWindow( m_slider, rContent.left, rContent.top, RectWidth(rContent), RectHeight(rContent), TRUE );
	}




	VOID OnEnable( HWND hPropGrid, BOOL bEnable )
	{ if( m_slider.hWnd ) EnableWindow( m_slider, bEnable ); }




	BOOL OnEdit( HWND hPropGrid )
	{
		GuiAssert( m_slider.hWnd );
		SetFocus( m_slider );
		return TRUE;
	}



	INT GetInt( HWND hPropGrid, BOOL& bSuccess ) const { return m_nPos; }
	
	BOOL SetInt( HWND hPropGrid, INT n ) { m_nPos = n; if( m_slider.hWnd ) m_slider.SetPos( n, TRUE ); return TRUE; }



	LRESULT MsgProc( HWND hPropGrid, UINT Msg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
	{
		switch( Msg )
		{
		/*case WM_NOTIFY:
			if( m_slider.hWnd && (HWND)lParam == m_slider.hWnd )
			{
				NMHDR* pNM = (NMHDR*)lParam;
				DebugPrintfLine( TEXT("WM_NOTIFY from slider. %d"), pNM->code );
			}
			return 0;*/

		case WM_HSCROLL:
			if( (HWND)lParam == m_slider.hWnd )
			{
				static BOOL bChanged = FALSE;
				static BOOL bEditing = FALSE;

				INT newPos = m_slider.GetPos();
				bChanged = newPos != m_nPrevPos;
				m_nPrevPos = newPos;
				m_nPos = newPos;
				
				if( bEditing == FALSE && bChanged )
				{
					bEditing = TRUE;
					PropertyGrid_Notify( hPropGrid, PGN_EDITSTART, GetHandle(), NULL );
				}

				if( bChanged )
				{
					PropertyGrid_Notify( hPropGrid, PGN_PROPERTYCHANGING, GetHandle(), NULL );
					PropertyGrid_Notify( hPropGrid, PGN_PROPERTYCHANGED, GetHandle(), NULL );
				}

				if( bEditing && LOWORD(wParam) == SB_ENDSCROLL )
					PropertyGrid_Notify( hPropGrid, PGN_EDITDONE, GetHandle(), NULL );
				return 0;
			}
			break;
		}
		return 0;
	}
};




PGItemTypeT< PGSliderItem > g_PGSliderItemType( TEXT("Slider" ) );



PGItemType* PropertyGrid_GetSliderType() { return &g_PGSliderItemType; }




HPGITEM PropertyGrid_InsertSlider( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent, HPGITEM hInsertBefore, LPVOID pUserData )
{
	HPGITEM hItem = PropertyGrid_InsertItem( hPropGrid,
		g_PGSliderItemType.AllocateItem( hPropGrid, NULL ),
		id, sTitle, hParent, hInsertBefore,
		pUserData, NULL );
	return hItem;
}




HPGITEM PropertyGrid_InsertSlider( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent, HPGITEM hInsertBefore,
								  LPVOID pUserData, INT nMin, INT nMax, INT nPos,
								  INT nSelMin, INT nSelMax, BOOL bEnableSelRange )
{
	HPGITEM hItem = PropertyGrid_InsertSlider( hPropGrid, id, sTitle, hParent, hInsertBefore, pUserData );
	if( !hItem )
		return NULL;
	PGSlider_SetRange( hPropGrid, hItem, nMin, nMax );
	PGSlider_SetSel( hPropGrid, hItem, nSelMin, nSelMax );
	PGSlider_EnableSelRange( hPropGrid, hItem, bEnableSelRange );
	PropertyGrid_SetInt( hPropGrid, hItem, nPos );
	return hItem;
}



VOID PGSlider_SetRange( HWND hPropGrid, HPGITEM hItem, INT nMin, INT nMax )
{
	PGSliderItem* pItem = (PGSliderItem*)hItem;
	pItem->m_nMin = nMin;
	pItem->m_nMax = nMax;
	if( pItem->m_slider )
		pItem->m_slider.SetRange( nMin, nMax, TRUE );
}

VOID PGSlider_GetRange( HWND hPropGrid, HPGITEM hItem, INT* pMin, INT* pMax )
{
	PGSliderItem* pItem = (PGSliderItem*)hItem;
	if( pMin ) *pMin = pItem->m_nMin;
	if( pMax ) *pMax = pItem->m_nMax;
}

VOID PGSlider_SetSel( HWND hPropGrid, HPGITEM hItem, INT nStart, INT nEnd )
{
	PGSliderItem* pItem = (PGSliderItem*)hItem;
	pItem->m_nSelStart = nStart;
	pItem->m_nSelEnd = nEnd;
	if( pItem->m_slider )
		pItem->m_slider.SetSel( nStart, nEnd, TRUE );
}

VOID PGSlider_GetSel( HWND hPropGrid, HPGITEM hItem, INT* pStart, INT* pEnd )
{
	PGSliderItem* pItem = (PGSliderItem*)hItem;
	if( pStart ) *pStart = pItem->m_nSelStart;
	if( pEnd ) *pEnd = pItem->m_nSelEnd;
}

VOID PGSlider_EnableSelRange( HWND hPropGrid, HPGITEM hItem, BOOL bEnable )
{
	//PGSliderItem* pItem = (PGSliderItem*)hItem;
	DebugPrintf( TEXT( "Not yet impleneted!\n" ) );
}

BOOL PGSlider_IsSelRangeEnabled( HWND hPropGrid, HPGITEM hItem )
{
	PGSliderItem* pItem = (PGSliderItem*)hItem;
	return pItem->m_bEnableSelRange;
}
