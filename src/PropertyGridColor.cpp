// This file implements the color property type for the Angel3D Property Grid control.

# include <PropertyGridCtrl.h>
# include <PropertyGridItem.h>
# include <CSTB\Win32\ColorBoxCtrl.h>
# include <CSTB\Win32\Win32Util.h>




class PGColorItem : public PGItem
{
public:
	COLORREF	m_color;
	HWND		m_hColorBox;

	PGColorItem( PGItemType& type ) : PGItem( type )
	{
		m_color = RGB(255,255,255);
		m_hColorBox = NULL;
	}
	virtual ~PGColorItem() { GuiAssert( m_hColorBox == NULL ); }



	BOOL OnEdit( HWND hPropGrid )
	{
		GuiAssert( m_hColorBox );
		SetFocus( m_hColorBox );
		return TRUE;
	}



	BOOL CreateColorBox( HWND hPropGrid )
	{
		GuiAssert( m_hColorBox == NULL );
		m_hColorBox = CreateWindowEx( 0, WC_COLORBOX, NULL,
			WS_CHILD | WS_BORDER,
			0, 0, 10, 10, hPropGrid, NULL,
			(HINSTANCE)GetWindowLongPtr( hPropGrid, GWLP_HINSTANCE ),
			NULL );
		if( !m_hColorBox )
		{
			DebugPrintf( TEXT( "\nFailed to create color box control! GLE: %d" ), GetLastError() );
			return FALSE;
		}
		ColorBox_SetColor( m_hColorBox, m_color, FALSE, FALSE );
		if( !PropertyGrid_IsItemEnabled( GetHandle() ) )
			EnableWindow( m_hColorBox, FALSE );
		return TRUE;
	}




	VOID OnShow( HWND hPropGrid, BOOL bShow )
	{
		if( bShow )
		{
			if( !CreateColorBox( hPropGrid ) )
				return;
			OnSizeMove( hPropGrid );
			ShowWindow( m_hColorBox, SW_SHOW );
		}
		else
			SAFE_DESTROY_WINDOW( m_hColorBox );
	}




	VOID OnSizeMove( HWND hPropGrid )
	{
		GuiAssert( m_hColorBox );
		//GuiAssert( PropertyGrid_IsItemVisible( hPropGrid, GetHandle() ) );
		RECT rContent;
		PropertyGrid_GetItemRects( hPropGrid, GetHandle(), NULL, &rContent, NULL, NULL, NULL, NULL );
		rContent.left += PG_PADDING_PIXELS_H;
		//rContent.top += PG_PADDING_PIXELS_V;
		//InflateRect( &rContent, 0, -PG_PADDING_PIXELS_V );
		MoveWindow( m_hColorBox, rContent.left, rContent.top, RectWidth(rContent), RectHeight(rContent), TRUE );
	}



	VOID OnEnable( HWND hPropGrid, BOOL bEnable )
	{ if( m_hColorBox ) EnableWindow( m_hColorBox, bEnable ); }


	// Prevent any drawing in the content area, as the combo box is occupying it.
	//VOID PaintContent( HWND hPropGrid, HDC hDC, RECT* prcPaint, RECT* prcContent ) { }



	INT GetInt( HWND hPropGrid, BOOL& bSuccess ) const
	{
		bSuccess = TRUE;
		return (INT)m_color;
	}



	BOOL SetInt( HWND hPropGrid, INT n )
	{
		m_color = (COLORREF)n;
		if( m_hColorBox )
			ColorBox_SetColor( m_hColorBox, m_color, TRUE, FALSE );
		return TRUE;
	}



	LRESULT MsgProc( HWND hPropGrid, UINT Msg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
	{
		switch( Msg )
		{
		case WM_NOTIFY:
			if( m_hColorBox && ((LPNMHDR)lParam)->hwndFrom == m_hColorBox )
			{
				NMHDR& nmh = *(NMHDR*)lParam;

				if( nmh.code == CBXN_COLORCHANGING || nmh.code == CBXN_COLORCHANGE )
				{
					//g_Log( TEXT( "Notification from color box: %x" ), nmh.code );
					bHandled = TRUE;
					UINT code = (nmh.code == CBXN_COLORCHANGING) ? PGN_PROPERTYCHANGING : PGN_PROPERTYCHANGED;
					m_color = ColorBox_GetColor( m_hColorBox );
					return PropertyGrid_Notify( hPropGrid, code, GetHandle(), PG_FIRST_CHANGE );
				}
			}
			break;
		}
		return 0;
	}
};




PGItemTypeT< PGColorItem > g_PGColorItemType(TEXT("Color") );



PGItemType* PropertyGrid_GetColorBoxType() { return &g_PGColorItemType; }




HPGITEM PropertyGrid_InsertColor( HWND hPropGrid, UINT id, LPCTSTR sTitle,
								HPGITEM hParent, HPGITEM hInsertBefore,
								LPVOID pUserData, COLORREF color )
{
	if( !ColorBox_Init( (HINSTANCE)GetWindowLongPtr( hPropGrid, GWLP_HINSTANCE ) ) )
	{
		DebugPrintf( TEXT("\nFailed to initialize the color box control class!" ) );
		return NULL;
	}
	HPGITEM hItem = PropertyGrid_InsertItem( hPropGrid,
		g_PGColorItemType.AllocateItem( hPropGrid, NULL ),
		id, sTitle, hParent, hInsertBefore, pUserData, NULL );
	if( hItem )
		PropertyGrid_SetInt( hPropGrid, hItem, (INT)color );
	return hItem;
}




BOOL PropertyGrid_SetColor( HWND hPropGrid, HPGITEM hItem, COLORREF color )
{ return PropertyGrid_SetInt( hPropGrid, hItem, (INT)color ); }




COLORREF PropertyGrid_GetColor( HWND hPropGrid, HPGITEM hItem, BOOL* pSuccess )
{ return (COLORREF)PropertyGrid_GetInt( hPropGrid, hItem, pSuccess ); }