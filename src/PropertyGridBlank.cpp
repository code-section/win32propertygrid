// This file has the definitions for the default property types for use with the property grid control.

# include <PropertyGridCtrl.h>
# include <PropertyGridItem.h>




class : public PGItemType
{
public:
	virtual LPCTSTR GetName() { return TEXT("Blank"); }
	virtual PGItem* AllocateItem( HWND hPropGrid, LPVOID pCreationData ) { return new PGItem( *this ); }
	virtual VOID DeleteItem( HWND hPropGrid, PGItem* pItem ) { delete pItem; }
} g_BlankItemType;


PGItemType* PropertyGrid_GetBlankType() { return &g_BlankItemType; }