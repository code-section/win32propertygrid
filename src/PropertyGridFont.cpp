// Font item type for the win32 property grid control.
// http://code-sectino.com


# include <PropertyGridCtrl.h>
# include <PropertyGridItem.h>
# include <CSTB\win32\Win32Util.h>



class PGFontItem : public PGItem
{
	LOGFONT m_lf = {0};
	HFONT m_hFont = 0;
public:
	PGFontItem( PGItemType& type ) : PGItem( type ) { }


	HFONT GetFont() const { return m_hFont; }
	const LOGFONT* GetLogFont() const { return &m_lf; }




	HFONT SetFont( HWND hPropGrid, LOGFONT* pLF )
	{
		DeleteObject( m_hFont );
		m_hFont = CreateFontIndirect( pLF );
		if( pLF != &m_lf )
			m_lf = *pLF;

		int height = abs(m_lf.lfHeight) + 2 * PG_PADDING_PIXELS_V;
		height = MAX( height, 8 );
		PropertyGrid_SetItemHeight( hPropGrid, (HPGITEM)this, height );

		return m_hFont;
	}



	void OnRemove( HWND hPropGrid )
	{
		DeleteObject( m_hFont );
		m_hFont = 0;
	}



	VOID PaintContent( HWND hPropGrid, HDC hDC, RECT* prcPaint, RECT* prcContent )
	{
		int oldBkMode = SetBkMode( hDC, TRANSPARENT );
		COLORREF oldColor = SetTextColor( hDC, RGB(0,0,0) );
		FillRect( hDC, prcContent, (HBRUSH)(COLOR_3DFACE+1));

		prcContent->left += PG_PADDING_PIXELS_H;

		HGDIOBJ hOldFont = 0;
		if( m_hFont )
			hOldFont = SelectObject( hDC, m_hFont );

		LPCTSTR sText = TEXT("(not set)");
		if( m_lf.lfFaceName[0] )
			sText = m_lf.lfFaceName;
		DrawText( hDC, sText, -1, prcContent, DT_SINGLELINE | DT_VCENTER );

		if( m_hFont )
			SelectObject( hDC, hOldFont );

		SetBkMode( hDC, oldBkMode );
		SetTextColor( hDC, oldColor );
	}




	BOOL ChooseFont( HWND hPropGrid )
	{
		HDC hdc = 0;
		CHOOSEFONT cf = {0};

		cf.lStructSize = sizeof(CHOOSEFONT);
		cf.hwndOwner = hPropGrid;
		cf.lpLogFont = &m_lf;
		cf.Flags = CF_SCREENFONTS | CF_EFFECTS;
		if( m_hFont )
			cf.Flags |= CF_INITTOLOGFONTSTRUCT;

		if( !::ChooseFont( &cf ) )
			return FALSE;

		SetFont( hPropGrid, &m_lf );

		PropertyGrid_Notify( hPropGrid, PGN_PROPERTYCHANGED, GetHandle(), (LPVOID)m_hFont );

		return TRUE;
	}



	BOOL OnEdit( HWND hPropGrid )
	{
		ChooseFont( hPropGrid );
		return FALSE; // We don't need to stay in edit mode, we just show the dialog and return.
	}
};


PGItemTypeT< PGFontItem > g_PGFontItemType( TEXT("Font") );

PGItemType* PropertyGrid_GetFontType() { return &g_PGFontItemType; }




HFONT PGFont_GetFont( HPGITEM hItem )
{
	PGFontItem* pItem = g_PGFontItemType.GetItem( hItem );
	if( !pItem ) return 0;
	return pItem->GetFont();
}




const LOGFONT* PGFont_GetLogFont( HPGITEM hItem )
{
	PGFontItem* pItem = g_PGFontItemType.GetItem( hItem );
	return pItem ? pItem->GetLogFont() : 0;
}




HFONT PGFont_SetFont( HWND hPropGrid, HPGITEM hItem, LOGFONT* pLF )
{
	PGFontItem* pItem = g_PGFontItemType.GetItem( hItem );
	return pItem ? pItem->SetFont( hPropGrid, pLF ) : 0;
}
