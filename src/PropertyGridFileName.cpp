// This file implements the file name property type for the Angel3D Property Grid control.

# include <PropertyGridCtrl.h>
# include <PropertyGridItem.h>
# include <CSTB\Win32\Win32Util.h>




class PGFileNameItem : public PGItem
{
public:
	HWND			m_hEdit;
	HWND			m_hButton;
	OPENFILENAME	m_ofn;
	BOOL			m_bDirty;

	PGFileNameItem( PGItemType& type ) : PGItem( type )
	{
		ZeroMemory( &m_ofn, sizeof(OPENFILENAME) );
		m_ofn.lStructSize = sizeof(OPENFILENAME);
		m_ofn.Flags = OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR;
		m_bDirty = FALSE;
	}


	virtual ~PGFileNameItem()
	{
		GuiAssert( m_hEdit == NULL );
		GuiAssert( m_hButton == NULL );
	}



	VOID OnInsert( HWND hPropGrid )
	{
		HINSTANCE hInstance = (HINSTANCE)GetWindowLongPtr( hPropGrid, GWLP_HINSTANCE );
		DWORD dwEditStyle = WS_CHILD | ES_AUTOHSCROLL;

		m_hEdit = CreateWindowEx( WS_EX_CLIENTEDGE, WC_EDIT, 0, dwEditStyle, 0, 0, 10, 10, hPropGrid, 0, hInstance, 0 );
		m_hButton = CreateWindowEx( 0, WC_BUTTON, TEXT("..."), WS_CHILD, 0, 0, 10, 10, hPropGrid, 0, hInstance, 0 );

		SendMessage( m_hEdit, WM_SETFONT, (WPARAM)SendMessage( hPropGrid, WM_GETFONT, 0, 0 ), 0 );
		SendMessage( m_hButton, WM_SETFONT, (WPARAM)SendMessage( hPropGrid, WM_GETFONT, 0, 0 ), 0 );
	}




	VOID OnSizeMove( HWND hPropGrid )
	{
		if( !m_hEdit || !m_hButton )
			return;
		RECT rContent;
		PropertyGrid_GetItemRects( hPropGrid, GetHandle(), NULL, &rContent, NULL, NULL, NULL, NULL );
		rContent.left += PG_PADDING_PIXELS_H;
		//rContent.top += PG_PADDING_PIXELS_V;
		//InflateRect( &rContent, -PG_PADDING_PIXELS_H, -PG_PADDING_PIXELS_V )
		INT buttonWidth = 36;
		RECT rEdit = rContent;
		rEdit.right = MAX( rEdit.left, rEdit.right - buttonWidth );
		RECT rButton = rContent;
		rButton.left = rEdit.right;
		MoveWindow( m_hEdit, rEdit.left, rEdit.top, RectWidth(rEdit), RectHeight(rEdit), TRUE );
		MoveWindow( m_hButton, rButton.left, rButton.top, RectWidth(rButton), RectHeight(rButton), TRUE );
	}




	VOID OnShow( HWND hPropGrid, BOOL bShow )
	{
		if( bShow )
			OnSizeMove( hPropGrid );
		ShowWindow( m_hEdit, bShow ? SW_SHOW : SW_HIDE );
		ShowWindow( m_hButton, bShow ? SW_SHOW : SW_HIDE );
	}



	VOID OnEnable( HWND hPropGrid, BOOL bEnable )
	{
		EnableWindow( m_hEdit, bEnable );
		EnableWindow( m_hButton, bEnable );
	}




	VOID OnRemove( HWND hPropGrid )
	{
		SAFE_DESTROY_WINDOW( m_hEdit );
		SAFE_DESTROY_WINDOW( m_hButton );
	}



	BOOL OnEdit( HWND hPropGrid )
	{
		SetFocus( m_hEdit );
		SendMessage( m_hEdit, EM_SETSEL, 0, -1 );
		return TRUE;
	}



	VOID PaintContent( HWND hPropGrid, HDC hDC, RECT* prcPaint, RECT* prcContent )
	{ FillRect( hDC, prcContent, GetSysColor( COLOR_WINDOW ) ); }




	BOOL SetText( HWND hPropGrid, LPCTSTR sText, INT len )
	{
		if( !m_hEdit )
			return FALSE;
		if( len == 0 || sText == NULL )
			SetWindowText( m_hEdit, 0 );
		else
			SetWindowText( m_hEdit, sText );
		return TRUE;
	}




	INT GetText( HWND hPropGrid, LPTSTR sBuffer, INT bufferSize, BOOL& bSuccess ) const
	{
		if( !m_hEdit )
			return PGItem::GetText( hPropGrid, sBuffer, bufferSize, bSuccess );
		bSuccess = TRUE;
		return GetWindowText( m_hEdit, sBuffer, bufferSize );
	}




	LRESULT MsgProc( HWND hPropGrid, UINT Msg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
	{
		HPGITEM hItem = GetHandle();
		switch( Msg )
		{
		case WM_COMMAND:
			if( m_hEdit && (HWND)lParam == m_hEdit )
			{
				bHandled = TRUE;
				if( HIWORD( wParam ) == EN_CHANGE ) // Sent after control redraws.
				{
					m_bDirty = TRUE;
					PropertyGrid_Notify( hPropGrid, PGN_EDITING, hItem, NULL );
				}
				else if( HIWORD( wParam ) == EN_KILLFOCUS && m_bDirty )
				{
					m_bDirty = FALSE;
					PropertyGrid_Notify( hPropGrid, PGN_PROPERTYCHANGING, hItem, NULL );
					PropertyGrid_Notify( hPropGrid, PGN_PROPERTYCHANGED, hItem, PG_FIRST_CHANGE );
				}
				return 0;
			}
			else if( m_hButton && (HWND)lParam == m_hButton )
			{
				bHandled = TRUE;
				BOOL bSuccess = FALSE;
				TCHAR sFile[ MAX_PATH ];
				sFile[0] = 0;
				GetText( hPropGrid, sFile, MAX_PATH, bSuccess );
				m_ofn.lpstrFile = sFile;
				m_ofn.nMaxFile = MAX_PATH;
				if( FALSE == GetOpenFileName( &m_ofn ) )
					return 0;

				SetText( hPropGrid, sFile, MAX_PATH );
				m_bDirty = FALSE;
				PropertyGrid_Notify( hPropGrid, PGN_PROPERTYCHANGING, hItem, NULL );
				PropertyGrid_Notify( hPropGrid, PGN_PROPERTYCHANGED, hItem, PG_FIRST_CHANGE );
			}
			break;
		}
		
		return 0;
	}
};




PGItemTypeT< PGFileNameItem > g_PGOpenFileNameType(TEXT("FileName"));



PGItemType* PropertyGrid_GetFileNameType() { return &g_PGOpenFileNameType; }




OPENFILENAME* PGFileName_GetOFN( HWND hPropGrid, HPGITEM hItem )
{ return &((PGFileNameItem*)hItem)->m_ofn; }





HPGITEM PropertyGrid_InsertFileName( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent,
									HPGITEM hInsertBefore, LPVOID pUserData, LPCTSTR sInitialText )
{
	HPGITEM hItem = PropertyGrid_InsertItem( hPropGrid,
		g_PGOpenFileNameType.AllocateItem( hPropGrid, NULL ),
		id, sTitle, hParent,
		hInsertBefore, pUserData, NULL );
	if( !hItem ) return NULL;
	PropertyGrid_SetText( hPropGrid, hItem, sInitialText, -1 );
	return hItem;
}