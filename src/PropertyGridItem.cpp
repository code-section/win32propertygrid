# include <PropertyGridItem.h>
# include "PropertyGridCtrlImpl.h"
# include <strsafe.h>




PGItem::PGItem( PGItemType& type ) : m_type( type )
{
	m_uID = 0;
	m_hMenu = NULL;
	m_bMenuShared = FALSE;
	m_sTitle = NULL;
	m_pos = 0;
	m_height = 0;
	m_dwState = 0;
	m_pUserData = NULL;
}



PGItem::~PGItem()
{
	SetTitle( NULL, NULL );
	SetMenu( NULL, NULL, FALSE );
}




VOID PGItem::SetTitle( HWND hPropGrid, LPCTSTR sTitle )
{
	if( m_sTitle && m_sTitle != LPSTR_TEXTCALLBACK )
		delete [] m_sTitle;
	
	m_sTitle = NULL;

	if( sTitle == LPSTR_TEXTCALLBACK )
		m_sTitle = LPSTR_TEXTCALLBACK;
	else if( sTitle && sTitle[0] != 0 )
	{
		INT len = _tcslen( sTitle );
		if( len > 0 )
		{
			m_sTitle = new TCHAR[ len+1 ];
			//_tcsncpy_s( m_sTitle, len+1, sTitle, len );
			StringCchCopy( m_sTitle, len+1, sTitle );
		}
	}

	if( hPropGrid )
		PropertyGrid_InvalidateItem( hPropGrid, GetHandle() );
}




LPCTSTR PGItem::GetTitle( HWND hPropGrid ) const
{
	if( m_sTitle != LPSTR_TEXTCALLBACK )
		return m_sTitle;
	if( !hPropGrid )
		return NULL;
	return (LPCTSTR)PropertyGrid_Notify( hPropGrid, PGN_GETITEMTITLE, GetHandle(), NULL );
}




VOID PGItem::SetMenu( HWND hPropGrid, HMENU hMenu, BOOL bShared )
{
	if( hMenu == m_hMenu )
		return;

	BOOL bHadMenu = m_hMenu != NULL;
	BOOL bHasMenu = hMenu != NULL;

	if( m_hMenu && !m_bMenuShared )
		DestroyMenu( m_hMenu );

	m_hMenu = hMenu;
	
	if( hPropGrid && bHadMenu != bHasMenu )
		PropertyGrid_InvalidateItem( hPropGrid, GetHandle() );
}




VOID PGItem::PaintTitleBG( HWND hPropGrid, HDC hDC, RECT* prcPaint, RECT* prcTitle )
{
	FillRect( hDC, prcTitle, GetBackgroundColor( hPropGrid ) );
	//FillRect( hDC, prcTitle, (HBRUSH)(COLOR_WINDOW+1) );
	RECT rLeftShaft = *prcTitle;
	rLeftShaft.right = PG_INDENTATION_PIXELS;
	FillRect( hDC, &rLeftShaft, (HBRUSH)(COLOR_BTNSHADOW+1) );
	//FillRect( hDC, &rLeftShaft, (HBRUSH)(COLOR_3DFACE+1) );
	if( PropertyGrid_GetFocusItem( hPropGrid ) == GetHandle() && GetFocus() == hPropGrid )
	{
		RECT rFocus = *prcTitle;
		//rFocus.left = PG_PADDING_PIXELS_H + PG_INDENTATION_PIXELS * PropertyGrid_GetItemLevel( hPropGrid, hItem );
		rFocus.left += PG_PADDING_PIXELS_H;
		rFocus.right -= PG_PADDING_PIXELS_H;
		rFocus.top += PG_PADDING_PIXELS_V;
		rFocus.bottom -= PG_PADDING_PIXELS_V;
		DrawFocusRect( hDC, &rFocus );
	}

	if( PropertyGrid_IsItemSelected( GetHandle() ) )
		pgGlobals.hilighter.FillRect( hDC, *prcTitle, RGB(0, 100, 100), 64 );
		//PaintHighlightRect( hPropGrid, hDC, prcTitle );
}




VOID PGItem::PaintText( HWND hPropGrid, HDC hDC, RECT* prcPaint, RECT* prcText )
{
	INT oldBkMode = SetBkMode( hDC, TRANSPARENT );
	COLORREF oldTextColor = SetTextColor( hDC, GetSysColor( COLOR_BTNTEXT ) );

	DrawText( hDC, PropertyGrid_GetItemTitle( hPropGrid, GetHandle() ), -1, prcText,
		DT_LEFT | DT_VCENTER | DT_SINGLELINE );

	SetBkMode( hDC, oldBkMode );
	SetTextColor( hDC, oldTextColor );
}




VOID PGItem::PaintMenuArrow( HWND hPropGrid, HDC hDC, RECT* prcMenuArrow )
{
	INT oldBkMode = SetBkMode( hDC, TRANSPARENT );
	COLORREF oldTextColor = SetTextColor( hDC, GetSysColor( COLOR_BTNTEXT ) );
	HFONT oldFont = (HFONT)SelectObject( hDC, pgGlobals.hSymbolFont );

	TCHAR c = 0x36;
	DrawText( hDC, &c, 1, prcMenuArrow, DT_SINGLELINE | DT_VCENTER | DT_CENTER );

	SetBkMode( hDC, oldBkMode );
	SetTextColor( hDC, oldTextColor );
	SelectObject( hDC, oldFont );
}




VOID PGItem::PaintExpandGlyph( HWND hPropGrid, HDC hDC, RECT* prcGlyph )
{
	HBRUSH hOldBrush = (HBRUSH)SelectObject( hDC, (HBRUSH)GetStockObject( HOLLOW_BRUSH ) );
	HPEN hOldPen = (HPEN)SelectObject( hDC, pgGlobals.hGlyphPen );

	RECT& rGlyph = *prcGlyph;

	UINT drawSize = 9;
	RECT rDraw = rGlyph;
	rDraw.top += RECTHEIGHT( rGlyph )/2 - drawSize/2;
	rDraw.left += PG_PADDING_PIXELS_H;
	rDraw.bottom = rDraw.top + drawSize;
	rDraw.right = rDraw.left + drawSize;
	prcGlyph = &rDraw;

	Rectangle( hDC, prcGlyph->left, prcGlyph->top, prcGlyph->right, prcGlyph->bottom );
	UINT midY = prcGlyph->top + (prcGlyph->bottom - prcGlyph->top) / 2;
	UINT midX = prcGlyph->left + (prcGlyph->right - prcGlyph->left) / 2;

	MoveToEx( hDC, prcGlyph->left + 2, midY, NULL );
	LineTo( hDC, prcGlyph->right - 2, midY );

	// Draw vertical line if required.
	if( PropertyGrid_GetItemState( GetHandle() ) & PGIS_COLLAPSED )
	{
		MoveToEx( hDC, midX, prcGlyph->top + 2, NULL );
		LineTo( hDC, midX, prcGlyph->bottom - 2 );
	}
	SelectObject( hDC, hOldBrush );
	SelectObject( hDC, hOldPen );
}





BOOL PropertyGrid_SetText( HWND hPropGrid, HPGITEM hItem, LPCTSTR sNewText, INT length )
{ if( !hItem ) return FALSE; return CONV(hItem)->SetText( hPropGrid, sNewText, length ); }

BOOL PropertyGrid_SetInt( HWND hPropGrid, HPGITEM hItem, INT n )
{ if( !hItem ) return FALSE; return CONV(hItem)->SetInt( hPropGrid, n ); }

BOOL PropertyGrid_SetFloat( HWND hPropGrid, HPGITEM hItem, FLOAT f )
{ if( !hItem ) return FALSE; return CONV(hItem)->SetFloat( hPropGrid, f ); }

BOOL PropertyGrid_SetBool( HWND hPropGrid, HPGITEM hItem, BOOL b )
{ if( !hItem ) return FALSE; return CONV(hItem)->SetBool( hPropGrid, b ); }


INT PropertyGrid_GetText( HWND hPropGrid, HPGITEM hItem, LPTSTR sBuffer, INT bufferSize, BOOL* pSuccess )
{
	BOOL bSuccess = FALSE;
	if( !hItem ) return -1;
	return CONV(hItem)->GetText( hPropGrid, sBuffer, bufferSize, pSuccess ? *pSuccess : bSuccess );
}

INT PropertyGrid_GetInt( HWND hPropGrid, HPGITEM hItem, BOOL* pSuccess )
{
	BOOL bSuccess = FALSE;
	if( !hItem ) return FALSE;
	return CONV(hItem)->GetInt( hPropGrid, pSuccess ? *pSuccess : bSuccess );
}

FLOAT PropertyGrid_GetFloat( HWND hPropGrid, HPGITEM hItem, BOOL* pSuccess )
{
	BOOL bSuccess = FALSE;
	if( !hItem ) return FALSE;
	return CONV(hItem)->GetFloat( hPropGrid, pSuccess ? *pSuccess : bSuccess );
}

BOOL PropertyGrid_GetBool( HWND hPropGrid, HPGITEM hItem, BOOL* pSuccess )
{
	BOOL bSuccess = FALSE;
	if( !hItem ) return FALSE;
	return CONV(hItem)->GetBool( hPropGrid, pSuccess ? *pSuccess : bSuccess );
}