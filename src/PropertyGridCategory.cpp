// This file has the definitions for the default property types for use with the property grid control.

# include <PropertyGridCtrl.h>
# include <PropertyGridItem.h>
# include <tchar.h>
# include <CSTB\win32\Win32Util.h>



class PGCategoryItem : public PGItem
{
public:

	PGCategoryItem( PGItemType& type ): PGItem( type ) 
	{}

	BOOL HasContent( HWND hPropGrid ) { return FALSE; }




	// Comment out this method to disable drawing using bold font.
	VOID PaintText( HWND hPropGrid, HDC hDC, RECT* prcPaint, RECT* prcText )
	{
		static HFONT hFont = 0;
		if( !hFont )
		{
			LOGFONT lf = {0};
			GetObject( GetGuiFont(), sizeof(lf), &lf );
			lf.lfWeight = FW_BOLD;
			hFont = CreateFontIndirect( &lf );
		}

		//DrawText( hDC, s, MAX_PATH, prcText, DT_LEFT );
		auto oldFont = SelectObject( hDC, hFont );
		PGItem::PaintText( hPropGrid, hDC, prcPaint, prcText );
		SelectObject( hDC, oldFont );
	}




	COLORREF GetBackgroundColor( HWND hPropGrid )
	{
		COLORREF c2 = GetSysColor( COLOR_ACTIVECAPTION );//COLOR_WINDOW );
		COLORREF c1 = GetSysColor( COLOR_3DFACE );
		INT level = PropertyGrid_GetItemLevel( hPropGrid, CONV(this) );
		level--; //make level 0-based.
		float f = 0.65f;
		f -= level * 0.2f;
		f = MAX( f, 0.f );
		COLORREF c = InterpolateCOLORREF( c1, c2, f );
		return c;
	}

	virtual ~PGCategoryItem() { }
};




PGItemTypeT< PGCategoryItem > g_CategoryItemType(TEXT("Category") );



PGItemType* PropertyGrid_GetCategoryType() { return &g_CategoryItemType; }



HPGITEM PropertyGrid_InsertCategory( HWND hPropGrid, UINT id, LPCTSTR sTitle, HPGITEM hParent,
									HPGITEM hInsertBefore, LPVOID pUserData )
{
	return PropertyGrid_InsertItem( hPropGrid, &g_CategoryItemType, id,
		sTitle, hParent, hInsertBefore, pUserData, NULL );
}