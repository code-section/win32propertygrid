// This demonstrates creating a new item type. The demo creates a custom item and
// a couple of standard items below it to allow the user to edit some properties
// of the custom item.


# include <PropertyGridCtrl.h>
# include <PropertyGridItem.h>
# include <CSTB\Win32\Win32Util.h>



# define IDPGI_BG		1000
# define IDPGI_FG		1001



// This custom item has a background and foreground color. It will draw a simple string
// using these colors in the content area. It is not editable by itself.
class MyCustomItem : public PGItem
{
public:

	COLORREF bg, fg;



	MyCustomItem( PGItemType& type ) : PGItem( type )
	{
		bg = RGB(0,100,100);
		fg = RGB(255,255,255);
	}



	VOID PaintContent( HWND hPropGrid, HDC hDC, RECT* prcPaint, RECT* prcContent )
	{
		FillRect( hDC, prcContent, bg );
		COLORREF oldColor = SetTextColor( hDC, fg );
		int oldBkMode = SetBkMode( hDC, TRANSPARENT );
		DrawText( hDC, TEXT("custom item"), -1, prcContent, DT_CENTER | DT_VCENTER | DT_SINGLELINE );
		SetTextColor( hDC, oldColor );
		SetBkMode( hDC, oldBkMode );
	}
};




PGItemTypeT<MyCustomItem> g_myCustomItemType(TEXT("MyCustomItem"));





static LRESULT OnPropertyGridNotification( HWND hPropGrid, NMPROPGRID* pNM, void* )
{
	switch( pNM->hdr.code )
	{
	case PGN_PROPERTYCHANGED:
		if( pNM->idItem == IDPGI_FG || pNM->idItem == IDPGI_BG )
		{
			HPGITEM hCustom = PropertyGrid_GetParent( pNM->hItem );
			MyCustomItem* pCustom = g_myCustomItemType.GetItem( hCustom );
			if( !pCustom )
				return 0;
			COLORREF c = PropertyGrid_GetColor( hPropGrid, pNM->hItem );
			if( pNM->idItem == IDPGI_FG )
				pCustom->fg = c;
			else
				pCustom->bg = c;
			PropertyGrid_InvalidateItem( hPropGrid, hCustom );
			UpdateWindow( hPropGrid );
		}
		return 0;
	}
	return 0;
}




// We subclass the property grid control because we are using it as a popup window
// instead of a child window, and we need to generate a quit message so the message
// loop will exit and control will return to the main dialog window.
class PGSubclass : public WindowSubclass
{
public:
	PGSubclass(HWND h) : WindowSubclass(h) { }
	LRESULT MsgProc( UINT Msg, WPARAM wParam, LPARAM lParam )
	{
		if( Msg == WM_CLOSE ) PostQuitMessage( 0 );
		return WindowSubclass::MsgProc( Msg, wParam, lParam );
	}
};




void Demo_CustomItem( HINSTANCE hInstance, HWND hMainWindow )
{
	if( !PropertyGrid_Init( hInstance ) )
	{
		MessageBox( 0, TEXT("Failed to initialize PropertyGrid control class"), TEXT("ERROR"), MB_OK | MB_ICONSTOP );
		return;
	}

	unsigned width = 400, height = 600;
	RECT rWindow = { 0, 0, (long)width, (long)height };
	AdjustWindowRect( &rWindow, WS_OVERLAPPEDWINDOW, FALSE );

	HWND hPG = CreateWindowEx( NULL, WC_PROPERTYGRID, TEXT("Custom item demo"),
		WS_POPUPWINDOW | WS_CAPTION,
		0, 0, width, height, hMainWindow, 0, hInstance, 0 );

	PGSubclass subclass(hPG);

	PropertyGrid_SetNotificationCallback( hPG, OnPropertyGridNotification, 0 );

	HPGITEM hCustomItem = PropertyGrid_InsertItem( hPG, &g_myCustomItemType, 0, TEXT("Custom Item"), 0, 0, 0, 0 );
	MyCustomItem* pCustom = g_myCustomItemType.GetItem( hCustomItem );
	if( pCustom )
	{
		PropertyGrid_InsertColor( hPG, IDPGI_BG, TEXT("Background color"), hCustomItem, 0, 0, pCustom->bg );
		PropertyGrid_InsertColor( hPG, IDPGI_FG, TEXT("Foreground color"), hCustomItem, 0, 0, pCustom->fg );
	}



	ShowWindow( hPG, SW_SHOW );
	UpdateWindow( hPG );

	SetFocus( hPG );

	EnableWindow( hMainWindow, FALSE );


	MSG Msg = {0};

	while( GetMessage( &Msg, NULL, 0, 0 ) )
	{
		// IsPropertyGridMessage() allows the property grid control to implement keyboard navigation.
		if( !IsPropertyGridMessage( hPG, &Msg ) )
		{
			TranslateMessage( &Msg );
			DispatchMessage( &Msg );
		}
	}

	PropertyGrid_Uninit();

	EnableWindow( hMainWindow, TRUE );
	SetFocus( hMainWindow );
}