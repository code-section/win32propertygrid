// The basic demo creates a window with a property grid control embedded in it. The window also
// contains a read-only edit box to display text messages generated from the demo to the user.


// CSTB/Win32/Win32Util.h contains link directives to enable visual styles if this is defined.
# define WIN32UTIL_VISUAL_STYLES

# define STRICT
# define WIN32_LEAN_AND_MEAN
# include <windows.h>
# include <PropertyGridCtrl.h>
# include <CSTB\Win32\Win32Util.h>



HWND				g_hWnd = NULL;
HWND				g_hPG = NULL;
HWND				g_hEdit = NULL;

LRESULT CALLBACK	WindowProc( HWND, UINT, WPARAM, LPARAM );




void AppendEdit( HWND hEdit, LPCTSTR s )
{
	if( !hEdit || !s || !s[0] ) return;

	int start, end;

	SendMessage( hEdit, EM_GETSEL, (WPARAM)&start, (LPARAM)&end );
	INT length = (INT)SendMessage( hEdit, WM_GETTEXTLENGTH, 0L, 0L );
	SendMessage( hEdit, EM_SETSEL, length, length );
	SendMessage( hEdit, EM_REPLACESEL, FALSE, (LPARAM)s );
	SendMessage( hEdit, EM_SETSEL, start, end );
}




void OutputMessage( LPCTSTR s, bool newline = true )
{ AppendEdit( g_hEdit, s); if( newline ) AppendEdit( g_hEdit, TEXT("\r\n" ) ); }




LRESULT OnPropertyGridNotification( HWND hPropGrid, NMPROPGRID* pNM, void* )
{
	switch( pNM->hdr.code )
	{
	case PGN_COLLAPSEITEM:
		OutputMessage( TEXT("PGN_COLLAPSEITEM") );
		return 0;

	case PGN_SELCHANGED:
		OutputMessage( TEXT("PGN_SELCHANGED" ) );
		if( pNM->hItem )
		{
			PGITEMINFO itemInfo = PropertyGrid_GetItemInfo( hPropGrid, pNM->hItem );
			OutputMessage( TEXT("  selected item title: " ), false );
			OutputMessage( itemInfo.sTitle ? itemInfo.sTitle : TEXT("<empty>" ) );
		}
		else
			OutputMessage( TEXT("Nothing selected" ) );
		return 0;


	case PGN_EDITDONE:
		OutputMessage( TEXT("PGN_EDITDONE: user has finished editing an item" ) );
		return 0;

	case PGN_PROPERTYCHANGED:
		OutputMessage( TEXT("PGN_PROPERTYCHANGED") );
		return 0;

	case PGN_MENUCOMMAND:
		OutputMessage( TEXT("PGN_MENUCOMMAND" ) );
		return 0;
	}
	return 0;
}




void Demo_Basic( HINSTANCE hInstance, HWND hMainWindow )
{
	if( g_hWnd )
		return; // already created.

	WNDCLASSEX winClass;

	winClass.lpszClassName = TEXT("WC_BASICWINDOW");
	winClass.cbSize        = sizeof(WNDCLASSEX);
	winClass.style         = CS_HREDRAW | CS_VREDRAW;
	winClass.lpfnWndProc   = WindowProc;
	winClass.hInstance     = hInstance;
	winClass.hIcon	       = NULL;
	winClass.hIconSm	   = NULL;
	winClass.hCursor       = LoadCursor(NULL, IDC_ARROW);
	winClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	winClass.lpszMenuName  = NULL;
	winClass.cbClsExtra    = 0;
	winClass.cbWndExtra    = 0;

	// Register the class of the main window.
	if( !RegisterClassEx(&winClass) )
		return;

	if( !PropertyGrid_Init( hInstance ) )
	{
		MessageBox( 0, TEXT("Failed to initialize PropertyGrid control class"), TEXT("ERROR"), MB_OK | MB_ICONSTOP );
		return;
	}

	unsigned width = 400, height = 600;
	RECT rWindow = { 0, 0, (long)width, (long)height };
	AdjustWindowRect( &rWindow, WS_OVERLAPPEDWINDOW, FALSE );

	g_hWnd = CreateWindowEx( NULL, winClass.lpszClassName,
						TEXT("Basic demo"),
						WS_OVERLAPPEDWINDOW,
						0, 0, rWindow.right - rWindow.left,
						rWindow.bottom - rWindow.top,
						NULL, NULL, hInstance, NULL );

	if( g_hWnd == NULL )
	{
		MessageBox( hMainWindow, TEXT("Failed to create window"), TEXT("ERROR"), MB_OK | MB_ICONSTOP );
		return;
	}

	g_hEdit = CreateWindowEx( WS_EX_CLIENTEDGE, WC_EDIT, TEXT(""),
						WS_VISIBLE | WS_CHILD | ES_MULTILINE | WS_VSCROLL,
						0, 0, 0, 0, g_hWnd, 0, hInstance, 0 );
	SendMessage( g_hEdit, WM_SETFONT, (WPARAM)CreateGuiFont(), 0 );

	g_hPG = CreateWindowEx( NULL, WC_PROPERTYGRID, TEXT("property grid"), WS_VISIBLE | WS_CHILD,
							0, 0, width, height, g_hWnd, 0, hInstance, 0 );

	PropertyGrid_SetNotificationCallback( g_hPG, OnPropertyGridNotification, 0 );

	SendMessage( g_hWnd, WM_SIZE, 0, 0 );

	// Categories
	HPGITEM hFirstCategory = PropertyGrid_InsertCategory( g_hPG, 0, TEXT("First Category"), 0, 0, 0 );
	PropertyGrid_InsertCategory( g_hPG, 0, TEXT("Second Category"), 0, 0, 0 );

	// Simple edit control
	HPGITEM hEdit = PropertyGrid_InsertEdit( g_hPG, 0, TEXT("Edit"), hFirstCategory, 0, 0, 0 );
	PropertyGrid_SetText( g_hPG, hEdit, TEXT("Hello") );

	// Multi-line edit control
	HPGITEM hMLE = PropertyGrid_InsertEdit( g_hPG, 0, TEXT("Multi-line edit"), hFirstCategory, 0, 0, 0 );
	PGEdit_SetMultiLine( g_hPG, hMLE, true, 8, TRUE );

	// Color button
	PropertyGrid_InsertColor( g_hPG, 0, TEXT("Color"), 0, 0, 0, RGB(120, 0, 0 ) );

	// Font item.
	PropertyGrid_InsertFont( g_hPG, 0, TEXT("Font"), 0, 0, 0 );

	// Checkboxes
	HPGITEM hRot = PropertyGrid_InsertCategory( g_hPG, 0, TEXT("Rotation"), 0, 0, 0 );
	PropertyGrid_InsertCheckbox( g_hPG, 0, 0, hRot, 0, 0, TEXT("x"), TRUE );
	PropertyGrid_InsertCheckbox( g_hPG, 0, 0, hRot, 0, 0, TEXT("y"), TRUE );
	PropertyGrid_InsertCheckbox( g_hPG, 0, 0, hRot, 0, 0, TEXT("z"), TRUE );

	// combo box (list item)
	HPGITEM hCB = PropertyGrid_InsertComboBox( g_hPG, 0, TEXT("ComboBox"), 0, 0, 0 );
	PGCombo_AddItem( g_hPG, hCB, TEXT("Zero"), 0 );
	PGCombo_AddItem( g_hPG, hCB, TEXT("One"), 1 );
	PGCombo_AddItem( g_hPG, hCB, TEXT("Two"), 2 );
	PGCombo_AddItem( g_hPG, hCB, TEXT("Three"), 3 );
	PGCombo_SelectByValue( g_hPG, hCB, 2 );

	// Create and attach a menu to the combo box item.
	HMENU hMenu = CreatePopupMenu();
	AppendMenu( hMenu, 0, 1102, TEXT("This is a sample menu" ) );
	AppendMenu( hMenu, 0, 1100, TEXT("Restore Default" ) );
	AppendMenu( hMenu, 0, 1101, TEXT("Refresh Values") );
	PropertyGrid_SetItemMenu( g_hPG, hCB, hMenu, FALSE );

	// File name
	PropertyGrid_InsertFileName( g_hPG, 0, TEXT("File name"), 0, 0, 0, 0 );

	// Slider
	PropertyGrid_InsertSlider( g_hPG, 0, TEXT("Volume"), 0, 0, 0, 0, 100, 60 );

	// Spinbox number
	HPGITEM hSBN = PropertyGrid_InsertSpinBoxNumber( g_hPG, 0, TEXT("Spinbox"), 0, 0, 0 );
	PGSpinBoxNumber_SetRange( g_hPG, hSBN, 0, INFINITY );
	PGSpinBoxNumber_SetSpinFactor( g_hPG, hSBN, 0.01f );
	PGSpinBoxNumber_SetFormatStrings( g_hPG, hSBN, NULL, TEXT("%.2f centimeters" ) );


	// This demonstrates how to iterate through all items
	/*
	HPGITEM hItem = PropertyGrid_GetFirstItem( g_hPG );
	while( hItem )
	{
		PGITEMINFO info = {0};
		PropertyGrid_GetItemInfo( g_hPG, hItem, &info );
		hItem = PropertyGrid_GetNextItem( hItem );
	}
	*/


	ShowWindow( g_hWnd, SW_SHOW );
	UpdateWindow( g_hWnd );

	SetFocus( g_hPG );

	EnableWindow( hMainWindow, FALSE );


	MSG Msg = {0};
	
	while( GetMessage( &Msg, NULL, 0, 0 ) )
	{
		// IsPropertyGridMessage() allows the property grid control to implement keyboard navigation.
		if( !IsPropertyGridMessage( g_hPG, &Msg ) )
		{
			TranslateMessage( &Msg );
			DispatchMessage( &Msg );
		}
	}

	PropertyGrid_Uninit();

	UnregisterClass( winClass.lpszClassName, winClass.hInstance );

	EnableWindow( hMainWindow, TRUE );
	SetFocus( hMainWindow );
	g_hWnd = 0;
}




// The message procedure for the main window.
LRESULT CALLBACK WindowProc( HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam )
{
	switch( Msg )
	{
	case WM_CLOSE:
		PostQuitMessage( 0 );
		break;

	case WM_KEYDOWN:
		if( wParam == VK_ESCAPE )
			PostQuitMessage( 0 );
		break;

	case WM_SIZE:
		if( g_hPG )
		{
			int logHeight = 100;
			RECT rClient = GetClientRect( hWnd );
			MoveWindow( g_hPG, 0, 0, rClient.right, rClient.bottom - logHeight, true );
			MoveWindow( g_hEdit, 0, rClient.bottom - logHeight, rClient.right, logHeight, true );
		}
		break;
	}
	return DefWindowProc( hWnd, Msg, wParam, lParam );
}