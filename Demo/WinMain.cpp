// Win32 Property Grid demo.
// http://code-section.com


# define STRICT
# define WIN32_LEAN_AND_MEAN
# include <windows.h>
# include "resource.h"


HINSTANCE g_hInstance = 0;



void Demo_Basic( HINSTANCE hInstance, HWND hMainWindow );
void Demo_CustomItem( HINSTANCE hInstance, HWND hMainWindow );




INT_PTR CALLBACK DialogProc( HWND hDlg, UINT msg, WPARAM wParam, LPARAM lParam )
{
	if( msg == WM_COMMAND )
	{
		switch( LOWORD(wParam) )
		{
		case IDC_BASIC:
			Demo_Basic( g_hInstance, hDlg );
			return TRUE;

		case IDC_CUSTOM_DRAWING:
			Demo_CustomItem( g_hInstance, hDlg );
			return TRUE;

		case IDOK:
		case IDCANCEL:
			EndDialog( hDlg, LOWORD(wParam) );
			PostQuitMessage( 0 );
			return TRUE;
		}
	}
	return FALSE;
}




INT WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, INT nShowCmd )
{
	g_hInstance = hInstance;
	DialogBox( hInstance, MAKEINTRESOURCE(IDD_MAIN), NULL, DialogProc );
}
