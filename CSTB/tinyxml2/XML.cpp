

# include "XML.h"
# include <CSTB\Win32\Win32Util.h>
#pragma warning( push )
#pragma warning( disable: 4995 )
# include <TinyXml2\TinyXml2.h>
#pragma warning( pop )


using namespace tinyxml2;


namespace XML
{



XMLElement*	CONV(HELEMENT e)		{ return e.GetXmlElement(); }



Document::Document()
{
	pDoc = new tinyxml2::XMLDocument;
}


Document::~Document() { delete pDoc; }




bool Document::Parse( const char* sXML )
{
	pDoc->SetError( XML_SUCCESS, NULL, NULL );
	return XML_SUCCESS == pDoc->Parse( sXML );
}


bool Document::Load( LPCSTR sFileName )
{
	pDoc->SetValue( sFileName );
	return XML_SUCCESS == pDoc->LoadFile( sFileName );
}

bool Document::Load( LPCWSTR sFileName )
{
	TempStrConv conv;
	return Load( conv.Convert( sFileName, CP_UTF8 ) );
}


bool Document::Save( LPCSTR sFileName )
{
	// Clear the error flag, otherwise XMLDocument::SaveFile() will always return an error.
	pDoc->SetError( XML_SUCCESS, NULL, NULL );
	pDoc->SetValue( sFileName );
	return XML_SUCCESS == pDoc->SaveFile( sFileName );
}


bool Document::Save( LPCWSTR sFileName )
{
	TempStrConv conv;
	return Save( conv.Convert( sFileName, CP_UTF8 ) );
}


bool Document::Save()
{
	// NOTE: XMLNode::SetValue( XMLNode::Value() ) results in an error as SetValue() will delete
	// the string returned by the Value() method. Consequently, the following line results in
	// an error as the Save() function sets the document node's value to be the file name.
	//return Save( pDoc->Value() );
	char sTemp[ MAX_PATH ];
	strcpy_s( sTemp, MAX_PATH, pDoc->Value() );
	return Save( sTemp );
}


int Document::ErrorID() const { return this->GetXmlDocument()->ErrorID(); }



HELEMENT Document::CreateElement( const char* name, HELEMENT hParent )
{
	if( hParent )
		return hParent.CreateChildElement( name );
	XMLElement* ele = pDoc->NewElement( name );
	pDoc->InsertEndChild( ele );
	return HELEMENT( ele );
}


HELEMENT Document::FindElement( const char* name, HELEMENT hParent )
{
	XMLNode* pParent = pDoc;
	if( hParent )
		pParent = CONV(hParent);
	return HELEMENT(pParent->FirstChildElement( name ));
}



void Document::DeleteElement( HELEMENT e )
{
	if( !e ) return;
	CONV(e)->Parent()->DeleteChild(CONV(e));
}



HELEMENT Document::GetFirst()
{ return HELEMENT(pDoc->FirstChildElement()); }


HELEMENT Document::GetParent( HELEMENT e )
{ return HELEMENT(CONV(e)->Parent()->ToElement()); }


HELEMENT Document::GetFirstChild( HELEMENT e )
{ return HELEMENT(CONV(e)->FirstChildElement()); }


HELEMENT Document::NextSibling( HELEMENT e )
{ return HELEMENT(CONV(e)->NextSiblingElement()); }



HELEMENT Document::PrevSibling( HELEMENT e )
{ return HELEMENT( CONV(e)->PreviousSiblingElement()); }




HELEMENT::HELEMENT( XMLElement* pElem ) { m_pElem = pElem; }


void HELEMENT::SetAttribute( const char* name, int value )
{ if( !m_pElem ) return; m_pElem->SetAttribute( name, value ); }

void HELEMENT::SetAttribute( const char* name, double value )
{ if( !m_pElem ) return; m_pElem->SetAttribute( name, value ); }

void HELEMENT::SetAttribute( const char* name, const char* value )
{ if( !m_pElem ) return; m_pElem->SetAttribute( name, value ); }

void HELEMENT::SetAttribute( const char* name, const WCHAR* value )
{
	TempStrConv conv;
	SetAttribute( name, conv.Convert( value, CP_UTF8 ) );
}

void HELEMENT::SetValue( const char* value )
{ if( !m_pElem ) return; m_pElem->SetValue( value ); }


void HELEMENT::SetValue( const WCHAR* value )
{ if( !m_pElem ) return; TempStrConv conv; m_pElem->SetValue( conv.Convert( value, CP_UTF8 ) ); }


bool HELEMENT::GetAttribute( const char* name, int& value ) const
{ if( !m_pElem ) return false; return XML_NO_ERROR == m_pElem->QueryIntAttribute( name, &value ); }

bool HELEMENT::GetAttribute( const char* name, bool& value ) const
{ if( !m_pElem ) return false; return XML_NO_ERROR == m_pElem->QueryBoolAttribute( name, &value ); }

bool HELEMENT::GetAttribute( const char* name, double& value ) const
{ if( !m_pElem ) return false; return XML_NO_ERROR == m_pElem->QueryDoubleAttribute( name, &value ); }

bool HELEMENT::GetAttribute( const char* name, float& value ) const
{ if( !m_pElem ) return false; return XML_NO_ERROR == m_pElem->QueryFloatAttribute( name, &value ); }


bool HELEMENT::GetAttribute( const char* name, WCHAR* ws, INT bufferLen )
{
	const char* s = GetAttribute( name );
	return 0 != MultiByteToWideChar( CP_UTF8, 0, s, -1, ws, bufferLen );
}

const char* HELEMENT::GetAttribute(const char *name) const
{ if( !m_pElem ) return 0; return m_pElem->Attribute( name ); }


const char* HELEMENT::GetValue() const
{ if( !m_pElem ) return false; return m_pElem->Value(); }

const char* HELEMENT::GetText() const
{ if( !m_pElem ) return false; return m_pElem->GetText(); }


void HELEMENT::DeleteAttribute( const char* name )
{ if( !m_pElem ) return; m_pElem->DeleteAttribute( name ); }


unsigned HELEMENT::CountChildren() const
{
	unsigned c=0;
	for( HELEMENT hChild = FirstChildElement();
		hChild != NULL;
		hChild = Document::NextSibling( hChild ) )
		c++;
	return c;
}

HELEMENT HELEMENT::FirstChildElement( const char* name ) const
{ if( !m_pElem ) return HELEMENT(0); return HELEMENT( m_pElem->FirstChildElement( name ) ); }


HELEMENT HELEMENT::CreateChildElement( const char* name )
{
	if( !m_pElem )
		return HELEMENT(0);
	XMLElement* pChild = m_pElem->InsertEndChild( m_pElem->GetDocument()->NewElement(name) )->ToElement();
	if( !pChild )
		DebugBreak();
	return HELEMENT(pChild);
}


}; // namespace XML