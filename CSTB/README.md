# CSTB: code-section's toolbox
## Utility C++ code for win32 and more
Created by Adel Amro - [http://code-section.com](http://code-section.com)

code-section's toolbox is a small collection of classes and functions which was created and will be updated while working on other projects. A lot of them are for win32 programming. Some components are stand-alone while other components are used to work with some 3rd party libraries.

The toolbox is free for commercial and non-commercial use, with no guarantees or warranties of any kind.

The library consists of the following components:


*   **Event**: A simple templated Event object. More about it [here](http://www.code-section.com/entry/2/c-a-simple-event-system).
*   **Intrusive List and Tree Containers**
*   **Log Window**: A simple log window class. It creates a window which contains an edit control for easy text output in win32 applications. Example:
```sh
CSTB::LogWindow Log;
	Log.Create( "FileName.txt", parentWindow );
	Log( "simple string output" );
	Log( "x is %d", x );
	Log( TEXT("s: %s"), sFileName );

	Log( "Array elements: " );
	Log.Indent();
	for( unsigned i=0; i<theArray.length(); i++ )
		Log( "el[%d]: %x", i, theArray[i] );
	Log.Unindent();
	// TODO: return *this from most methods to enable method chaining.
```
*   **Win32Util.h/cpp**: This file contains lots of small useful stuff for win32 programming.
*   **Window.h/cpp**: A window class that wraps win32 windows. It can be used to create new window types (custom controls), or to subclass existing controls.
*   **XML.h/cpp**: Wrapper around [tinyxml2](http://www.grinninglizard.com/tinyxml2/).
> TODO: Add example code
*   **SciterWindow.h/cpp**: Derived from Window and [sciter](http://www.terrainformatica.com/sciter/)'s event_handler to facilitate working with sciter.
