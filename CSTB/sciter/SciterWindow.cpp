
# include "SciterWindow.h"
# include <CSTB/Win32/Win32Util.h>
# include <sciter-x-behavior.h>
# include <strsafe.h>
# include <map>
# include <list>



/*
# ifdef WIN64
# pragma comment( lib, "sciter64.lib" )
#else
# pragma comment( lib, "sciter32.lib" )
#endif
*/

namespace CSTB
{


const unsigned IDM_RELOAD = 38383;



typedef std::map< std::string, SciterWindow::PFNativeFunction > FunctionMap;
FunctionMap g_nativeFunctions;


void SciterWindow::RegisterNativeFunction( PFNativeFunction pf, const char* name )
{ g_nativeFunctions[ name ] = pf; }

void SciterWindow::RemoveNativeFunction( const char* name )
{ g_nativeFunctions.erase( name ); }


SciterWindow::PFNativeFunction SciterWindow::GetNativeFunction( const char* name )
{
	FunctionMap::iterator it = g_nativeFunctions.find( name );
	if( it == g_nativeFunctions.end() )
		return NULL;
	return it->second;
}




typedef std::map< std::string, std::list< std::tuple< sciter::value, HWND > > > ScriptCBMap;
//typedef std::map< std::string, std::list< sciter::value > > ScriptCBMap;
ScriptCBMap g_scriptCallbacks;




/// Subscribe a script function to a native event.
/// Expected invocation: view.Subscribe( "eventName", function(...) { } );
bool SubscribeEvent( HWND hWnd, HELEMENT he, const char* name, UINT argc, SCITER_VALUE* argv, SCITER_VALUE& retval )
{
	if( argc != 2 )
	{
		retval = false;
		return true;
	}
	sciter::value& function = argv[1];

	//if( !function.is_function() )
	{
		//retval = false;
		//return true;
	}

	TempStrConv cnv;
	std::string eventName = cnv.Convert( argv[0].to_string().c_str(), CP_UTF8 );
	if( eventName.empty() )
	{
		retval = false;
		return true;
	}

	g_scriptCallbacks[ eventName ].push_back( std::make_tuple(function,hWnd) );
	retval = true;
	return true;
}




/// Not tested yet.
bool UnsubscribeEvent( HWND hWnd, HELEMENT he, const char* name, UINT argc, SCITER_VALUE* argv, SCITER_VALUE& ret )
{
	ret = false;
	if( argc < 1 )
		return true;

	TempStrConv cnv;
	std::string eventName = cnv.Convert( argv[0].to_string().c_str(), CP_UTF8 );
	if( eventName.empty() )
		return true;

	if( g_scriptCallbacks.find( eventName ) == g_scriptCallbacks.end() )
		return true;

	if( argc > 1 )
	{
		sciter::value& function = argv[1];
		g_scriptCallbacks[ eventName ].remove( std::make_tuple(function,hWnd) );
	}
	else
		g_scriptCallbacks[ eventName ].clear();

	ret = true;
	return true;
}




// This will be called from script as follows:
// view.Trigger( "eventName", ... );
bool TriggerEventFromScript( HWND hWnd, HELEMENT he, const char* name, UINT argc, SCITER_VALUE* argv, SCITER_VALUE& ret )
{
	ret = false;
	if( argc < 1 || !argv[0].is_string())
	{
		DebugPrintf( "Invalid arguments. Expecting view.Trigger( 'eventName', ... )" );
		return false;
	}
	TempStrConv cnv;
	std::string eventName = cnv.Convert(argv[0].to_string().c_str(), CP_UTF8);
	if (eventName.empty())
		return true;

	SciterWindow::TriggerEvent( eventName.c_str(), argc-1, argc > 1 ? &argv[1] : 0 );
	return true;
}




bool SciterWindow::TriggerEvent( const char* name, int argc, const sciter::value* argv, sciter::value* pRet )
{
	sciter::value ret;
	if( name == 0 )
		return false;
	ScriptCBMap::iterator it = g_scriptCallbacks.find( name );
	if( it == g_scriptCallbacks.end() )
		return false;
	
	if( it->second.empty() )
		return false;

	for( auto itcb = it->second.begin();
		itcb != it->second.end();
		itcb++ )
	{
		ret = itcb->_Myfirst._Val.call( argc, argv );
	}
	if( pRet )
		*pRet = ret;
	return true;
}




BOOL SciterWindow::Create( DWORD dwStyleEx, LPCTSTR sTitle, DWORD dwStyle,
		int x, int y, int width, int height, HWND hParentWnd,
		HMENU hMenu, HINSTANCE hInstance, void* lParam )
{
	RegisterNativeFunction( SubscribeEvent, "Subscribe" );
	RegisterNativeFunction( UnsubscribeEvent, "Unsubscribe" );
	RegisterNativeFunction( TriggerEventFromScript, "Trigger" );

	if( !Window::Create( dwStyleEx, CSTB_WINDOWCLASS, sTitle, dwStyle, x, y, width, height,
		hParentWnd, hMenu, hInstance, lParam ) )
		return FALSE;

	SciterSetCallback( hWnd, SciterWindow::NotifyHandler, (LPVOID)this );

	sciter::attach_dom_event_handler( hWnd, this );

	// Set a global variable with sciter so we can distinguish in tiscript between
	// content loaded by curver and content loaded elsewhere (e.g. sciter.exe).
	// This is done by using media variables.

	sciter::value globals;
	globals["CURVER"] = true;

#if defined( DEBUG ) || defined( _DEBUG ) || defined( __DEBUG )
	globals["DEBUG"] = true;
	SciterSetOption(hWnd, SCITER_SET_DEBUG_MODE, TRUE);
#else
	globals["RELEASE"] = true;
# endif

	SciterSetMediaVars(hWnd, &globals);

	// In CSS, this can be used to create app-specific css rules:
	// @media CURVER {
	// div.red{ color: red; } <<-- app-specific
	// }

	// In tiscript, it can be used as:
	// const isCurver = view.mediaVars()["CURVER"];

	// Add a "Reload" menu entry to the system menu.
	HMENU hSysMenu = GetSystemMenu( hWnd, FALSE );
	AppendMenu( hSysMenu, MF_STRING, IDM_RELOAD, TEXT("Reload") );

	return TRUE;
}




BOOL SciterWindow::LoadFile( LPCTSTR sPath, BOOL bMakeAbsPath )
{
	if( !hWnd || !sPath )
		return FALSE;


	TCHAR sAbsPath[ MAX_PATH ];
	TCHAR sFullPath[ MAX_PATH ];
	sAbsPath[0] = 0;
	sFullPath[0] = 0;

	if( bMakeAbsPath )
	{
		LPTSTR sFilePart = NULL;
		DWORD len = GetFullPathName( sPath, MAX_PATH, sAbsPath, &sFilePart );
		::StringCchPrintf( sFullPath, MAX_PATH, TEXT("file:///%s"), sAbsPath );
		sPath = sFullPath;
	}

	m_sFileName = sPath;

	return SciterLoadFile( hWnd, sPath );
}



LRESULT SciterWindow::MsgProc( UINT Msg, WPARAM wParam, LPARAM lParam )
{
	BOOL bHandled = FALSE;
	LRESULT ret = 0;
	ret = SciterProcND( hWnd, Msg, wParam, lParam, &bHandled );
	if( bHandled )
		return ret;

	if( (Msg == WM_SYSCOMMAND && wParam == IDM_RELOAD) ||
		(Msg == WM_KEYDOWN && wParam == VK_F5) )
	{
		if( !Reload() )
			OutputDebugString( TEXT("Failed to reload\n") );
	}

	// This removes all script callbacks associated with this window. However, it's commented out for Curver
	// because currently there is no need for it as no events are triggered after windows are destroyed,
	// and no windows are destroyed during program lifetime. This may change in the future.
	/*
	if( Msg == WM_DESTROY )
	{
		for( auto it = g_scriptCallbacks.begin(); it != g_scriptCallbacks.end(); it++ )
		{
			auto& cblist = it->second;
			auto cbit = cblist.begin();
			while( cbit != cblist.end() )
			{
				auto curIt = cbit++;
				auto tuple = *curIt;
				HWND cbWnd = tuple._Get_rest()._Myfirst._Val; // TODO: This can't be the proper way to get the HWND of the tuple
				if( cbWnd == hWnd )
				{
					cblist.erase( curIt );
					//DebugPrintf( "Removing event callback from window: %S", GetWindowText( hWnd ) );
				}
			}
		}
	}
	*/

	return Window::MsgProc( Msg, wParam, lParam );
}




LRESULT SciterWindow::OnSciterNotify( LPSCITER_CALLBACK_NOTIFICATION pn )
{
	if( pn->code == SC_ATTACH_BEHAVIOR )	return OnAttachBehavior( (LPSCN_ATTACH_BEHAVIOR)pn );
	return 0;
}




// Loop through the global list of behavior factories and find the
// behavior matching the name, and attach it to the element.
LRESULT SciterWindow::OnAttachBehavior( LPSCN_ATTACH_BEHAVIOR pn )
{
	sciter::event_handler *pb = sciter::behavior_factory::create(pn->behaviorName, pn->element);
	if(pb)
	{
		pn->elementTag  = pb;
		pn->elementProc = sciter::event_handler::element_proc;
		return TRUE;
	}
	return FALSE;
}

}; // namespace CSTB