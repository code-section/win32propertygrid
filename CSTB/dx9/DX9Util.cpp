// Direct3D 9 utility stuff.
// Adel Amro.


# include "DX9Util.h"
# include <TCHAR.H>
# include <string>
# include <map>


namespace DX9
{


// Dynamically load Direct3D9.
BOOL CreateD3DInterface( LPDIRECT3D9* ppD3D, HINSTANCE* phLib )
{
	LPDIRECT3D9 pD3D = NULL;
	LPDIRECT3D9 (WINAPI * pD3DCreator )( UINT ) = NULL;
	if( NULL == (*phLib = LoadLibrary( TEXT("d3d9.dll") ) ) ||
		NULL == (pD3DCreator = (LPDIRECT3D9 (WINAPI *)(UINT))GetProcAddress( *phLib, "Direct3DCreate9" ) ) ||
		NULL == (*ppD3D = pD3DCreator( D3D_SDK_VERSION ) ) )
		return FALSE;

	return TRUE;
}




// Returns the number of color channel bits in the specified D3DFORMAT
UINT ColorChannelBits( D3DFORMAT fmt )
{
	switch(	fmt	)
	{
		case D3DFMT_R8G8B8:			return 8;
		case D3DFMT_A8R8G8B8:		return 8;
		case D3DFMT_X8R8G8B8:		return 8;
		case D3DFMT_R5G6B5:			return 5;
		case D3DFMT_X1R5G5B5:		return 5;
		case D3DFMT_A1R5G5B5:		return 5;
		case D3DFMT_A4R4G4B4:		return 4;
		case D3DFMT_R3G3B2:			return 2;
		case D3DFMT_A8R3G3B2:		return 2;
		case D3DFMT_X4R4G4B4:		return 4;
		case D3DFMT_A2B10G10R10:	return 10;
		case D3DFMT_A2R10G10B10:	return 10;
		default:					return 0;
	}
}




// Returns the number of alpha channel bits in the specified D3DFORMAT
UINT AlphaChannelBits( D3DFORMAT fmt )
{
	switch(	fmt	)
	{
		case D3DFMT_R8G8B8:			return 0;
		case D3DFMT_A8R8G8B8:		return 8;
		case D3DFMT_X8R8G8B8:		return 0;
		case D3DFMT_R5G6B5:			return 0;
		case D3DFMT_X1R5G5B5:		return 0;
		case D3DFMT_A1R5G5B5:		return 1;
		case D3DFMT_A4R4G4B4:		return 4;
		case D3DFMT_R3G3B2:			return 0;
		case D3DFMT_A8R3G3B2:		return 8;
		case D3DFMT_X4R4G4B4:		return 0;
		case D3DFMT_A2B10G10R10:	return 2;
		case D3DFMT_A2R10G10B10:	return 2;
		default:					return 0;
	}
}




// Returns the number of depth bits in the specified D3DFORMAT
UINT DepthBits(	D3DFORMAT fmt )
{
	switch(	fmt	)
	{
		case D3DFMT_D16:		return 16;
		case D3DFMT_D15S1:		return 15;
		case D3DFMT_D24X8:		return 24;
		case D3DFMT_D24S8:		return 24;
		case D3DFMT_D24X4S4:	return 24;
		case D3DFMT_D32:		return 32;
		default:				return 0;
	}
}




// Returns the number of stencil bits in the specified D3DFORMAT
UINT StencilBits( D3DFORMAT fmt )
{
	switch(	fmt	)
	{
		case D3DFMT_D16:		return 0;
		case D3DFMT_D15S1:		return 1;
		case D3DFMT_D24X8:		return 0;
		case D3DFMT_D24S8:		return 8;
		case D3DFMT_D24X4S4:	return 4;
		case D3DFMT_D32:		return 0;
		default:				return 0;
	}
}



// Returns the name of the given D3DFORMAT.
TCHAR* D3DFormatToString( D3DFORMAT format, bool bWithPrefix )
{
	TCHAR* pstr = NULL;
	switch( format )
	{
		case D3DFMT_UNKNOWN:         pstr = TEXT("D3DFMT_UNKNOWN");			break;
		case D3DFMT_R8G8B8:          pstr = TEXT("D3DFMT_R8G8B8");			break;
		case D3DFMT_A8R8G8B8:        pstr = TEXT("D3DFMT_A8R8G8B8");		break;
		case D3DFMT_X8R8G8B8:        pstr = TEXT("D3DFMT_X8R8G8B8");		break;
		case D3DFMT_R5G6B5:          pstr = TEXT("D3DFMT_R5G6B5");			break;
		case D3DFMT_X1R5G5B5:        pstr = TEXT("D3DFMT_X1R5G5B5");		break;
		case D3DFMT_A1R5G5B5:        pstr = TEXT("D3DFMT_A1R5G5B5");		break;
		case D3DFMT_A4R4G4B4:        pstr = TEXT("D3DFMT_A4R4G4B4");		break;
		case D3DFMT_R3G3B2:          pstr = TEXT("D3DFMT_R3G3B2");			break;
		case D3DFMT_A8:              pstr = TEXT("D3DFMT_A8");				break;
		case D3DFMT_A8R3G3B2:        pstr = TEXT("D3DFMT_A8R3G3B2");		break;
		case D3DFMT_X4R4G4B4:        pstr = TEXT("D3DFMT_X4R4G4B4");		break;
		case D3DFMT_A2B10G10R10:     pstr = TEXT("D3DFMT_A2B10G10R10");		break;
		case D3DFMT_A8B8G8R8:        pstr = TEXT("D3DFMT_A8B8G8R8");		break;
		case D3DFMT_X8B8G8R8:        pstr = TEXT("D3DFMT_X8B8G8R8");		break;
		case D3DFMT_G16R16:          pstr = TEXT("D3DFMT_G16R16");			break;
		case D3DFMT_A2R10G10B10:     pstr = TEXT("D3DFMT_A2R10G10B10");		break;
		case D3DFMT_A16B16G16R16:    pstr = TEXT("D3DFMT_A16B16G16R16");	break;
		case D3DFMT_A8P8:            pstr = TEXT("D3DFMT_A8P8");			break;
		case D3DFMT_P8:              pstr = TEXT("D3DFMT_P8");				break;
		case D3DFMT_L8:              pstr = TEXT("D3DFMT_L8");				break;
		case D3DFMT_A8L8:            pstr = TEXT("D3DFMT_A8L8");			break;
		case D3DFMT_A4L4:            pstr = TEXT("D3DFMT_A4L4");			break;
		case D3DFMT_V8U8:            pstr = TEXT("D3DFMT_V8U8");			break;
		case D3DFMT_L6V5U5:          pstr = TEXT("D3DFMT_L6V5U5");			break;
		case D3DFMT_X8L8V8U8:        pstr = TEXT("D3DFMT_X8L8V8U8");		break;
		case D3DFMT_Q8W8V8U8:        pstr = TEXT("D3DFMT_Q8W8V8U8");		break;
		case D3DFMT_V16U16:          pstr = TEXT("D3DFMT_V16U16");			break;
		case D3DFMT_A2W10V10U10:     pstr = TEXT("D3DFMT_A2W10V10U10");		break;
		case D3DFMT_UYVY:            pstr = TEXT("D3DFMT_UYVY");			break;
		case D3DFMT_YUY2:            pstr = TEXT("D3DFMT_YUY2");			break;
		case D3DFMT_DXT1:            pstr = TEXT("D3DFMT_DXT1");			break;
		case D3DFMT_DXT2:            pstr = TEXT("D3DFMT_DXT2");			break;
		case D3DFMT_DXT3:            pstr = TEXT("D3DFMT_DXT3");			break;
		case D3DFMT_DXT4:            pstr = TEXT("D3DFMT_DXT4");			break;
		case D3DFMT_DXT5:            pstr = TEXT("D3DFMT_DXT5");			break;
		case D3DFMT_D16_LOCKABLE:    pstr = TEXT("D3DFMT_D16_LOCKABLE");	break;
		case D3DFMT_D32:             pstr = TEXT("D3DFMT_D32");				break;
		case D3DFMT_D15S1:           pstr = TEXT("D3DFMT_D15S1");			break;
		case D3DFMT_D24S8:           pstr = TEXT("D3DFMT_D24S8");			break;
		case D3DFMT_D24X8:           pstr = TEXT("D3DFMT_D24X8");			break;
		case D3DFMT_D24X4S4:         pstr = TEXT("D3DFMT_D24X4S4");			break;
		case D3DFMT_D16:             pstr = TEXT("D3DFMT_D16");				break;
		case D3DFMT_L16:             pstr = TEXT("D3DFMT_L16");				break;
		case D3DFMT_VERTEXDATA:      pstr = TEXT("D3DFMT_VERTEXDATA");		break;
		case D3DFMT_INDEX16:         pstr = TEXT("D3DFMT_INDEX16");			break;
		case D3DFMT_INDEX32:         pstr = TEXT("D3DFMT_INDEX32");			break;
		case D3DFMT_Q16W16V16U16:    pstr = TEXT("D3DFMT_Q16W16V16U16");	break;
		case D3DFMT_MULTI2_ARGB8:    pstr = TEXT("D3DFMT_MULTI2_ARGB8");	break;
		case D3DFMT_R16F:            pstr = TEXT("D3DFMT_R16F");			break;
		case D3DFMT_G16R16F:         pstr = TEXT("D3DFMT_G16R16F");			break;
		case D3DFMT_A16B16G16R16F:   pstr = TEXT("D3DFMT_A16B16G16R16F");	break;
		case D3DFMT_R32F:            pstr = TEXT("D3DFMT_R32F");			break;
		case D3DFMT_G32R32F:         pstr = TEXT("D3DFMT_G32R32F");			break;
		case D3DFMT_A32B32G32R32F:   pstr = TEXT("D3DFMT_A32B32G32R32F");	break;
		case D3DFMT_CxV8U8:          pstr = TEXT("D3DFMT_CxV8U8");			break;
		default:                     pstr = TEXT("Unknown format");			break;
	}
	if( bWithPrefix || _tcsstr( pstr, TEXT("D3DFMT_") )== NULL )
		return pstr;
	else
		return pstr + lstrlen( TEXT("D3DFMT_") );
}



// Returns the name of the given D3DPRESENT_INTERVAL (which is a UINT).
TCHAR* D3DPresentIntervalToString( UINT pi, bool bWithPrefix )
{
	TCHAR* pstr = NULL;
	switch( pi )
	{
		case D3DPRESENT_INTERVAL_IMMEDIATE:	pstr = TEXT("D3DPRESENT_INTERVAL_IMMEDIATE");	break;
		case D3DPRESENT_INTERVAL_DEFAULT:	pstr = TEXT("D3DPRESENT_INTERVAL_DEFAULT");		break;
		case D3DPRESENT_INTERVAL_ONE:		pstr = TEXT("D3DPRESENT_INTERVAL_ONE");			break;
		case D3DPRESENT_INTERVAL_TWO:		pstr = TEXT("D3DPRESENT_INTERVAL_TWO");			break;
		case D3DPRESENT_INTERVAL_THREE:		pstr = TEXT("D3DPRESENT_INTERVAL_THREE");		break;
		case D3DPRESENT_INTERVAL_FOUR:		pstr = TEXT("D3DPRESENT_INTERVAL_FOUR");		break;
		default:							pstr = TEXT("Unknown presentation interval");	break;
	}
	if( bWithPrefix || _tcsstr( pstr, TEXT("D3DPRESENT_INTERVAL_") )== NULL )
		return pstr;
	else
		return pstr + lstrlen( TEXT("D3DPRESENT_INTERVAL_") );
}



TCHAR* D3DXParameterTypeToString( D3DXPARAMETER_TYPE type, bool bWithPrefix )
{
	TCHAR* pstr = NULL;
	switch( type )
	{
		case D3DXPT_VOID :			pstr = TEXT( "D3DXPT_VOID" );			break;
		case D3DXPT_BOOL :			pstr = TEXT( "D3DXPT_BOOL" );			break;
		case D3DXPT_INT:			pstr = TEXT( "D3DXPT_INT" );			break;
		case D3DXPT_FLOAT:			pstr = TEXT( "D3DXPT_FLOAT" );			break;
		case D3DXPT_STRING:			pstr = TEXT( "D3DXPT_STRING" );			break;
		case D3DXPT_TEXTURE:		pstr = TEXT( "D3DXPT_TEXTURE" );		break;
		case D3DXPT_TEXTURE1D:		pstr = TEXT( "D3DXPT_TEXTURE1D" );		break;
		case D3DXPT_TEXTURE2D:		pstr = TEXT( "D3DXPT_TEXTURE2D" );		break;
		case D3DXPT_TEXTURE3D:		pstr = TEXT( "D3DXPT_TEXTURE3D" );		break;
		case D3DXPT_TEXTURECUBE:	pstr = TEXT( "D3DXPT_TEXTURECUBE" );	break;
		case D3DXPT_SAMPLER:		pstr = TEXT( "D3DXPT_SAMPLER" );		break;
		case D3DXPT_SAMPLER1D:		pstr = TEXT( "D3DXPT_SAMPLER1D" );		break;
		case D3DXPT_SAMPLER2D:		pstr = TEXT( "D3DXPT_SAMPLER2D" );		break;
		case D3DXPT_SAMPLER3D:		pstr = TEXT( "D3DXPT_SAMPLER3D" );		break;
		case D3DXPT_SAMPLERCUBE:	pstr = TEXT( "D3DXPT_SAMPLERCUBE" );	break;
		case D3DXPT_PIXELSHADER:	pstr = TEXT( "D3DXPT_PIXELSHADER" );	break;
		case D3DXPT_VERTEXSHADER:	pstr = TEXT( "D3DXPT_VERTEXSHADER" );	break;
		case D3DXPT_PIXELFRAGMENT:	pstr = TEXT( "D3DXPT_PIXELFRAGMENT" );	break;
		case D3DXPT_VERTEXFRAGMENT:	pstr = TEXT( "D3DXPT_VERTEXFRAGMENT" );	break;
		//case D3DXPT_FORCEDWORD:		pstr = TEXT( "D3DXPT_FORCEDWORD" );		break;
		default:					pstr = TEXT( "UNKNOWN" );
	}
	if( bWithPrefix || _tcsstr( pstr, TEXT( "D3DXPT_" ) ) == NULL )
		return pstr;
	return pstr + lstrlen( TEXT( "D3DXPT_" ) );
}



TCHAR* D3DXParameterClassToString( D3DXPARAMETER_CLASS pc, bool bWithPrefix )
{
	TCHAR* pstr = NULL;
	switch( pc )
	{
		case D3DXPC_SCALAR:			pstr = TEXT( "D3DXPC_SCALAR" );				break;
		case D3DXPC_VECTOR:			pstr = TEXT( "D3DXPC_VECTOR" );				break;
		case D3DXPC_MATRIX_ROWS:	pstr = TEXT( "D3DXPC_MATRIX_ROWS" );		break;
		case D3DXPC_MATRIX_COLUMNS:	pstr = TEXT( "D3DXPC_MATRIX_COLUMNS" );		break;
		case D3DXPC_OBJECT:			pstr = TEXT( "D3DXPC_OBJECT" );				break;
		case D3DXPC_STRUCT:			pstr = TEXT( "D3DXPC_STRUCT" );				break;
		//case D3DXPC_FORCEDWORD:		pstr = TEXT( "D3DXPC_FORCEDWORD" );			break;
		default:					pstr = TEXT( "D3DXPC_UNKNOWN" );
	}
	if( bWithPrefix || _tcsstr( pstr, TEXT( "D3DXPC_" ) ) == NULL )
		return pstr;
	return pstr + lstrlen( TEXT( "D3DXPC_" ) );
}






VOID DrawAxii( LPDIRECT3DDEVICE9 pD3DDevice, FLOAT length, const D3DXMATRIX* pTransform )
{
	struct TVertex
	{
		D3DXVECTOR3 pos;
		DWORD color;
	};
	const DWORD FVF = D3DFVF_XYZ | D3DFVF_DIFFUSE;
	TVertex p[6];
	p[0].pos = D3DXVECTOR3( -length, 0, 0 );
	p[1].pos = D3DXVECTOR3( length, 0, 0 );
	p[2].pos = D3DXVECTOR3( 0, -length, 0 );
	p[3].pos = D3DXVECTOR3( 0, length, 0 );
	p[4].pos = D3DXVECTOR3( 0, 0, -length );
	p[5].pos = D3DXVECTOR3( 0, 0, length );

	p[0].color = 0x00000000;
	p[1].color = 0xFFFF0000;
	p[2].color = 0x00000000;
	p[3].color = 0xFF00FF00;
	p[4].color = 0x00000000;
	p[5].color = 0xFF0000FF;

	D3DXMATRIX mIdentity;
	if( !pTransform )
	{
		D3DXMatrixIdentity( &mIdentity );
		pTransform = &mIdentity;
	}

	pD3DDevice->SetTexture( 0, NULL );
	pD3DDevice->SetTransform( D3DTS_WORLD, pTransform );
	pD3DDevice->SetFVF( FVF );
	pD3DDevice->SetRenderState( D3DRS_LIGHTING, FALSE );
	pD3DDevice->DrawPrimitiveUP( D3DPT_LINELIST, 3, p, sizeof(TVertex) );
}




HRESULT DrawTransformedQuad( LPDIRECT3DDEVICE9 pDevice,
						 FLOAT x, FLOAT y, FLOAT z,
						 FLOAT width, FLOAT height,
						 D3DXVECTOR2 uvTopLeft, D3DXVECTOR2 uvTopRight,
						 D3DXVECTOR2 uvBottomLeft, D3DXVECTOR2 uvBottomRight,
						 D3DCOLOR c1, D3DCOLOR c2, D3DCOLOR c3, D3DCOLOR c4 )
{
	struct
	{
		float pos[4];
		D3DCOLOR color;
		float uv[2];
	} quad[] =
	{
		x,			y,			z, 1.f,		c1, uvTopLeft.x,		uvTopLeft.y,
		x + width,	y,			z, 1.f,		c2, uvBottomRight.x,	uvTopLeft.y,
		x,			y + height, z, 1.f,		c3, uvTopLeft.x,		uvBottomRight.y,
		x + width,	y + height, z, 1.f,		c4, uvBottomRight.x,	uvBottomRight.y,
	};
	pDevice->SetFVF( D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE2(0) );
	return pDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, quad, sizeof(quad[0]) );
}






VOID DrawQuad( LPDIRECT3DDEVICE9 pDevice, FLOAT x, FLOAT y, FLOAT z, FLOAT width, FLOAT height,
			  D3DCOLOR c1, D3DCOLOR c2, D3DCOLOR c3, D3DCOLOR c4 )
{
	struct
	{
		float pos[3];
		D3DCOLOR color;
		float uv[2];
	} quad[] =
	{
		x,			y,			z,		c1, 0, 1,
		x,			y + height, z,		c3, 0, 0,
		x + width,	y,			z,		c2, 1, 1,
		x + width,	y + height, z,		c4, 1, 0,
	};
	pDevice->SetFVF( D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE2(0) );
	pDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, quad, sizeof(quad[0]) );
}




HRESULT DrawTransformedRectangle( LPDIRECT3DDEVICE9 pDevice, const RECT& r, D3DCOLOR c )
{
	struct
	{
		float pos[4];
		D3DCOLOR color;
	} rect[] =
	{
		(FLOAT)r.left, (FLOAT)r.top, 0.f, 1.f, c,
		(FLOAT)r.right, (FLOAT)r.top, 0.f, 1.f, c,
		(FLOAT)r.right, (FLOAT)r.bottom, 0.f, 1.f, c,
		(FLOAT)r.left, (FLOAT)r.bottom, 0.f, 1.f, c,
		(FLOAT)r.left, (FLOAT)r.top, 0.f, 1.f, c
	};
	pDevice->SetFVF( D3DFVF_XYZRHW | D3DFVF_DIFFUSE );
	return pDevice->DrawPrimitiveUP( D3DPT_LINESTRIP, 4, rect, sizeof(rect[0]) );
}




HRESULT DrawRectangle( LPDIRECT3DDEVICE9 pDevice, D3DXVECTOR2 v1, D3DXVECTOR2 v2, D3DXVECTOR2 v3, D3DXVECTOR2 v4, D3DCOLOR c )
{
	float z = 0.f;
	struct
	{
		float pos3[3];
		D3DCOLOR color;
	} rect[] =
	{
		v1.x, v1.y, z, c,
		v2.x, v2.y, z, c,
		v3.x, v3.y, z, c,
		v4.x, v4.y, z, c,
		v1.x, v1.y, z, c
	};
	pDevice->SetFVF( D3DFVF_XYZ | D3DFVF_DIFFUSE );
	return pDevice->DrawPrimitiveUP( D3DPT_LINESTRIP, 4, rect, sizeof(rect[0]) );
}





//
//VOID DrawFullscreenQuad( LPDIRECT3DDEVICE9 pDevice, FLOAT z )
//{
//	float quad[] = 
//	{
//		-1.0f, 1.0f, z,  0.0f,0.0f ,
//		 1.0f, 1.0f, z,  1.0f,0.0f ,
//		-1.0f,-1.0f, z,  0.0f,1.0f ,
//		 1.0f,-1.0f, z,  1.0f,1.0f
//	};
//
//	pDevice->SetFVF( D3DFVF_XYZ | D3DFVF_TEX1 );
//	pDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, quad, sizeof(TVertexUV) );
//}




HRESULT SetRenderTarget( LPDIRECT3DDEVICE9 pDevice, UINT rtIndex, LPDIRECT3DTEXTURE9 pRTTexture )
{
	LPDIRECT3DSURFACE9 pSurface = NULL;
	HRESULT hr =pRTTexture->GetSurfaceLevel( 0, &pSurface );
	if( FAILED( hr ) ) return hr;
	hr = pDevice->SetRenderTarget( rtIndex, pSurface );
	pSurface->Release();
	return hr;
}



// Assumes buffers have sufficient space.
// Number of indices: 6 * (uDensity-1) * (uDensity-1)
// Number of vertices: uDensity * uDensity
VOID FillPlaneData( UINT uDensity, WORD* pIndices, D3DXVECTOR2* pVertices )
{
	// Fill the vertex buffer.
	DWORD x, y;
	for( y = 1; y < uDensity; y++ )
	{
		for( x = 1; x < uDensity; x++ )
		{
			*pIndices++ = (WORD)( (y-1)*uDensity + (x-1) );
			*pIndices++ = (WORD)( (y-0)*uDensity + (x-1) );
			*pIndices++ = (WORD)( (y-1)*uDensity + (x-0) );

			*pIndices++ = (WORD)( (y-1)*uDensity + (x-0) );
			*pIndices++ = (WORD)( (y-0)*uDensity + (x-1) );
			*pIndices++ = (WORD)( (y-0)*uDensity + (x-0) );
		}
	}

	// Now fill the index buffer.
	for( y = 0; y < uDensity; y++ )
	{
		for( x = 0; x < uDensity; x++ )
		{
			*pVertices++ = D3DXVECTOR2( ((float)x / (float)(uDensity-1) - 0.5f) * D3DX_PI,
										((float)y / (float)(uDensity-1) - 0.5f) * D3DX_PI );
		}
	}
}





// Assumes buffers are not created.
HRESULT CreatePlane( LPDIRECT3DDEVICE9 pDevice, LPDIRECT3DVERTEXBUFFER9 pVB,
					LPDIRECT3DINDEXBUFFER9 pIB, UINT uDensity )
{
	HRESULT hr;
	DWORD numIndices = 6 * (uDensity-1) * (uDensity-1);
	DWORD numVertices = uDensity * uDensity;


	if( FAILED( hr = pDevice->CreateIndexBuffer( numIndices * sizeof(WORD),
		0, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &pIB, NULL ) ) ) return hr;

	if(	FAILED( hr = pDevice->CreateVertexBuffer( numVertices * sizeof(D3DXVECTOR2),
		0, 0, D3DPOOL_DEFAULT, &pVB, NULL ) ) ) return hr;


	WORD* pIndices;
	D3DXVECTOR2 *pVertices;

	if( FAILED( hr = pIB->Lock( 0, 0, (VOID**)&pIndices, 0 ) ) ) return hr;
	if( FAILED( hr = pVB->Lock( 0, 0, (VOID**)&pVertices, 0 ) ) ) return hr;

	FillPlaneData( uDensity, pIndices, pVertices );

	pIB->Unlock();
	pVB->Unlock();

	return S_OK;
}



/*
///////////////////////////////// Texture utility functions /////////////////////////////
// Create a Direct3D texture from the specified RGBASurface.
HRESULT CreateTextureFromARGBSurface(	LPDIRECT3DDEVICE9 pDevice, ARGBSurface* pSurface,
												LPDIRECT3DTEXTURE9* ppTexture )
{
	HRESULT hr;
	if( pDevice == NULL || pSurface == NULL || ppTexture == NULL )
		return E_INVALIDARG;

	if( *ppTexture )
		(*ppTexture)->Release();

	// Create a 1-level texture with the size of the given surface.
	hr = pDevice->CreateTexture(	pSurface->uWidth, pSurface->uHeight, 1, NULL,
									D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, ppTexture, NULL );
	if( FAILED( hr ) )
		return hr;

	D3DSURFACE_DESC d3dsd;
	D3DLOCKED_RECT LockedRect;
	UINT x, y, k;

	(*ppTexture)->GetLevelDesc( 0, &d3dsd );

	// Now lock the texture and copy the RGBASurface data into it.
	hr = (*ppTexture)->LockRect( 0, &LockedRect, NULL, NULL );//D3DLOCK_DISCARD );
	if( FAILED( hr ) )
		return hr;

	DWORD* pTextureMemory = (DWORD*)LockedRect.pBits;
	// sizeof(DWORD) should be replaced with the actual bit depth of the texture.
	UINT uPitchInDWORDs = LockedRect.Pitch / sizeof(DWORD);
	for( y=0; y<d3dsd.Height; y++ )
	{
		k = y * uPitchInDWORDs;
		for( x=0; x<d3dsd.Width; x++ )
		{
			pTextureMemory[ x + k ] = D3DCOLOR_RGBA(pSurface->pPixels[x+y*d3dsd.Height].r,
													pSurface->pPixels[x+y*d3dsd.Height].g,
													pSurface->pPixels[x+y*d3dsd.Height].b,
													pSurface->pPixels[x+y*d3dsd.Height].a );
		}
	}
	(*ppTexture)->UnlockRect( 0 );

	return S_OK;
}
*/


/*



HRESULT CreateTexturedSphere(	LPDIRECT3DDEVICE9 pDevice, LPD3DXMESH* ppMesh, FLOAT fRadius,
								UINT uSlices, UINT uStacks )
{
	HRESULT hr;
	if( pDevice == NULL || ppMesh == NULL )
		return E_INVALIDARG;

	if( *ppMesh )
		(*ppMesh)->Release();

	LPD3DXMESH pTempMesh;
	hr = D3DXCreateSphere( pDevice, fRadius, uSlices, uStacks, &pTempMesh, NULL );
	if( FAILED( hr ) )
		return hr;

	hr = pTempMesh->CloneMeshFVF( NULL, TVertexNUV::FVF, pDevice, ppMesh );
	pTempMesh->Release();
	if( FAILED( hr ) )
		return hr;

	INT nVertexCount = (*ppMesh)->GetNumVertices();
	LPVertexNUV pVertices = NULL;
	hr = (*ppMesh)->LockVertexBuffer( 0, (VOID**)&pVertices );
	if( FAILED( hr ) )
		return hr;
	for( INT i=0; i<nVertexCount; i++ )
	{	
		D3DXVECTOR3 v = pVertices->pos;
		D3DXVec3Normalize( &v, &v );
		pVertices->tex.x = asinf( v.x )/D3DX_PI + 0.5f;	// tu.
		pVertices->tex.y = asinf( v.y )/D3DX_PI + 0.5f; // tv.
		
		// OR:
		//pVertices->vTex.x = asinf( pVertices->vNormal.x )/D3DX_PI + 0.5f;
		//pVertices->vTex.y = asinf( pVertices->vNormal.y )/D3DX_PI + 0.5f;

		pVertices++;
	}
	(*ppMesh)->UnlockVertexBuffer();

	return S_OK;
}




HRESULT CreateTexturedPlane(	LPDIRECT3DDEVICE9 pDevice, LPD3DXMESH* ppMesh,
								UINT PatchesU, UINT PatchesV, FLOAT fWidth,
								FLOAT fLength, FLOAT fRepeatU, FLOAT fRepeatV,
								PLANE_ORIENTATION PlaneOrientation,
								DWORD dwOptions )
{
	HRESULT hr;
	if( pDevice == NULL || ppMesh == NULL )
		return E_INVALIDARG;

	if( PatchesU == 0 )
		PatchesU = 1;
	if( PatchesV == 0 )
		PatchesV = 1;
	if( fWidth <= 0.0f )
		fWidth = 1.0f;
	if( fLength <= 0.0f )
		fLength = 1.0f;
	if( fRepeatU <= 0.0f )
		fRepeatU = 1.0f;
	if( fRepeatV <= 0.0f )
		fRepeatV = 1.0f;

	SAFE_RELEASE( (*ppMesh) );
	
	LPD3DXMESH pWallMeshTemp = NULL;
	DWORD* pdwAdjacency = NULL;

	UINT uTriangleCount = PatchesU * PatchesV * 2;
	UINT uVertexCount = uTriangleCount * 3;
	hr = D3DXCreateMeshFVF(	uTriangleCount, uVertexCount, dwOptions,
							TVertexUV::FVF, pDevice, &pWallMeshTemp );
	if( FAILED( hr ) )
		return hr;

	// Fill in the grid vertex data
	LPVertexUV pVertices = NULL;
	hr = pWallMeshTemp->LockVertexBuffer(0, (VOID**)&pVertices);
	if( FAILED( hr ) )
	{
		SAFE_RELEASE( pWallMeshTemp );
		return hr;
	}
	FLOAT dU = 1.0f/(PatchesU);
	FLOAT dV = 1.0f/(PatchesV);
	UINT k = 0;
	FLOAT PosU, PosV;
	// Speed optimization variables.
	FLOAT fWidthOver2 = fWidth/2;
	FLOAT fLengthOver2 = fLength/2;
	FLOAT uByDU;
	FLOAT vByDV;
	FLOAT fWidthByDU = fWidth * dU;
	FLOAT fLengthByDV = fLength * dV;
	D3DXVECTOR2 vPos;
	for( UINT v=0; v < (PatchesV); v++)
	{
		vByDV = v * dV;
		for (UINT u=0; u < (PatchesU); u++)
		{
			uByDU = u * dU;
			PosU = fWidth * uByDU;
			PosV = fLength * vByDV;
			vPos = D3DXVECTOR2( fWidthOver2 - PosU, fLengthOver2 - PosV );
			switch( PlaneOrientation )
			{
			case PO_XY:		pVertices[k].pos = D3DXVECTOR3( vPos.x, vPos.y, 0.0f );		break;
			case PO_XZ:		pVertices[k].pos = D3DXVECTOR3( vPos.x, 0.0f, vPos.y );		break;
			default:		pVertices[k].pos = D3DXVECTOR3( 0.0f, vPos.x, vPos.y );
			}
			pVertices[k].tex = D3DXVECTOR2( PosU/fWidth * fRepeatU, PosV/fLength * fRepeatV );
			k++;

			PosU = fWidth * uByDU;
			PosV = fLengthByDV * (v+1);
			vPos = D3DXVECTOR2( fWidthOver2 - PosU, fLengthOver2 - PosV );
			switch( PlaneOrientation )
			{
			case PO_XY:		pVertices[k].pos = D3DXVECTOR3( vPos.x, vPos.y, 0.0f );		break;
			case PO_XZ:		pVertices[k].pos = D3DXVECTOR3( vPos.x, 0.0f, vPos.y );		break;
			default:		pVertices[k].pos = D3DXVECTOR3( 0.0f, vPos.x, vPos.y );
			}
			pVertices[k].tex = D3DXVECTOR2( PosU/fWidth * fRepeatU, PosV/fLength * fRepeatV );
			k++;

			PosU = fWidthByDU * (u+1);
			PosV = fLengthByDV * (v+1);
			vPos = D3DXVECTOR2( fWidthOver2 - PosU, fLengthOver2 - PosV );
			switch( PlaneOrientation )
			{
			case PO_XY:		pVertices[k].pos = D3DXVECTOR3( vPos.x, vPos.y, 0.0f );		break;
			case PO_XZ:		pVertices[k].pos = D3DXVECTOR3( vPos.x, 0.0f, vPos.y );		break;
			default:		pVertices[k].pos = D3DXVECTOR3( 0.0f, vPos.x, vPos.y );
			}
			pVertices[k].tex = D3DXVECTOR2( PosU/fWidth * fRepeatU, PosV/fLength * fRepeatV );
			k++;

			PosU = fWidth * uByDU;
			PosV = fLength * vByDV;
			vPos = D3DXVECTOR2( fWidthOver2 - PosU, fLengthOver2 - PosV );
			switch( PlaneOrientation )
			{
			case PO_XY:		pVertices[k].pos = D3DXVECTOR3( vPos.x, vPos.y, 0.0f );		break;
			case PO_XZ:		pVertices[k].pos = D3DXVECTOR3( vPos.x, 0.0f, vPos.y );		break;
			default:		pVertices[k].pos = D3DXVECTOR3( 0.0f, vPos.x, vPos.y );
			}
			pVertices[k].tex = D3DXVECTOR2( PosU/fWidth * fRepeatU, PosV/fLength * fRepeatV );
			k++;

			PosU = fWidthByDU * (u+1);
			PosV = fLengthByDV * (v+1);
			vPos = D3DXVECTOR2( fWidthOver2 - PosU, fLengthOver2 - PosV );
			switch( PlaneOrientation )
			{
			case PO_XY:		pVertices[k].pos = D3DXVECTOR3( vPos.x, vPos.y, 0.0f );		break;
			case PO_XZ:		pVertices[k].pos = D3DXVECTOR3( vPos.x, 0.0f, vPos.y );		break;
			default:		pVertices[k].pos = D3DXVECTOR3( 0.0f, vPos.x, vPos.y );
			}
			pVertices[k].tex = D3DXVECTOR2( PosU/fWidth * fRepeatU, PosV/fLength * fRepeatV );
			k++;

			PosU = fWidthByDU * (u+1);
			PosV = fLength * vByDV;
			vPos = D3DXVECTOR2( fWidthOver2 - PosU, fLengthOver2 - PosV );
			switch( PlaneOrientation )
			{
			case PO_XY:		pVertices[k].pos = D3DXVECTOR3( vPos.x, vPos.y, 0.0f );		break;
			case PO_XZ:		pVertices[k].pos = D3DXVECTOR3( vPos.x, 0.0f, vPos.y );		break;
			default:		pVertices[k].pos = D3DXVECTOR3( 0.0f, vPos.x, vPos.y );
			}
			pVertices[k].tex = D3DXVECTOR2( PosU/fWidth * fRepeatU, PosV/fLength * fRepeatV );
			k++;
		}
	}
	pWallMeshTemp->UnlockVertexBuffer();

	// Fill in index data
	WORD* pIndex;
	hr = pWallMeshTemp->LockIndexBuffer(0, (LPVOID*)&pIndex);
	if( FAILED( hr ) )
	{
		SAFE_RELEASE( pWallMeshTemp );
		return hr;
	}

	WORD iIndex;
	for( iIndex = 0; iIndex < uTriangleCount * 3; iIndex++ )
	{
		*(pIndex++) = iIndex;
	}
	pWallMeshTemp->UnlockIndexBuffer();

	// Eliminate redundant vertices
	pdwAdjacency = new DWORD[3 * uTriangleCount];
	
	hr = D3DXWeldVertices( pWallMeshTemp, D3DXWELDEPSILONS_WELDALL, NULL, NULL, pdwAdjacency, NULL, NULL );
	if( FAILED( hr ) )
	{
		SAFE_DELETE_ARRAY( pdwAdjacency );
		SAFE_RELEASE( pWallMeshTemp );
		return hr;
	}

	// Optimize the mesh
	hr = pWallMeshTemp->OptimizeInplace(	D3DXMESHOPT_COMPACT | D3DXMESHOPT_VERTEXCACHE,
											pdwAdjacency, NULL, NULL, NULL );
	if( FAILED( hr ) )
	{
		SAFE_DELETE_ARRAY( pdwAdjacency );
		SAFE_RELEASE( pWallMeshTemp );
		return hr;
	}

	SAFE_RELEASE( (*ppMesh) );

	// Copy the generated mesh into ppMesh.
	hr = pWallMeshTemp->CloneMeshFVF( D3DXMESH_WRITEONLY, TVertexUV::FVF, pDevice, ppMesh );
	if( FAILED( hr ) )
	{
		SAFE_DELETE_ARRAY( pdwAdjacency );
		SAFE_RELEASE( pWallMeshTemp );
		return hr;
	}
	SAFE_RELEASE( pWallMeshTemp );
	SAFE_DELETE_ARRAY( pdwAdjacency );

	return hr;
}
*/


}; //namespace DX9
