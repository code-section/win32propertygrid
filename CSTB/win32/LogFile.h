# pragma once

# ifndef CSTB_LOGFILE_H
# define CSTB_LOGFILE_H


# include <windows.h>


namespace CSTB
{


/// This class can be used to write text to a file for logging purposes.
class LogFile
{
public:
	HANDLE hFile;



	LogFile( LPCTSTR filename = 0, bool overwrite = false )
	{
		hFile = INVALID_HANDLE_VALUE;
		if( filename )
			SetFile( filename, overwrite );
	}



	~LogFile() { Close(); }



	bool SetFile( LPCTSTR filename, bool overwrite = false )
	{
		Close();
		hFile = CreateFile( filename, GENERIC_WRITE,
			0, // Do not share
			NULL, // Default security
			overwrite ? CREATE_ALWAYS : OPEN_ALWAYS, // Overwrite existing
													 //FILE_FLAG_WRITE_THROUGH, // Write directly to file (we don't want to lose output if process crashes)
													 //FILE_FLAG_NO_BUFFERING,
			0,
			NULL );
		return hFile != INVALID_HANDLE_VALUE;
	}



	void Close()
	{
		if( hFile == INVALID_HANDLE_VALUE ) return;
		CloseHandle( hFile );
		hFile = INVALID_HANDLE_VALUE;
	}



	void Write( const char* msg, int len = -1 )
	{
		if( hFile == INVALID_HANDLE_VALUE || len == 0 ) return;
		if( len < 0 ) len = strlen( msg );
		DWORD written = 0;
		WriteFile( hFile, msg, len, &written, 0 );
	}



	void Write( const wchar_t* msg, int len = -1 )
	{
		if( hFile == INVALID_HANDLE_VALUE || len == 0 ) return;
		if( len < 0 ) len = wcslen( msg );
		DWORD written = 0;
		WriteFile( hFile, msg, len * sizeof(wchar_t), &written, 0 );
	}



	void operator() ( const char* msg ) { Write( msg ); }
	void operator() ( const wchar_t* msg ) { Write( msg ); }
};



}; // namespace CSTB



# endif