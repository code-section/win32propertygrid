

# include "Dialog.h"



namespace CSTB
{


BOOL Dialog::Create( HINSTANCE hInstance, HWND hParent, LPCTSTR templateName )
{
	EnableCreationHook();
	return NULL != ::CreateDialog( hInstance, templateName, hParent, (DLGPROC)Dialog::DlgProc );
}





INT_PTR Dialog::DoModal( HINSTANCE hInstance, HWND hParent, LPCTSTR templateName )
{
	EnableCreationHook();
	return DialogBox( hInstance, templateName, hParent, (DLGPROC)Dialog::DlgProc );
}





BOOL Dialog::CreateIndirect( HINSTANCE hInstance, HWND hParent, LPCDLGTEMPLATE pDialogTemplate )
{
	EnableCreationHook();
	return NULL != ::CreateDialogIndirect( hInstance, pDialogTemplate, hParent, (DLGPROC)Dialog::DlgProc );
}




INT_PTR Dialog::DoModalIndirect( HINSTANCE hInstance, HWND hParent, LPCDLGTEMPLATE pDialogTemplate )
{
	EnableCreationHook();
	return NULL != ::DialogBoxIndirect( hInstance, pDialogTemplate, hParent, (DLGPROC)Dialog::DlgProc );
}




// static
INT_PTR CALLBACK Dialog::DlgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	Dialog* dlg = (Dialog*)FromHandle( hWnd );
	if( dlg )
		return dlg->MsgProc( msg, wParam, lParam );
	return FALSE;
}



int Dialog::TranslateMessage( MSG* pMsg )
{
	INT ret = Window::TranslateMessage( pMsg );
	if( ret != 0 ) return ret;
	return IsDialogMessage( hWnd, pMsg );
}




BOOL Dialog::Subclass( HWND hNewWnd )
{
	if( hWnd || FromHandle( hNewWnd ) )
		return FALSE;
	m_OriginalProc = (WNDPROC)::SetWindowLongPtr( hNewWnd, DWLP_DLGPROC, (LONG)DlgProc );
	return Attach( hNewWnd );
}




LRESULT Dialog::MsgProc( UINT Msg, WPARAM wParam, LPARAM lParam )
{
	switch( Msg )
	{
	case WM_INITDIALOG: return OnInitDialog();
	case WM_NOTIFY:
		{
			BOOL bHandled = FALSE;
			LRESULT ret = OnNotify( wParam, (NMHDR*)lParam, bHandled );
			if( bHandled )
			{
				SetWindowLongPtr( hWnd, DWLP_MSGRESULT, ret );
				return ret;
			}
		}
		break;
	}
	return Window::MsgProc( Msg, wParam, lParam );
}




LRESULT Dialog::OnCommand( HWND hCtrl, WORD id, WORD code )
{
	switch( id )
	{
	case IDOK:
	case IDCANCEL:
		EndDialog( hWnd, id ); // TODO: ensure this is correct whether the dialog is modal or modeless.
		return TRUE;
	}
	return FALSE;
}


}; // namespace CSTB