/**
Simple log (text-output) window.
An output file can be assigned so that output is also directed to that file.

Copyright Adel Amro - http://code-section.com
*/


# ifndef CSTB_LOG_WINDOW_H
# define CSTB_LOG_WINDOW_H



# include <windows.h>



namespace CSTB
{


class LogWindow
{
public:
	LogWindow();
	~LogWindow();

	BOOL Create( HINSTANCE, HWND parent = NULL, BOOL topMost = FALSE );
	BOOL SetOutputFile( LPCTSTR fileName, BOOL overwrite = FALSE );
	BOOL SetOutputFile( HANDLE file );

	VOID Clear();

	VOID Write( const char* msg, ... );
	VOID WriteNewLine( const char* msg, ... );
	VOID operator () ( const char* msg, ... ); // Equivalent to WriteNewLine().

# ifdef UNICODE
	VOID Write( const WCHAR* msg, ... );
	VOID WriteNewLine( const WCHAR* msg, ... );
	VOID operator () ( const WCHAR* msg, ... ); // Equivalent to WriteNewLine().
# endif

	VOID Indent()	{ indentation++; }							// Add an indentation level.
	VOID Unindent()	{ if( indentation != 0 ) indentation--; }	// Remove an indentation level.

	operator HWND() const { return hWnd; }

	HWND hWnd;
	HWND hEdit;
	HANDLE hFile;

	// Utility object for easier indentation.
	// The object will indent the log window when it's created, and unindent it when it goes out of scope.
	struct ScopeIndent
	{
		LogWindow& logWindow;
		ScopeIndent(LogWindow& _logWindow, const char* msg = 0) : logWindow(_logWindow) { logWindow.Indent(); if (msg) logWindow.WriteNewLine(msg);  }
		ScopeIndent(LogWindow& _logWindow, const WCHAR* msg = 0) : logWindow(_logWindow) { logWindow.Indent(); if (msg) logWindow.WriteNewLine(msg); }
		~ScopeIndent() { logWindow.Unindent(); }
	};

protected:
	LRESULT MsgProc( UINT Msg, WPARAM wParam, LPARAM lParam );
	static LRESULT CALLBACK StaticMsgProc( HWND, UINT, WPARAM, LPARAM );
	static BOOL RegisterClass( HINSTANCE );

	UINT indentation;
	BOOL deleteFile;
};


};


# endif // inclusion guard