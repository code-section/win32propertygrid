// UXTheme wrapper class.

// Allows an application to use uxtheme dynamically, without explicitly linking to uxtheme.lib
// and introducing a dependancy that causes the application to not work on pre-XP versions of Windows.
// Create an instance of this class and call the Load() method. If it succeeds, use the methods
// (well, member function pointers) which map to functions declared in UxTheme.h.
// -Adel Amro - http://code-section.com

# pragma once
# ifndef CSTB_UXTHEME_H
# define CSTB_UXTHEME_H



# include <windows.h>
# include <UxTheme.h>
// #include <tmschema.h>
# include <vssym32.h>


#ifndef WM_THEMECHANGED
#define WM_THEMECHANGED                 0x031A
#endif


class UXThemeProxy
{
protected:
	HMODULE		m_hlibUXTheme;
public:

	// Typedefs for function pointer syntax readability.

	typedef BOOL	( __stdcall* PFIsAppThemed )		();
	typedef BOOL	( __stdcall* PFIsThemeActive )		();
	typedef HTHEME	( __stdcall* PFOpenThemeData )		(HWND, LPCWSTR);
	typedef HRESULT ( __stdcall* PFCloseThemeData )		(HTHEME);

	typedef HRESULT ( __stdcall* PFGetThemePartSize )	(HTHEME, HDC, int, int, RECT*, enum THEMESIZE, SIZE*);
	typedef HRESULT ( __stdcall* PFGetThemeInt )		(HTHEME, int, int, int, int*);
	typedef HRESULT ( __stdcall* PFGetThemeMetric )		(HTHEME, HDC, int, int, int, int*);
	typedef HRESULT ( __stdcall* PFGetThemeMargins )	(HTHEME, HDC, int, int, int, RECT*, MARGINS*);
	typedef HRESULT ( __stdcall* PFGetThemeRect )		(HTHEME, int, int, int, RECT*);
	typedef HRESULT ( __stdcall* PFGetThemePosition )	(HTHEME, int, int, int, POINT*);
	typedef HRESULT ( __stdcall* PFGetThemeFont )		(HTHEME, HDC, int, int, int, LOGFONT*);
	typedef HRESULT ( __stdcall* PFGetThemeSysFont )	(HTHEME, int, LOGFONT*);
	typedef HRESULT ( __stdcall* PFGetThemeTextExtent ) (HTHEME, HDC, int, int, LPCWSTR, int, DWORD, LPCRECT, LPRECT);

	typedef HRESULT ( __stdcall* PFDrawThemeBackground )			(HTHEME, HDC, int, int, const RECT*, const RECT*);
	typedef HRESULT ( __stdcall* PFDrawThemeText )					(HTHEME, HDC, int, int, LPCWSTR, int, DWORD, DWORD, const RECT*);
	typedef HRESULT ( __stdcall* PFDrawThemeParentBackground )		(HWND, HDC, RECT* );

	typedef HRESULT ( __stdcall* PFGetThemeBackgroundContentRect )			(HTHEME, HDC, int, int, const RECT*, RECT*);
	typedef HRESULT ( __stdcall* PFIsThemeBackgroundPartiallyTransparent )	(HTHEME, int, int);
	


	// The function pointers.

	PFIsAppThemed			IsAppThemed;
	PFIsThemeActive			IsThemeActive;
	PFOpenThemeData			OpenThemeData;
	PFCloseThemeData		CloseThemeData;
	
	PFGetThemePartSize		GetThemePartSize;
	PFGetThemeInt			GetThemeInt;
	PFGetThemeMetric		GetThemeMetric;
	PFGetThemeMargins		GetThemeMargins;
	PFGetThemeRect			GetThemeRect;
	PFGetThemePosition		GetThemePosition;
	PFGetThemeFont			GetThemeFont;
	PFGetThemeSysFont		GetThemeSysFont;
	PFGetThemeTextExtent	GetThemeTextExtent;

	PFDrawThemeBackground			DrawThemeBackground;
	PFDrawThemeText					DrawThemeText;
	PFDrawThemeParentBackground		DrawThemeParentBackground;

	PFGetThemeBackgroundContentRect				GetThemeBackgroundContentRect;
	PFIsThemeBackgroundPartiallyTransparent		IsThemeBackgroundPartiallyTransparent;



	UXThemeProxy()		{ m_hlibUXTheme = NULL; Load(); }
	~UXThemeProxy()		{ Free(); }


	// See note at the head of the class.
	BOOL Load()
	{
		static BOOL bUnavailable = FALSE;

		if( m_hlibUXTheme )
			return TRUE;

		if( bUnavailable )
			return FALSE;

		if( !(m_hlibUXTheme = LoadLibrary( TEXT( "UxTheme.dll" ) ) ) )
		{
			bUnavailable = TRUE;
			return FALSE;
		}
		if(
			!(IsAppThemed = (PFIsAppThemed)GetProcAddress( m_hlibUXTheme, "IsAppThemed" ) ) ||
			!(IsThemeActive = (PFIsThemeActive)GetProcAddress( m_hlibUXTheme, "IsThemeActive" ) ) ||
			!(OpenThemeData = (PFOpenThemeData)GetProcAddress( m_hlibUXTheme, "OpenThemeData" ) ) ||
			!(CloseThemeData = (PFCloseThemeData)GetProcAddress( m_hlibUXTheme, "CloseThemeData" ) ) ||

			!(GetThemePartSize = (PFGetThemePartSize)GetProcAddress( m_hlibUXTheme, "GetThemePartSize" ) ) ||
			!(GetThemeInt = (PFGetThemeInt)GetProcAddress( m_hlibUXTheme, "GetThemeInt" ) ) ||
			!(GetThemeMetric = (PFGetThemeMetric)GetProcAddress( m_hlibUXTheme, "GetThemeMetric" ) ) ||
			!(GetThemeMargins = (PFGetThemeMargins)GetProcAddress( m_hlibUXTheme, "GetThemeMargins" ) ) ||
			!(GetThemeRect = (PFGetThemeRect)GetProcAddress( m_hlibUXTheme, "GetThemeRect" ) ) ||
			!(GetThemePosition = (PFGetThemePosition)GetProcAddress( m_hlibUXTheme, "GetThemePosition" ) ) ||
			!(GetThemeFont = (PFGetThemeFont)GetProcAddress( m_hlibUXTheme, "GetThemeFont" ) ) ||
			!(GetThemeSysFont = (PFGetThemeSysFont)GetProcAddress( m_hlibUXTheme, "GetThemeSysFont" ) ) ||
			!(GetThemeTextExtent = (PFGetThemeTextExtent)GetProcAddress( m_hlibUXTheme, "GetThemeTextExtent" ) ) ||

			!(DrawThemeBackground = (PFDrawThemeBackground)GetProcAddress( m_hlibUXTheme, "DrawThemeBackground" ) ) ||
			!(DrawThemeText = (PFDrawThemeText)GetProcAddress( m_hlibUXTheme, "DrawThemeText" ) ) ||
			!(DrawThemeParentBackground = (PFDrawThemeParentBackground)GetProcAddress( m_hlibUXTheme, "DrawThemeParentBackground" ) ) ||

			!(GetThemeBackgroundContentRect = (PFGetThemeBackgroundContentRect)GetProcAddress( m_hlibUXTheme, "GetThemeBackgroundContentRect" ) ) ||
			!(IsThemeBackgroundPartiallyTransparent = (PFIsThemeBackgroundPartiallyTransparent)GetProcAddress( m_hlibUXTheme, "IsThemeBackgroundPartiallyTransparent" ) )
			)
		{
			FreeLibrary( m_hlibUXTheme );
			m_hlibUXTheme = NULL;
			return FALSE;
		}
		return TRUE;
	}




	VOID Free()
	{
		if( m_hlibUXTheme )
			FreeLibrary( m_hlibUXTheme );
		m_hlibUXTheme = NULL;
	}	
};

# endif // include guard